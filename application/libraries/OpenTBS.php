<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . "/third_party/OpenTBS/tbs_class.php";
require_once APPPATH . "/third_party/OpenTBS/tbs_plugin_opentbs.php";

class OpenTBS extends clsTinyButStrong {

    public function __construct() {
        parent::__construct();
    }

}
