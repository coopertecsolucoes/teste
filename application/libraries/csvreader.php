<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class CSVReader {

    var $fields;/** columns names retrieved after parsing */
    var $separator = ';';/** separator used to explode each line */
    var $enclosure = '"';/** enclosure used to decorate each field */
    var $max_row_size = 4096;/** maximum row size to be used for decoding */

    function parse_file($p_Filepath) {
        $file = fopen($p_Filepath, 'r');
        $this->fields = fgetcsv($file, $this->max_row_size, $this->separator, $this->enclosure);
        $x_values = $this->fields;

        $content = "";
        $keys = $this->escape_string($x_values);

        $i = 1;
        while (($row = fgetcsv($file, $this->max_row_size, $this->separator, $this->enclosure)) != false) {
            if ($row != null) { // skip empty lines
                for($i=1;$i < count($x_values);$i++){
                    $valor = $row[$i];
                    $content .= " X:".$x_values[$i]."   Y:".$row[0]."   VALOR:".$valor."<br>";     
                }
            }
        }
        fclose($file);
        return $content;
    }

    function escape_string($data) {
        $result = array();
        foreach ($data as $row) {
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }

}

?>