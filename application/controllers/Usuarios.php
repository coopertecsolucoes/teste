<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Usuarios extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('usuario_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->usuario_model->retorna_usuarios_completos();
        $data->page_title = 'Users';
        $this->load->template('usuarios/index', $data);
    }

    public function create() {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->page_title = 'Users - Novo';
        $this->load->model('Usuario_grupo_model');
        $data->usuario_grupos = $this->Usuario_grupo_model->retorna_usuario_grupos_ativos();
        $this->load->template('usuarios/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->usuario_model->retorna_usuario($id);
        $this->load->model('Usuario_grupo_model');
        $data->usuario_grupos = $this->Usuario_grupo_model->retorna_usuario_grupos_ativos();
        $data->page_title = 'Users - Edit';
        $this->load->template('usuarios/edit', $data);
    }
    
    public function senha($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->usuario_model->retorna_usuario($id);
        $data->page_title = 'Users - Edit';
        $this->load->template('usuarios/senha', $data);
    }
    public function senha_en($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->usuario_model->retorna_usuario($id);
        $data->page_title = 'User - Edit';
        $this->load->template('usuarios/senha_en', $data);
    }

    public function create_usuario() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data->page_title = 'Users';

        // set validation rules

        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[4]|is_unique[usuario.usuario]', array('is_unique' => 'Este User já existe. Por favor, escolha outro nome de User.'));
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('confirma_senha', 'Confirmar Senha', 'trim|required|min_length[5]|matches[senha]');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->alerta_erro = 'Erro';
            $this->load->template('usuarios/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            
            $class->usuario = $this->input->post('usuario');
            $class->senha = $this->input->post('senha');
            $class->ativo = $this->input->post('ativo');
            if ($_SESSION['is_admin']) {
               $class->usuario_grupo_id = $this->input->post('usuario_grupo_id'); 
               $class->tipo = $this->input->post('tipo');
            }
            else{
                //Busca o Grupo de Usuário Padrão por Unidade de Negocios
                $this->load->model('Unidade_negocio_model');
                $uni_neg = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
                $class->tipo = 'T'; //Básico 
                $class->usuario_grupo_id = $uni_neg->usuario_grupo_id;
            }
            $class->nome = $this->input->post('nome');
            $class->email = $this->input->post('email');
            $class->sobre = $this->input->post('sobre');
            $class->facebook = $this->input->post('facebook');
            $class->instagram = $this->input->post('instagram');
            //UPLOAD DO LOGO
            if (!is_dir('./uploads/usuario/' . $id)) {
                mkdir('./uploads/usuario/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/usuario/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('unidade_negocio/edit/' . $id);
                } else {
                    $class->foto = 'uploads/usuario/'. $id . '/' . $this->upload->file_name;
                    
                }
            }

            $this->db->trans_start();
            if ($this->usuario_model->create_usuario($class)) {
                $retID = $this->db->insert_id();
                $this->load->model('usuario_acesso_model');
                $acesso = new stdClass();
                $acesso->usuario_id = $retID;
                
                //Grava a Unidade Atual como Padrão
                $this->grava_unidade_padrao($retID, $_SESSION["uni_negocio_id"]);
                
                if ($this->usuario_acesso_model->create_usuario_acesso($acesso)) {
                    $this->db->trans_complete();
                    $colec = new stdClass();
                    $colec->usuario_id = $retID;
                    $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                    //redirect('usuarios/edit/'.$retID);
                    redirect('usuarios/create');
                } else {
                    // user creation failed, this should never happen
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                    redirect('usuarios/create');
                }
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('usuarios/create');
            }
        }
    }

    function grava_unidade_padrao($usuario_id, $unidade_negocio_id){
        $class = new stdClass();
        $class->usuario_id = $usuario_id;
        $class->unidade_negocio_id = $unidade_negocio_id;
        $class->padrao = '1';
        $this->load->model('Usuario_unidade_model');
        $this->Usuario_unidade_model->create_usuario_unidade($class);        
    }
    
    
    
    public function update_usuario() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]');

        if ($this->form_validation->run() === false) {
            $data->result = $this->usuario_model->retorna_usuario($id);
            $data->page_title = 'Users - Edit';
            $this->load->template('usuarios/edit', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->id = $id;
            $class->usuario_grupo_id = $this->input->post('usuario_grupo_id');            
            $class->usuario = $this->input->post('usuario');
            $class->ativo = $this->input->post('ativo');
            if ($_SESSION['is_admin']) {
               $class->tipo = $this->input->post('tipo');
            }
            else{
                $class->tipo = 'T'; //Básico                
            }
            
            $class->nome = $this->input->post('nome');
            $class->email = $this->input->post('email');
            $class->sobre = $this->input->post('sobre');
            $class->facebook = $this->input->post('facebook');
            $class->instagram = $this->input->post('instagram');
               //UPLOAD DO LOGO
            if (!is_dir('./uploads/usuario/' . $id)) {
                mkdir('./uploads/usuario/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/usuario/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('unidade_negocio/edit/' . $id);
                } else {
                    $class->foto = 'uploads/usuario/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->input->post('senha')) {
                $class->senha = $this->input->post('senha');
            }

            if ($this->usuario_model->update_usuario($class)) {
                $colec = new stdClass();
                $colec->usuario_id = $id;
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                $data->result = $this->usuario_model->retorna_usuario($id);
                redirect('usuarios/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('usuarios/edit/' . $id);
            }
        }
    }

     public function update_troca_senha() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules

        // set variables from the form
        $class = new stdClass();
        $class->id = $id;
        $class->senha = $this->input->post('senha');

        if ($this->usuario_model->update_usuario($class)) {
            $colec = new stdClass();
            $colec->usuario_id = $id;
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('login/login');
        } else {
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

        }
 
    }

    
    
    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->usuario_model->delete_usuario($id)) {
            $data->alerta_sucesso = 'Sucesso ao gravar';
            $data->result = $this->usuario_model->retorna_usuarios();
            $data->page_title = 'Users';
            redirect('usuarios/index');
        } else {
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
        }
    }
    
    //###### Funções User e Unidade
         
    public function unidade_negocio($usuario_id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        
        $this->load->model('Usuario_unidade_model');
        $this->load->model('Unidade_negocio_model');
        $data->result= $this->usuario_model->retorna_usuario($usuario_id);
        $data->Usuario_unidade= $this->Usuario_unidade_model->retorna_usuario_unidades($usuario_id);
        $data->Unidade_negocio = $this->Unidade_negocio_model->retorna_Unidades_usuario($usuario_id);        
        $data->page_title = 'Business Subsidiary - Nova';
        $this->load->template('usuarios/unidade_negocio', $data);
    }
    
    public function create_unidade_negocio() {

        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'User - Unidade Negócios';
        $usuario_id = $this->input->post('usuario_id');

        // set validation rules
        $this->form_validation->set_rules('usuario_id', 'User', 'required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $data->result= $this->usuario_model->retorna_usuario($usuario_id);
            $data->Usuario_unidade= $this->Usuario_unidade_model->retorna_usuario_unidades($usuario_id);
            $data->Unidade_negocio = $this->Unidade_negocio_model->retorna_Unidades_usuario($usuario_id);        
            $this->load->template('usuarios/unidade_negocio', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->usuario_id = str_replace(',', '.', $this->input->post('usuario_id'));
            $class->unidade_negocio_id = $this->input->post('unidade_negocio_id');
            $class->padrao = $this->input->post('padrao');
            $this->load->model('Usuario_unidade_model');

            if ($this->Usuario_unidade_model->create_usuario_unidade($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('usuarios/unidade_negocio/' . $usuario_id);
            } else {
                // creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('usuarios/unidade_negocio/' . $usuario_id);
            }
        }
    }
    
    public function delete_unidade_negocio($usuario_unidade_id) {
        $this->load->model('Usuario_unidade_model');
        // create the data object
        $data = new stdClass();
        $Unidade= $this->Usuario_unidade_model->retorna_usuario_unidade($usuario_unidade_id);
        if ($this->Usuario_unidade_model->delete_usuario_unidade($usuario_unidade_id)) {
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('usuarios/unidade_negocio/' . $Unidade->usuario_id);
        } else {
            // deletion failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('usuarios/unidade_negocio/' . $Unidade->usuario_id);
        }
    }
    

}
