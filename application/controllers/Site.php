<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Site extends MY_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->model('Painel_site_model');
        $this->load->model('Cliente_model');
        $this->load->model('Historia_model');
        $this->load->model('Galeria_model');
        $this->load->model('Grupo_model'); 
        $this->load->model('Contato_model');
        $this->load->model('Categoria_model'); 
        $this->load->model('Servico_model');  
        $this->load->model('Unidade_negocio_model'); 
        $this->load->model('Unidade_negocio_foto_model');
        
        $_SESSION["uni_negocio_id"] = 1;
    }
    
      
    
    public function index() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        
        //Busca os informações da empresa
        $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
        //Busca os informações da empresa
        $data->fotos_empresa = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_fotos($_SESSION["uni_negocio_id"]);
        
        //Busca os informações da empresa
        $data->foto_principal = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_foto_principal($_SESSION["uni_negocio_id"]);
        
        
//        //Informações do Categorias
//        $data->categorias = $this->Categoria_model->retorna_categoria_ativas($_SESSION["uni_negocio_id"]); 
        $data->galerias = $this->Galeria_model->retorna_galeria_ativos($_SESSION["uni_negocio_id"]);
        
        $data->grupos = $this->Grupo_model->retorna_grupo_ativas($_SESSION["uni_negocio_id"]);  
        
        $data->categorias = $this->Categoria_model->retorna_categoria_ativas($_SESSION["uni_negocio_id"]);
        //Informações das Serviços Prestados
        $data->servicos = $this->Servico_model->retorna_servicos_ativos($_SESSION["uni_negocio_id"]);
        
          //Informações das Serviços Prestados
        $data->clientes = $this->Cliente_model->retorna_cliente_ativos($_SESSION["uni_negocio_id"]);
       
          //Informações das Serviços Prestados
        $data->historia = $this->Historia_model->retorna_historia_ativos($_SESSION["uni_negocio_id"]);
       
          
         //Informações das Serviços Prestados
        $data->apartamento = $this->Servico_model->retorna_servico(1);
        //Informações das Serviços Prestados
        $data->fotos = $this->Servico_model->retorna_servico_fotos(1);
        
        //Busca mensagens do Painel
        $data->painel = $this->Painel_site_model->retorna_painel_site_ativas($_SESSION["uni_negocio_id"]);
//        $data->painel = $this->Pacote_model->retorna_pacotes_ativos_painel($_SESSION["uni_negocio_id"]); 

        if ($data->empresa->ativo==='1'){
            $this->load->view('site/home', $data);
        }else{
            
            $this->load->template_site('site/manutencao', $data);
        }
        

    }
    

    public function servico($apartamento_id) {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
        //Busca os informações da empresa
        $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
        //Informações das Serviços Prestados
        $data->servicos = $this->Servico_model->retorna_servicos_ativos($_SESSION["uni_negocio_id"]);
        
         //Informações das Serviços Prestados
        $data->apartamento = $this->Servico_model->retorna_servico($apartamento_id);
        //Informações das Serviços Prestados
        $data->fotos = $this->Servico_model->retorna_servico_fotos($apartamento_id);
        
        $this->load->template_site('site/servico', $data);
    }
    
 
    public function contato_reserva($apartamento_id) {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
              //Informações das Serviços Prestados
        $apartamento = $this->Servico_model->retorna_servico($apartamento_id);
        
        $data->unidade_negocio_id = $_SESSION['uni_negocio_id'];
        $data->data = date('Y-m-d');
        $data->nome = $this->input->post('nome');
        $data->telefone = $this->input->post('telefone');
        $data->email = $this->input->post('email');
        $data->assunto = 'Reserva do' . $apartamento->descricao;
        $data->mensagem = $this->input->post('mensagem');
        $data->observacoes = 'Reserva';
        $data->status = 1;
        
        $this->Contato_model->create_contato($data);
        $contato_id = $this->db->insert_id();
        
        $this->enviar_aviso_contato($contato_id);
        
        $this->session->set_flashdata('alerta', 'Solicitação de reserva enviada com sucesso.');
        redirect('site/apartamento/' . $apartamento_id);
        
    }
    
    public function contato() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data->unidade_negocio_id = $_SESSION['uni_negocio_id'];
        $data->data = date('Y-m-d');
        $data->nome = $this->input->post('nome');
        $data->telefone = $this->input->post('telefone');
        $data->email = $this->input->post('email');
        $data->assunto = 'Mais Informações';
        $data->mensagem = $this->input->post('mensagem');
        $data->observacoes = 'CONTATO GERAL';
        $data->status = 1;
        
        $this->Contato_model->create_contato($data);
        $contato_id = $this->db->insert_id();
        
        $this->enviar_aviso_contato($contato_id);
        
        
        redirect('site/index');
        
    }
    
    public function enviar_aviso_contato($contato_id){
        
        $this->load->library('email');
        $empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
        $contato = $this->Contato_model->retorna_contato($contato_id);
        
        // Define remetente e destinatário
        $this->email->from($empresa->email_contato, 'Contato do Site'); // Remetente
        $this->email->to($empresa->email_contato, 'Atendimento'); // Destinatário
       
        // Define o assunto do email
        $this->email->subject('Novo Contato no Site');
        
        $corpo = '<table border="0" cellpadding="2" cellspacing="2" height="171" width="613">'
            .'<thead> '
                .'<tr>'
                    .'<th colspan="4" scope="col">'
                        .'<span style="font-size:16px;"><strong>Contato Nº ' . $contato->id .'</strong></span></th>'
                    .'</tr>'
            .'</thead>'
            .'<tbody>'
                .'<tr>'
                    .'<td>'
                        .'<span style="font-size:14px;"><strong>Nome: </strong> ' . $contato->nome .'</span></td>'
                    .'<td colspan="3" rowspan="1">'
                        .'<span style="font-size:14px;"><strong>Data: </strong> ' . date('d/m/Y', strtotime($contato->data)) .'</span></td>'
                .'</tr>'
                .'<tr>'
                    .'<td>'
                        .'<span style="font-size:14px;"><strong>Email:</strong>  ' . $contato->email .'</span>'
                    . '</td>'
                    .'<td colspan="3" rowspan="1">'
                        .'<span style="font-size:14px;"><strong>Celular:</strong>  ' . $contato->telefone .'</span></td>'
                .'</tr>'
                .'<tr>'
                    .'<td>'
                        .'<span style="font-size:14px;"><strong>Mensagem:</strong>  ' . $contato->mensagem .'</span>'
                    . '</td>'
                    
                .'</tr>'
            
            .'</tbody></table>';
        // Define o corpo do email com a mensagem
        $this->email->message($corpo);
        
        // Envia o email
        $this->email->send();
        $this->session->set_flashdata('alerta_erro',$this->email->print_debugger());        
        
    }


    
        public function sobre() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
         //Busca os informações da empresa
        $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
        //Busca os informações da empresa
        $data->fotos_empresa = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_fotos($_SESSION["uni_negocio_id"]);
         
        $this->load->template_site('modelo_site/modelo_site_sobre', $data);

        
    }
    public function pacotes() {
        
        $servico_id = $this->input->post('servico_id'); 
        $categoria_id = $this->input->post('categoria_id'); 
        $mostra_mais = $this->input->post('mostra_mais'); 
        if ($_SESSION["servico_id"] <> $servico_id){
            $_SESSION["servico_id"] = $servico_id;
        }
        $pacotes = $this->Pacote_model->retorna_pacotes_ativos_servico($_SESSION["uni_negocio_id"],$_SESSION["servico_id"],$categoria_id,$mostra_mais);      
         $html = ' <div class="row-pacotes col-md-12">';
                    
                   if (isset($pacotes)) {                        
                       foreach ($pacotes as $row) {    

                        $html = $html .'  <div class="pacotes col-md-3">'
                           .'<div class="pac-banner">' 
                               . '<a class="link" href="'. base_url('site/detalhes/'.$row->pacote_id ).'">'
                            .'    <div class="sombra"></div>'
                           .' <img src="'.base_url($row->foto_destaque).'" alt="">'
                           .'  <div class="label">'
                           .'   <div class="title">'. nl2br($row->descricao).'</div>'
//                           .'    <div class="price">A partir de <span>'.nl2br($row->valor).'</span></div>'
//                             .'        <a href="'. base_url('site/detalhes/'.$row->pacote_id ).'">Saiba Mais</a>'
                               
                           .'    </div>  </a> </div> </div> ';

                   }}


                   $html = $html .' <div class="clear"></div>           </div>';              
                   
        echo $html;
        
    }

    

    

    public function adm() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['page_title'] = 'Login';
        $this->load->view('login/login', $data);
        //redirect('site/site');

    }
       
    

      public function busca_galeria_grupo($grupo_id){
        $_SESSION["grupo_id"] = $grupo_id;
        $_SESSION["categoria_id"] = -1;
        $data = new stdClass();
        $data->galerias = $this->Galeria_model->retorna_galerias_site($_SESSION["grupo_id"], $_SESSION["categoria_id"]);
        $this->load->view('site/load_galeria', $data);
    } 
    
    public function busca_galeria_categoria($categoria_id){
        $_SESSION["categoria_id"] = $categoria_id;
        $data = new stdClass();
        $data->galerias = $this->Galeria_model->retorna_galerias_site($_SESSION["grupo_id"], $_SESSION["categoria_id"]);
        $this->load->view('site/load_galeria', $data);
    } 
    
  public function galeria($galeria_id) {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
        //Busca os informações da empresa
        $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
       
         //Informações das Serviços Prestados
        $data->galeria = $this->Galeria_model->retorna_galeria($galeria_id);
        
        //Informações das Serviços Prestados
        $data->fotos = $this->Galeria_model->retorna_galeria_fotos($galeria_id);
         $data->painel = $this->Painel_site_model->retorna_painel_site_ativas($_SESSION["uni_negocio_id"]);
        
        $this->load->template_site('site/galeria', $data);
        
    }

    
    
    
    


}
