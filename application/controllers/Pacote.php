<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Pacote extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Pacote_model');
        $this->load->model('Servico_model');
        $this->load->model('Item_model');
        $this->load->model('Categoria_model');
        
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Pacote_model->retorna_pacotes($_SESSION["uni_negocio_id"]);
        $this->load->template('pacote/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->servicos = $this->Servico_model->retorna_servicos_ativos($_SESSION["uni_negocio_id"]);
        $data->categoria = $this->Categoria_model->retorna_categoria_ativas($_SESSION["uni_negocio_id"]);
        $this->load->template('pacote/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->servicos = $this->Servico_model->retorna_servicos_ativos($_SESSION["uni_negocio_id"]);
        $data->categorias = $this->Categoria_model->retorna_categoria_ativas($_SESSION["uni_negocio_id"]);
//        $data->itens = $this->Item_model->retorna_item_ativos($_SESSION["uni_negocio_id"]);
//        $data->itens_salvos = $this->Pacote_model->retorna_pacote_itens($id);
        $data->categorias_salvas = $this->Pacote_model->retorna_pacote_categorias($id);
        $data->result = $this->Pacote_model->retorna_pacote($id);
        $this->load->template('pacote/edit', $data);
    }
    
     public function fotos($id) {
        $data = new stdClass();
        $this->load->library('form_validation');

        $data->result = $this->Pacote_model->retorna_pacote($id);
        $this->load->model('Pacote_foto_model');        
        $data->fotos = $this->Pacote_foto_model->retorna_pacote_fotos($id);       
        $this->load->template('pacote/imagens', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_pacote() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('pacote/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];   
            $class->descricao = $this->input->post('descricao');
            $class->servico_id = $this->input->post('servico_id');
//            $class->detalhe = $this->input->post('detalhe');
//            $class->observacao = $this->input->post('observacao');
//            $class->valor = $this->input->post('valor');
//            $class->valor_entrada = $this->input->post('valor_entrada');
//            $class->parcelamento = $this->input->post('parcelamento');
//            $class->destaque = $this->input->post('destaque');
            $class->ativo = 1;
//            $class->item_incluso = $this->input->post('item_incluso');
//            $class->obs_valor = $this->input->post('obs_valor');

            if ($this->Pacote_model->create_pacote($class)) {
                // OK
                $retID = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_inserir']);
                redirect('pacote/edit/' . $retID);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Ocorreu um erro. Por favor tente novamente.';
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                // send error to the view
                redirect('pacote/create');
            }
        }
    }

    public function update_pacote() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Pacote_model->retorna_pacote($id);
            $this->load->template('pacote/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];   
            $class->descricao = $this->input->post('descricao');
            $class->servico_id = $this->input->post('servico_id');
            $class->detalhe = $this->input->post('detalhe');
            $class->observacao = $this->input->post('observacao');
            $class->valor = $this->input->post('valor');
            $class->valor_entrada = $this->input->post('valor_entrada');
            $class->parcelamento = $this->input->post('parcelamento');
            $class->destaque = $this->input->post('destaque');
            $class->ativo = $this->input->post('ativo');
            $class->item_incluso = $this->input->post('item_incluso');
            $class->obs_valor = $this->input->post('obs_valor');
            
            $class->mostra_painel = $this->input->post('mostra_painel');
            $class->ordem = $this->input->post('ordem');
            
            $class->painel_titulo = $this->input->post('painel_titulo');
            $class->painel_preco = $this->input->post('painel_preco');
            $class->painel_parcelamento = $this->input->post('painel_parcelamento');
            $class->painel_entrada = $this->input->post('painel_entrada');
            $class->painel_obs = $this->input->post('painel_obs');
            
            
            
            
            //Foto Destaque
            if (!is_dir('./uploads/pacote/' . $id )) {
                mkdir('./uploads/pacote/'. $id , 0777);              
            }
            if (!is_dir('./uploads/pacote'. $id. '/destaque')) {
                  mkdir('./uploads/pacote/'. $id. '/destaque/' , 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/pacote/'. $id. '/destaque/';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('pacote/edit/' . $id);
                } else {
                    $class->foto_destaque = 'uploads/pacote/'. $id. '/destaque/' . $this->upload->file_name;
                    
                }
            }

            if ($this->Pacote_model->update_pacote($class)) {
                $tipos = new stdClass();
                $tipos->pacote_id = $id;
                $tipos->categorias = $this->input->post('categorias');
                $tipos->itens = $this->input->post('itens');
                if ($this->update_pacote_itens_categorias($tipos)) {
                    $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_update']);
                    redirect('pacote/edit/' . $id);
                } else {
                    // user creation failed, this should never happen
                    $data->error = 'Error! Please, try again.';
                    $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                    redirect('pacote/edit/' . $id);
                }
                $this->session->set_flashdata('alerta_sucesso',$_SESSION['msg_update']);
                $data->result = $this->Pacote_model->retorna_pacote($id);
                redirect('pacote/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('pacote/edit/' . $id);
            }
        }
    }
    
    public function update_pacote_itens_categorias($tipos) {
        $categoria = new stdClass();
        $item = new stdClass();
        if ($tipos->pacote_id) {
            $this->db->query("DELETE FROM pacote_categoria WHERE pacote_id = " . $tipos->pacote_id . "");
            $categoria->pacote_id = $tipos->pacote_id; 
            foreach ($tipos->categorias as $cat_id) {
                $categoria->categoria_id = $cat_id;
                $this->db->insert('pacote_categoria', $categoria);
            }
            $this->db->query("DELETE FROM pacote_item WHERE pacote_id = " . $tipos->pacote_id . "");
            $item->pacote_id = $tipos->pacote_id; 
            foreach ($tipos->itens as $item_id) {
                $item->item_id = $item_id;
                $this->db->insert('pacote_item', $item);
            }
            return true;
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Pacote_model->delete_pacote($id)) {

            // user creation ok
            $data->alerta_sucesso = $_SESSION['msg_delete'];
            redirect('pacote/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Ocorreu um erro. Por favor tente novamente.';
            $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
            redirect('pacote/index');
            
        }
    }

    
     public function update_foto() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');

        $id = $this->input->post('id');

        $this->form_validation->set_rules('id', 'Id Pacote', 'required');
      
        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Pacote_model->retorna_pacote($id);
            
            redirect('pacote/fotos/' . $id);
        } else {

            // set variables from the form
            $class = new stdClass();

            if (!is_dir('./uploads/pacote/' . $id)) {
                mkdir('./uploads/pacote/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/pacote/' . $id;
                $config['allowed_types'] = '*';
                $config['max_size'] = '1000000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('pacote/fotos/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;                   
                }
            }
            // fim upload
            // insere foto no BD
            $this->load->model('Pacote_foto_model');
            $class->pacote_id = $id;
            
            if (!$this->Pacote_foto_model->create_pacote_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                
            }
            redirect('pacote/fotos/' . $id);
        }
    }
    
    public function ativa_principal($pacote_foto_id) {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        
        $this->load->model('Pacote_foto_model');
        $foto = $this->Pacote_foto_model->retorna_pacote_foto($pacote_foto_id);
        
        $fotos = $this->Pacote_foto_model->ativa_foto_principal($foto->pacote_id,$pacote_foto_id);
        redirect('pacote/fotos/' . $foto->pacote_id);
    }
    
    public function delete_imagem($id) {
        $this->load->model('Pacote_foto_model');
        $foto = $this->Pacote_foto_model->retorna_pacote_foto($id);
        if ($this->Pacote_foto_model->delete_pacote_foto($foto->id)) {
            unlink("./uploads/pacote/" . $foto->pacote_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Foto excluida com sucesso');
            redirect('pacote/fotos/' . $foto->pacote_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('pacote/fotos/' . $foto->pacote_id);
        }
    }
}
