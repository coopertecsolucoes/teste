<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Pessoa extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Pessoa_model');
    }

    public function index() {
        $data = new stdClass();
        $uni_negocio = (int)$_SESSION['uni_negocio_id']; 
        $data->result = $this->Pessoa_model->retorna_Pessoas($uni_negocio);
        $data->page_title = 'Clientes';
        $this->load->template('pessoa/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Clientes';
        $this->load->model('cidade_model');
        $uni_negocio = (int)$_SESSION['uni_negocio_id']; 
        $data->cidades = $this->cidade_model->retorna_cidade_ativas($uni_negocio);
        $this->load->model('Condicao_pagamento_model');
        $data->cond_pgto = $this->Condicao_pagamento_model->retorna_condicao_pagamento_ativas($uni_negocio);
        $this->load->template('pessoa/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $this->load->model('cidade_model');
        $uni_negocio = (int)$_SESSION['uni_negocio_id']; 
        $data->cidades = $this->cidade_model->retorna_cidade_ativas($uni_negocio);
        $this->load->model('Condicao_pagamento_model');
        $data->cond_pgto = $this->Condicao_pagamento_model->retorna_condicao_pagamento_ativas($uni_negocio);
        $data->result = $this->Pessoa_model->retorna_Pessoa($id);
        $data->page_title = 'Clientes';
        $this->load->template('pessoa/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_Pessoa() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Cliente';

        // set validation rules
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('pessoa/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->razao_social = $this->input->post('razao_social');
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            //Colocar Sessão da Unidade Logada
            $class->unidade_negocio_id = (int)$_SESSION['uni_negocio_id'];                       
            $class->endereco = $this->input->post('endereco');
            $class->bairro = $this->input->post('bairro');
            $class->cep = $this->input->post('cep');
            $class->cnpj = $this->input->post('cnpj');
            $class->inscricao_estadual = $this->input->post('inscricao_estadual');
            $class->tipo_pessoa = $this->input->post('tipo_pessoa');
            $class->cidade_id = $this->input->post('cidade_id');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->condicao_pagamento_id = $this->input->post('condicao_pagamento_id');
            $class->ativo = $this->input->post('ativo');
            //Tipo C - Cliente
            $class->tipo_pessoa ='C';
            
            if ($this->Pessoa_model->create_Pessoa($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Gravado com sucesso.');
                redirect('pessoa/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Ocorreu um erro. Por favor tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Ocorreu um erro. Por favor tente novamente.');
                // send error to the view
                redirect('pessoa/create');
            }
        }
    }

    public function update_Pessoa() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Pessoa_model->retorna_Pessoa($id);
            $data->page_title = 'Cliente';
            $this->load->template('pessoa/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->razao_social = $this->input->post('razao_social');
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            //Colocar Sessão da Prefeitura Logada
            $class->unidade_negocio_id = (int)$_SESSION['uni_negocio_id'];                      
            $class->endereco = $this->input->post('endereco');
            $class->bairro = $this->input->post('bairro');
            $class->cep = $this->input->post('cep');
            $class->cnpj = $this->input->post('cnpj');
            $class->inscricao_estadual = $this->input->post('inscricao_estadual');
            $class->tipo_pessoa = $this->input->post('tipo_pessoa');
            $class->cidade_id = $this->input->post('cidade_id');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->condicao_pagamento_id = $this->input->post('condicao_pagamento_id');
            $class->ativo = $this->input->post('ativo');
            //Tipo C - Cliente
            $class->tipo_pessoa ='C';
            
            if ($this->Pessoa_model->update_Pessoa($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Gravado com sucesso.');
                $data->result = $this->Pessoa_model->retorna_Pessoa($id);
                redirect('pessoa/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Ocorreu um erro. Por favor tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Ocorreu um erro. Por favor tente novamente.');
                redirect('pessoa/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Pessoa_model->delete_Pessoa($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Apagado com sucesso.';
            redirect('pessoa/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Ocorreu um erro. Por favor tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Ocorreu um erro. Por favor tente novamente.');
            redirect('pessoa/index');
            
        }
    }
    
    public function retorna_cond_pgto_padrao($pessoa_id){   
        
     
        $pessoa = $this->Pessoa_model->retorna_pessoa($pessoa_id);   
        echo json_encode($pessoa);
        return '';

    }

}
