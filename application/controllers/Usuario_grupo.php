<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Usuario_grupo extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Usuario_grupo_model');
    }

    public function index() {
        $data = new stdClass();        
        $data->result = $this->Usuario_grupo_model->retorna_usuario_grupos();
        $data->page_title = 'User Group';
        $this->load->template('usuario_grupo/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'User Group - Novo';
        $this->load->template('usuario_grupo/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $this->load->model('Menu_model');
        $this->load->model('Usuario_grupo_menu_model');
        $data->result = $this->Usuario_grupo_model->retorna_usuario_grupo($id);
        
        $data->menus = $this->Menu_model->retorna_menus_ativos();
        $data->menus_salvos = $this->Usuario_grupo_menu_model->retorna_usuario_grupo_menus($id);
        
        $data->page_title = 'User Group - Novo';
        $this->load->template('usuario_grupo/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_usuario_grupo() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'User Group';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('usuario_grupo/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');            
            $class->ativo = $this->input->post('ativo');

            if ($this->Usuario_grupo_model->create_usuario_grupo($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('usuario_grupo/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('usuario_grupo/create');
            }
        }
    }

    public function update_usuario_grupo() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Usuario_grupo_model->retorna_usuario_grupo($id);
            $data->page_title = 'User Group - Edit';
            $this->load->template('usuario_grupo/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->descricao = $this->input->post('descricao');
            $class->ativo = $this->input->post('ativo');

            if ($this->Usuario_grupo_model->update_usuario_grupo($class)) {
                //Salva os menus do Grupo de Users
                $this->load->model('Usuario_grupo_menu_model');
                $menus_grp = new stdClass();
                $menus_grp->usuario_grupo_id = $id;
                $menus_grp->menus = $this->input->post('menus');
                
                if ($this->update_usuario_grupo_menu($menus_grp) ) {
                    $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                    redirect('usuario_grupo/edit/' . $id);
                } else {
                    // user creation failed, this should never happen
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                    redirect('usuario_grupo/edit/' . $id);
                }
                
                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                $data->result = $this->Usuario_grupo_model->retorna_usuario_grupo($id);
                redirect('usuario_grupo/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('usuario_grupo/edit/' . $id);
            }
        }
    }

    public function update_usuario_grupo_menu($data) {
        if ($data->usuario_grupo_id) {
            $this->db->query("DELETE FROM usuario_grupo_menu WHERE usuario_grupo_id = " . $data->usuario_grupo_id . "");
            foreach ($data->menus as $menu_id) {
                $data->menu_id = $menu_id;
                $this->db->insert('usuario_grupo_menu', $data);
            }
            return true;
        }
    }
    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Usuario_grupo_model->delete_usuario_grupo($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('usuario_grupo/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('usuario_grupo/index');
            
        }
    }
    
    public function atualiza_menu($id) {

        $this->load->model('usuario_model');
        //Carrega os Menus do Usuario
        $_SESSION['menus'] = $this->usuario_model->retorna_menus($_SESSION["usuario_id"]);
                 
        redirect('usuario_grupo/edit/'.$id);
            
        
    }
    
}
