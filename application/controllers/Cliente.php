<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Cliente extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Cliente_model');
        
    }

    public function index() {
        $data = new stdClass();
        
        $data->result = $this->Cliente_model->retorna_clientes($_SESSION["uni_negocio_id"]);
        
        $this->load->template('cliente/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->load->template('cliente/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        
        $data->result = $this->Cliente_model->retorna_cliente($id);
        $this->load->template('cliente/edit', $data);
    }

    
    
    
    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_cliente() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Painel Site';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('cliente/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');   
            $class->titulo = $this->input->post('titulo');
            $class->link = $this->input->post('link');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');
            //UPLOAD DO LOGO
            
            if ($this->Cliente_model->create_cliente($class)) {
                // OK
                $idret = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('cliente/edit/' . $idret);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('cliente/create');
            }
        }
    }

    public function update_cliente() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->load->template('cliente/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');  
            $class->titulo = $this->input->post('titulo');
            $class->link = $this->input->post('link');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');
            //UPLOAD DO LOGO
            if (!is_dir('./uploads/cliente/' . $id)) {
                mkdir('./uploads/cliente/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/cliente/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('cliente/edit/' . $id);
                } else {
                    $class->banner = 'uploads/cliente/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->Cliente_model->update_cliente($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso');
                //$data->result = $this->Linha_model->retorna_Tipomenu($id);
                redirect('cliente/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro, Por favor, tente novamente.');
                redirect('cliente/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Cliente_model->delete_cliente($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('cliente/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('cliente/index');
            
        }
    }
    
    public function update_imagem() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        $principal = $this->input->post('principal');
        // set validation rules
        $this->form_validation->set_rules('id', 'ID Painel', 'required');
        $this->form_validation->set_rules('principal', 'Principal', 'required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Cliente_model->retorna_cliente($id);
            $data->page_title = 'Painel - Mensagens';
            redirect('cliente/imagens/' . $id);
        } else {

            // set variables from the form
            $class = new stdClass();

            if (!is_dir('./uploads/cliente/' . $id)) {
                mkdir('./uploads/cliente/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/cliente/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('cliente/imagens/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;
                }
            }
            // fim upload
            // insere foto no BD
            $this->load->model('Cliente_foto_model');
            $class->cliente_id = $id;
            $class->principal = $this->input->post('principal');
            
            if (!$this->Cliente_foto_model->create_cliente_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
               
            }
             redirect('cliente/imagens/' . $id);
 
        }
    }
    
    public function delete_imagem($id) {
        $this->load->model('Cliente_foto_model');
        $foto = $this->Cliente_foto_model->retorna_cliente_foto($id);
        if ($this->Cliente_foto_model->delete_cliente_foto($foto->id)) {
            unlink("./uploads/cliente/" . $foto->cliente_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('cliente/imagens/' . $foto->cliente_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('cliente/imagens/' . $foto->cliente_id);
        }
    }
}
