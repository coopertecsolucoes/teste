<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Servico extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Servico_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Servico_model->retorna_servicos($_SESSION["uni_negocio_id"]);
        $data->page_title = 'Servico de Atuação';
        $this->load->template('servico/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = '';
        $this->load->template('servico/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Servico_model->retorna_servico($id);
        $data->page_title = 'Servico de Atuação';
        $this->load->template('servico/edit', $data);
    }

    public function imagens($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Servico_model->retorna_servico($id);
        $this->load->model('Servico_foto_model');
        
        $data->fotos = $this->Servico_foto_model->retorna_servico_fotos($id);
        $data->page_title = 'Área de Atuação - Fotos';
        $this->load->template('servico/imagens', $data);
    }
    
    
    
    
    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_servico() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Servico';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('servico/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');   
            $class->texto = $this->input->post('texto');  
            $class->detalhe = $this->input->post('detalhe');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');

            if ($this->Servico_model->create_servico($class)) {
                // OK
                $retID = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('servico/edit/' . $retID );
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('servico/create');
            }
        }
    }

    public function update_servico() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            //$data->result = $this->Servico_model->retorna_Tipomenu($id);
            $this->load->template('servico/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->descricao = $this->input->post('descricao');
            $class->texto = $this->input->post('texto'); 
            $class->detalhe = $this->input->post('detalhe');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');

            if ($this->Servico_model->update_servico($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                //$data->result = $this->Servico_model->retorna_Tipomenu($id);
                redirect('servico/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('servico/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Servico_model->delete_servico($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('servico/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('servico/index');
            
        }
    }
    
    public function update_imagem() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');

        $id = $this->input->post('id');

        $this->form_validation->set_rules('id', 'IdServico', 'required');
      
        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Servico_model->retorna_servico($id);
            $data->page_title = 'Servico - Imagens';
            redirect('servico/imagens/' . $id);
        } else {

            // set variables from the form
            $class = new stdClass();

            if (!is_dir('./uploads/servico/' . $id)) {
                mkdir('./uploads/servico/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/servico/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('servico/imagens/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;
                    $this->load->library('image_lib', $config2);
                }
            }
            // fim upload
            // insere foto no BD
            $this->load->model('Servico_foto_model');
            $class->servico_id = $id;
            $class->principal = 0;
            if (!$this->Servico_foto_model->create_servico_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                
            }
            redirect('servico/imagens/' . $id);
        }
    }
    
    public function ativa_principal($servico_foto_id) {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        
        $this->load->model('Servico_foto_model');
        $foto = $this->Servico_foto_model->retorna_servico_foto($servico_foto_id);
        
        $fotos = $this->Servico_foto_model->ativa_foto_principal($foto->servico_id,$servico_foto_id);
        redirect('servico/imagens/' . $foto->servico_id);
    }
    
    public function delete_imagem($id) {
        $this->load->model('Servico_foto_model');
        $foto = $this->Servico_foto_model->retorna_servico_foto($id);
        if ($this->Servico_foto_model->delete_servico_foto($foto->id)) {
            unlink("./uploads/servico/" . $foto->servico_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('servico/imagens/' . $foto->servico_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('servico/imagens/' . $foto->servico_id);
        }
    }
    
    
    public function mostra_modal_servico(){
        $this->load->model('Servico_foto_model');
        $servico_id = $this->input->post('servico_id');  
        $servico = $this->Servico_model->retorna_servico($servico_id);
        $servico_detalhes = $this->Servico_foto_model->retorna_servico_fotos_ativas($servico_id);
        
       
        $html = ' <div class="row">
                    <div class="modal-cabecalho">
                        <h3 >' . $servico->descricao .'</h3>	
                           
                        <h5> '.  nl2br($servico->detalhe) .'</h5>
                            <br/>
                    </div>	
                    </div>	
                        <!-- Slider -->
                        <div class="row">';
                        //   <div class="col-sm-3" id="slider-thumbs"> ' ;
//                                $primeiro = (int)0;
//                                foreach ($linha_detalhes as $row){
//                                
//                                    $html =  $html . '<ul class="hide-bullets">
//                                        <li class="col-sm-12">
//                                            <a class="thumbnail" id="carousel-selector-'.$primeiro.'"><img src="'. base_url('uploads/linha/' . $row->id) . '/' . $row->nome_arquivo . $row->extensao .'"></a>
//                                        </li>
//                                    </ul>';
//                                    $primeiro= $primeiro + 1;
//                                } 
                                
                          $html =  $html . '
                            <div class="col-sm-12">
                                <div class="col-xs-12" id="slider">
                                    <!-- Top part of the slider -->
                                    <div class="row">
                                        <div class="modal-carousel">
                                            <div class="col-sm-12" id="carousel-bounding-box">
                                                <div class="carousel slide" id="carouselservico">
                                                    <div class="carousel-inner">';

                                                        $primeiro = (int)0;
                                                     foreach ($servico_detalhes as $row){

                                                         if ($primeiro==0){
                                                            $html =  $html . '
                                                                <div class="active item" data-slide-number="'. $primeiro. '">
                                                                    <img src="'. base_url('uploads/servico/' . $row->servico_id) . '/' . $row->nome_arquivo . $row->extensao .'">
                                                                </div>
                                                            ';
                                                            $primeiro= $primeiro + 1;
                                                         } else {
                                                           $html =  $html . '
                                                                <div class="item" data-slide-number="'. $primeiro. '">
                                                                    <img src="'. base_url('uploads/servico/' . $row->servico_id) . '/' . $row->nome_arquivo . $row->extensao .'">
                                                                </div>
                                                            ';  
                                                             $primeiro= $primeiro + 1;

                                                     } }
                                                    $html =  $html . '</div>
                                                        <a class="left carousel-control" href="#carouselservico" role="button" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                    </a>
                                                    <a class="right carousel-control" href="#carouselservico" role="button" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>';
                          
    echo $html;
}
}
