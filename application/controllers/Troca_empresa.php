<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Acessorios class.
 * 
 * @extends CI_Controller
 */
class Troca_empresa extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
    }



    public function Trocar($novo_id) {
        $this->load->model('unidade_negocio_model');
        $unidade_desc = $this->unidade_negocio_model->retorna_Unidade_negocio($novo_id);
        $_SESSION['uni_negocio_id'] = $novo_id;
        $_SESSION['uni_negocio_desc'] = $unidade_desc->descricao;
        
        $_SESSION['idioma'] = $unidade_desc->idioma;
        if ( $_SESSION['idioma']==1){
            $_SESSION['pasta_gps'] = "GPS";
        }
        else
        {
            $_SESSION['pasta_gps'] = "GPS_en";                        
        };
        // user login ok
        $this->load->template('dashboard', $data);
    }

   

}
