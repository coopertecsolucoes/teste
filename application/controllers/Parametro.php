<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Parametro extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Parametro_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Parametro_model->retorna_parametros();
        $data->page_title = 'Parametro';
        $this->load->template('parametro/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Parametro';
        $this->load->template('parametro/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Parametro_model->retorna_parametro($id);
        $data->page_title = 'Parametro';
        $this->load->template('parametro/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_parametro() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Parametro';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('parametro/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->valor = $this->input->post('valor');
            $class->ativo = $this->input->post('ativo');

            if ($this->Parametro_model->create_parametro($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('parametro/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('parametro/create');
            }
        }
    }

    public function update_parametro() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            //$data->result = $this->Parametro_model->retorna_Tipomenu($id);
            $data->page_title = 'Parâmetro';
            $this->load->template('parametro/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->valor = $this->input->post('valor');
            $class->ativo = $this->input->post('ativo');

            if ($this->Parametro_model->update_parametro($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                //$data->result = $this->Parametro_model->retorna_Tipomenu($id);
                redirect('parametro/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('parametro/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Parametro_model->delete_parametro($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('parametro/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('parametro/index');
            
        }
    }
    
    
 
}

