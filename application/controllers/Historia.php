<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Historia extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Historia_model');
        
    }

    public function index() {
        $data = new stdClass();
        
        $data->result = $this->Historia_model->retorna_historias($_SESSION["uni_negocio_id"]);
        $this->load->template('historia/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->load->template('historia/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        
        $data->result = $this->Historia_model->retorna_historia($id);
        $this->load->template('historia/edit', $data);
    }

    
    
    
    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_historia() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Painel Site';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('historia/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');   
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
//            $class->sombra = $this->input->post('sombra');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');
            //UPLOAD DO LOGO
            
            if ($this->Historia_model->create_historia($class)) {
                // OK
                $idret = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('historia/edit/' . $idret);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('historia/create');
            }
        }
    }

    public function update_historia() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            //$data->result = $this->Linha_model->retorna_Tipomenu($id);
            $data->page_title = 'Painel Site';
            $this->load->template('historia/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');  
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
            $class->ordem = $this->input->post('ordem');
//             $class->sombra = $this->input->post('sombra');
            $class->ativo = $this->input->post('ativo');
            //UPLOAD DO LOGO
            if (!is_dir('./uploads/historia/' . $id)) {
                mkdir('./uploads/historia/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/historia/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('historia/edit/' . $id);
                } else {
                    $class->banner = 'uploads/historia/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->Historia_model->update_historia($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso');
                //$data->result = $this->Linha_model->retorna_Tipomenu($id);
                redirect('historia/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro, Por favor, tente novamente.');
                redirect('historia/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Historia_model->delete_historia($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('historia/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('historia/index');
            
        }
    }
    
    public function update_imagem() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        $principal = $this->input->post('principal');
        // set validation rules
        $this->form_validation->set_rules('id', 'ID Painel', 'required');
        $this->form_validation->set_rules('principal', 'Principal', 'required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Historia_model->retorna_historia($id);
            $data->page_title = 'Painel - Mensagens';
            redirect('historia/imagens/' . $id);
        } else {

            // set variables from the form
            $class = new stdClass();

            if (!is_dir('./uploads/historia/' . $id)) {
                mkdir('./uploads/historia/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/historia/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('historia/imagens/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;
                }
            }
            // fim upload
            // insere foto no BD
            $this->load->model('Historia_foto_model');
            $class->historia_id = $id;
            $class->principal = $this->input->post('principal');
            
            if (!$this->Historia_foto_model->create_historia_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
               
            }
             redirect('historia/imagens/' . $id);
 
        }
    }
    
    public function delete_imagem($id) {
        $this->load->model('Historia_foto_model');
        $foto = $this->Historia_foto_model->retorna_historia_foto($id);
        if ($this->Historia_foto_model->delete_historia_foto($foto->id)) {
            unlink("./uploads/historia/" . $foto->historia_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('historia/imagens/' . $foto->historia_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('historia/imagens/' . $foto->historia_id);
        }
    }
}
