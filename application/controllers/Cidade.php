<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Cidade extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Cidade_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Cidade_model->retorna_cidades($_SESSION["uni_negocio_id"]);
        $data->page_title = 'Cidades';
        $this->load->template('cidade/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Cidade';
        $this->load->model('Estado_model');
        $data->estados = $this->Estado_model->retorna_estado_ativos($_SESSION['uni_negocio_id']); 
        $this->load->template('cidade/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $this->load->model('Estado_model');
        $data->estados = $this->Estado_model->retorna_estado_ativos($_SESSION['uni_negocio_id']);  
        $data->result = $this->Cidade_model->retorna_cidade($id);
        $data->page_title = 'Cidade';
        $this->load->template('cidade/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_cidade() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Cidade';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            redirect('cidade/create');
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->estado_id = $this->input->post('estado_id');   
            $class->descricao = $this->input->post('descricao');   
            $class->ativo = $this->input->post('ativo');   
            
            if ($this->Cidade_model->create_cidade($class)) {
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('cidade/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('cidade/create');
            }
        }
    }

    public function update_cidade() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            //$data->result = $this->Cidade_model->retorna_Tipomenu($id);
            $data->page_title = 'Cidade';
            $this->load->template('cidade/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id= $id;
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->estado_id = $this->input->post('estado_id');   
            $class->descricao = $this->input->post('descricao');   
            $class->ativo = $this->input->post('ativo');   
            if ($this->Cidade_model->update_cidade($class)) {
                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('cidade/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('cidade/edit/' . $id);
            }
        }
    }
    
    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Cidade_model->delete_cidade($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('cidade/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('cidade/index');
            
        }
    }
    
    
}

