<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Unidade_negocio class.
 * 
 * @extends CI_Controller
 */
class Unidade_negocio extends MY_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
 
    public function __construct() {

        parent::__construct();
        $this->load->model('Unidade_negocio_model');
        
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Unidade_negocio_model->retorna_Unidade_negocios();
        $data->page_title = 'Unidade Negócio';
        //$data->unidades = $this->data['unidades'];
        $this->load->template('unidade_negocio/index', $data);
    }

    public function create() {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->page_title = 'Unidade Negócio';
       
        $this->load->template('unidade_negocio/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $this->load->model('Usuario_grupo_model');
        $data->result = $this->Unidade_negocio_model->retorna_Unidade_negocio($id);
        $data->page_title = 'Unidade Negócio';
        //$data->unidades = $this->data['unidades'];
        $this->load->template('unidade_negocio/edit', $data);
    }
    
    public function imagens($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Unidade_negocio_model->retorna_unidade_negocio($id);
        $this->load->model('Unidade_negocio_foto_model');
        $data->fotos = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_fotos($id);
        $this->load->template('unidade_negocio/imagens', $data);
    }

    public function update_Unidade_negocio() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('nome_fantasia', 'nome_fantasia', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Unidade_negocio_model->retorna_Unidade_negocio($id);
            $data->page_title = 'Unidade Negócio';
            $this->load->template('unidade_negocio/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            $class->telefone = $this->input->post('telefone');
            $class->celular = $this->input->post('celular');
            $class->email_contato = $this->input->post('email_contato');
            $class->cnpj = $this->input->post('cnpj');
            $class->cidade = $this->input->post('cidade');
            $class->endereco = $this->input->post('endereco');
            $class->bairro = $this->input->post('bairro');
            $class->cep = $this->input->post('cep');
            $class->titulo_sobre = $this->input->post('titulo_sobre');
            $class->texto_sobre = $this->input->post('texto_sobre');
            $class->localizacao_titulo = $this->input->post('localizacao_titulo');
            $class->localizacao = $this->input->post('localizacao');
            $class->galeria_titulo = $this->input->post('galeria_titulo');
            $class->galeria_texto = $this->input->post('galeria_texto');
            $class->servico_titulo = $this->input->post('servico_titulo');
            $class->servico_texto = $this->input->post('servico_texto');
            $class->noticia_titulo = $this->input->post('noticia_titulo');
            $class->noticia_texto = $this->input->post('noticia_texto');
            $class->ativo = $this->input->post('ativo'); 
            $class->contato_titulo = $this->input->post('contato_titulo');
            $class->fanpage = $this->input->post('fanpage');        
            $class->instagram = $this->input->post('instagram');        
            $class->skype = $this->input->post('skype');        
            $class->blog_titulo = $this->input->post('blog_titulo');
            $class->blog_texto = $this->input->post('blog_texto');
            $class->cliente_titulo = $this->input->post('cliente_titulo');
            $class->cliente_texto = $this->input->post('cliente_texto');
            
            $class->topico1 = $this->input->post('topico1');
            $class->topico1_texto = $this->input->post('topico1_texto');
            $class->topico2 = $this->input->post('topico2');
            $class->topico2_texto = $this->input->post('topico2_texto');
            $class->topico3 = $this->input->post('topico3');
            $class->topico3_texto = $this->input->post('topico3_texto');
            
            $class->msg_whats = $this->input->post('msg_whats');
            //UPLOAD DO LOGO
            if (!is_dir('./uploads/empresa/' . $id)) {
                mkdir('./uploads/empresa/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/empresa/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('unidade_negocio/edit/' . $id);
                } else {
                    $class->logo = 'uploads/empresa/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->Unidade_negocio_model->update_unidade_negocio($class)) {

                $data->alerta_sucesso = 'Sucesso ao gravar';
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                $data->result = $this->Unidade_negocio_model->retorna_Unidade_negocio($id);
                redirect('unidade_negocio/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('unidade_negocio/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Unidade_negocio_model->delete_unidade_negocio($id)) {
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('unidade_negocio/index');
        } else {
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('unidade_negocio/index');
        }
    }

    
    
   public function update_imagem() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        $this->load->model('Unidade_negocio_foto_model');
        $id = $this->input->post('id');

        $this->form_validation->set_rules('id', 'Unidade de Negócio', 'required');
      
        if ($this->form_validation->run() === false) {
            $data->result = $this->Unidade_negocio_model->retorna_unidade_negocio($id);
            redirect('unidade_negocio/imagens/' . $id);
        } else {
           $class = new stdClass();
            if (!is_dir('./uploads/empresa_img/' . $id)) {
                mkdir('./uploads/empresa_img/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/empresa_img/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '100000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('unidade_negocio/imagens/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;
                    $this->load->library('image_lib', $config2);
                }
            }
            $class->unidade_negocio_id = $id;
            $class->principal = 0;
            if (!$this->Unidade_negocio_foto_model->create_unidade_negocio_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                
            }
            redirect('unidade_negocio/imagens/' . $id);
        }
    }
    
    public function ativa_principal($unidade_negocio_foto_id) {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        
        $this->load->model('Unidade_negocio_foto_model');
        $foto = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_foto($unidade_negocio_foto_id);
        
        $this->Unidade_negocio_foto_model->ativa_foto_principal($foto->unidade_negocio_id,$unidade_negocio_foto_id);
        redirect('unidade_negocio/imagens/' . $foto->unidade_negocio_id);
    }
    
    public function delete_imagem($id) {
        $this->load->model('Unidade_negocio_foto_model');
        $foto = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_foto($id);
        if ($this->Unidade_negocio_foto_model->delete_unidade_negocio_foto($foto->id)) {
            unlink("./uploads/empresa_img/" . $foto->unidade_negocio_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('unidade_negocio/imagens/' . $foto->unidade_negocio_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('unidade_negocio/imagens/' . $foto->unidade_negocio_id);
        }
    }
}
