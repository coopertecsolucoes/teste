<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Tipo_Menu extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Tipo_menu_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Tipo_menu_model->retorna_Tipomenus();
        $data->page_title = 'Tipo de Menu';
        $this->load->template('tipo_menu/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Tipo de Menu - Novo';
        $this->load->template('tipo_menu/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Tipo_menu_model->retorna_Tipomenu($id);
        $data->page_title = 'Tipo de Menu - Nova';
        $this->load->template('tipo_menu/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_tipo_menu() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Tipo de Menu';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('tipo_menu/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');     
            $class->ordem= $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');

            if ($this->Tipo_menu_model->create_Tipomenu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('tipo_menu/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('tipo_menu/create');
            }
        }
    }

    public function update_tipo_menu() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Tipo_menu_model->retorna_Tipomenu($id);
            $data->page_title = 'Tipo de Menu- Edit';
            $this->load->template('tipo_menu/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->descricao = $this->input->post('descricao');
            $class->descricao_en = $this->input->post('descricao_en');
            $class->ordem= $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');

            if ($this->Tipo_menu_model->update_Tipomenu($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                $data->result = $this->Tipo_menu_model->retorna_Tipomenu($id);
                redirect('tipo_menu/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('tipo_menu/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Tipo_menu_model->delete_Tipomenu($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('tipo_menu/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('tipo_menu/index');
            
        }
    }
}
