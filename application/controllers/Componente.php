<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Estado extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Estado_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Estado_model->retorna_estados($_SESSION["uni_negocio_id"]);
        $data->page_title = 'Estados - UF';
        $this->load->template('estado/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Estado - UF';
        $this->load->template('estado/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Estado_model->retorna_estado($id);
        $data->page_title = 'Estado - UF';
        $this->load->template('estado/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_estado() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Estado - UF';

        // set validation rules
        $this->form_validation->set_rules('sigla', 'Sigla', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            redirect('estado/create');
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->sigla = strtoupper($this->input->post('sigla'));    
            $class->descricao = $this->input->post('descricao');   
            $class->ativo = $this->input->post('ativo');   
            
            if ($this->Estado_model->create_estado($class)) {                
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('estado/create');
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('estado/create');
            }
        }
    }

    public function update_estado() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('sigla', 'sigla', 'trim|required');

        if ($this->form_validation->run() === false) {
            $data->page_title = 'Estado - UF';
            $this->load->template('estado/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $id;
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->sigla = strtoupper($this->input->post('sigla'));   
            $class->descricao = $this->input->post('descricao');   
            $class->ativo = $this->input->post('ativo');   
            if ($this->Estado_model->update_estado($class)) {
                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('estado/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('estado/edit/' . $id);
            }
        }
    }
    
    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Estado_model->delete_estado($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('estado/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('estado/index');
            
        }
    }
    
    
}

