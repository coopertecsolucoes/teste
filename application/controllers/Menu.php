<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Menu extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Menu_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Menu_model->retorna_menus();
        $data->page_title = 'Menu';
        $this->load->template('menu/index', $data);
    }

    public function create() {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->page_title = 'Menu';
        $this->load->model('Tipo_menu_model');
        $data->tipo_menus = $this->Tipo_menu_model->retorna_tipomenus();
        $this->load->template('menu/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $this->load->model('Menu_model');
        $this->load->model('Tipo_menu_model');
        $data->tipo_menus = $this->Tipo_menu_model->retorna_tipomenus();
        $data->result = $this->Menu_model->retorna_menu($id);
        $data->page_title = 'Menu';
        $this->load->template('menu/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_menu() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Menu';

        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            $this->load->template('menu/create', $data);
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');
            $class->descricao_en = $this->input->post('descricao_en');
            $class->menu = $this->input->post('menu');
            $class->tipo_menu_id = $this->input->post('tipo_menu_id');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');
            if ($this->Menu_model->create_menu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('menu/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('menu/create');
            }
        }
    }

    public function update_menu() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');
       
        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('descricao', 'Description', 'trim|required');
        

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            $data->result = $this->Menu_model->retorna_menu($id);
            $data->page_title = 'Menu - Edit';
            $this->load->template('menu/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->descricao = $this->input->post('descricao');
            $class->descricao_en = $this->input->post('descricao_en');
            $class->menu = $this->input->post('menu');
            $class->tipo_menu_id = $this->input->post('tipo_menu_id');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = $this->input->post('ativo');


            if ($this->Menu_model->update_menu($class)) {

                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                $data->result = $this->Menu_model->retorna_menu($id);
                redirect('menu/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('menu/edit/' . $id);
            }
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Menu_model->delete_menu($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('menu/index');
        } else {

            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('menu/index');
        }
    }

}
