<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Contato extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Contato_model');
    }

    public function index() {
        $data = new stdClass();
        $data->result = $this->Contato_model->retorna_contatos($_SESSION["uni_negocio_id"]);
        $data->page_title = 'Onde nos encontrar?';
        $this->load->template('contato/index', $data);
    }

    public function create() {
        $this->load->library('form_validation');
        $data->page_title = 'Onde nos encontrar?';
      
        $this->load->template('contato/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
       
        $data->result = $this->Contato_model->retorna_contato($id);
        $data->page_title = 'Onde nos encontrar?';
        $this->load->template('contato/edit', $data);
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function create_contato() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');


        // set validation rules
        $this->form_validation->set_rules('nome', 'nome', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            redirect('contato/create');
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome'); 
            
            $class->mensagem = $this->input->post('mensagem');   
            $class->email = $this->input->post('email');   
            $class->telefone = $this->input->post('celular');   
            $class->assunto = $this->input->post('assunto');   
            $class->observacoes = $this->input->post('observacoes');  
            $dt_data =DateTime::createFromFormat('d/m/Y',  $this->input->post('data'));
            if ($dt_data == false) {
                $date = null;
            } else {
                $date = $dt_data->format('Y-m-d');
            }            
            $class->data = empty($date) ? NULL : $date;            
            $class->status = 1;
            //Salva a contato tecnica
            if ($this->Contato_model->create_contato($class)) {
                $retid = $this->db->insert_id();
               
                // OK
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('contato/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('contato/create');
            }
        }
    }

    public function update_contato() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        // set validation rules
        $this->form_validation->set_rules('nome', 'nome', 'trim|required');

        if ($this->form_validation->run() === false) {

            // validation not ok, send validation errors to the view
            //$data->result = $this->Contato_model->retorna_Tipomenu($id);
            $data->page_title = 'Tipo de Menu- Edit';
            $this->load->template('contato/edit', $data);
        } else {

            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');   
            $class->equipamento = $this->input->post('equipamento');   
            $class->email = $this->input->post('email');   
            $class->telefone = $this->input->post('telefone');   
            $class->cidade = $this->input->post('cidade');   
            $class->observacoes = $this->input->post('observacoes');  
         
            if ($this->Contato_model->update_contato($class)) {
                // user creation ok
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('contato/edit/' . $id);
            } else {
                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                redirect('contato/edit/' . $id);
            }
        }
    }
    
    
    public function encerrar($contato_id){
        
        $class = new stdClass();
        $class->id = $contato_id;  
        $class->observacoes = $this->input->post('parecer');
     
        $class->status = 2;
  
        if($this->Contato_model->update_contato($class))
        {
            $this->session->set_flashdata('alerta_sucesso','Contato encerrado com sucesso!');
             redirect('contato/edit/'. $contato_id);
        }
        else
        {
            $this->session->set_flashdata('alerta_erro','Ocorreu algum problema, tente novamente');
            redirect('contato/edit/'. $contato_id);
        }        
    }
    
    public function reabrir($contato_id){
        
        $class = new stdClass();
        $class->id = $contato_id;  
        $class->observacoes = $this->input->post('observacoes');
       
        $class->status = 1;
  
        if($this->Contato_model->update_contato($class))
        {
            $this->session->set_flashdata('alerta_sucesso','Contato reaberto com sucesso!');
             redirect('contato/edit/'. $contato_id);
        }
        else
        {
            $this->session->set_flashdata('alerta_erro','Ocorreu algum problema, tente novamente');
            redirect('contato/edit/'. $contato_id);
        }        
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Contato_model->delete_contato($id)) {

            // user creation ok
            $data->alerta_sucesso = 'Sucesso ao gravar';
            redirect('contato/index');
        } else {
            
            // user creation failed, this should never happen
            $data->error = 'Erro! Por favor, tente novamente.';
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('contato/index');
            
        }
    }
    
     public function create_contato_site() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data->page_title = 'Solicitação de Contato';

        // set validation rules
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');

        if ($this->form_validation->run() === false) {
            // validation not ok, send validation errors to the view
            redirect('contato/create');
        } else {
            // set variables from the form
            $class = new stdClass();
            $class->unidade_negocio_id= $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');  
            $class->assunto = $this->input->post('assunto');  
            $class->mensagem = $this->input->post('mensagem'); 
            $class->email = $this->input->post('email');   
            $class->telefone = $this->input->post('telefone');       
            $class->data = date("Y-m-d");            
            $class->status = 1;
            //Salva a contato tecnica
            if ($this->Contato_model->create_contato($class)) {
                  
                $retID = $this->db->insert_id();
                // OK
                $this->enviar_contato_cliente($retID);
                $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
                redirect('site/index');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
                // send error to the view
                redirect('site/index');
            }
        }
    }

    public function enviar_contato_cliente($contato_id){
        
        $this->load->library('email');
        $this->load->model('Unidade_negocio_model');
        $unidade = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        $contato = $this->Contato_model->retorna_contato($contato_id);
        
        // Define remetente e destinatário
        $this->email->from($unidade->email_contato, $unidade->descricao); // Remetente
        $this->email->to($unidade->email_contato, 'Contato pelo Site'); // Destinatário
       
        // Define o assunto do email
        $this->email->subject('Contato pelo Site - ' . $contato_id);
        
        $corpo = "Solicitação de Contato - " . $contato_id
                . "<br /> Nome: " . $contato->nome
                . "<br /> Telefone: " . $contato->telefone
                . "<br /> Email: " . $contato->email
                . "<br /> Assunto: " . $contato->assunto
                . "<br /> Mensagem: " . $contato->mensagem;
        // Define o corpo do email com a mensagem
        $this->email->message($corpo);
        
        // Envia o email
        $this->email->send();
        $this->session->set_flashdata('alerta_erro',$this->email->print_debugger());        
        
    }
}

