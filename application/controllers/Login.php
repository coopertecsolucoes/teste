<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Login extends MY_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->model('Painel_site_model');
    }

    public function index() {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data->page_title = 'Painel de Controle';
        
        
        //Busca mensagens do Painel
        //$data->painel = $this->Painel_site_model->retorna_painel_site_ativas(1);
        
        $this->load->template_site('site/grandesmarcas', $data);
        //redirect('site/site');
    }

    /**
     * login function.
     * 
     * @access public
     * @return void
     */
    public function adm() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['page_title'] = 'Login';
        $this->load->view('login/login', $data);
        //redirect('site/site');
    }
    public function login() {

        // create the data object
        $data = new stdClass();
        $data->page_title = 'Home';
        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('usuario_unidade_model');
        $this->load->model('unidade_negocio_model');

        // set validation rules
        $this->form_validation->set_rules('usuario', 'User', 'required');
        $this->form_validation->set_rules('senha', 'Password', 'required');

    
        if ($this->form_validation->run() == false) {

            // validation not ok, send validation errors to the view
            redirect('login/adm');
        } else {

            // set variables from the form
            $username = $this->input->post('usuario');
            $password = $this->input->post('senha');

            if ($this->usuario_model->valida_login($username, $password)) {

                $user_id = $this->usuario_model->retorna_id_por_usuario($username);
                $user = $this->usuario_model->retorna_usuario($user_id);
               
                if ($user->ativo == 0) {
                    // login failed
                    $this->session->set_flashdata('erro_login', 'User não está ativo.');
                    // send error to the view
                     redirect('/');
                } else {

                    // set session user datas
                    $_SESSION['usuario_id'] = (int) $user->id;
                    $_SESSION['pessoa_id'] = (int) $user->pessoa_id;
                    $_SESSION['usuario'] = (string) $user->usuario;
                    $_SESSION['nome'] = (string) $user->nome;
                    $_SESSION['logged_in'] = (bool) true;
                    $_SESSION['is_confirmed'] = (bool) $user->ativo;
                    $_SESSION['is_admin'] = (bool) ($user->tipo == "A" ? TRUE : FALSE);
                    $user_uni= $this->usuario_unidade_model->retorna_usuario_unidades($user->id);
                    $_SESSION['unidades'] = $user_uni;
                    
                    $_SESSION['logo'] = $user_uni->logo;
                    
                    $unidade_padrao = $this->usuario_unidade_model->retorna_id_unidade_padrao($user->id);
                    $_SESSION['uni_negocio_id'] = (int)$unidade_padrao->unidade_negocio_id;
                    $unidade_desc = $this->unidade_negocio_model->retorna_unidade_negocio($unidade_padrao->unidade_negocio_id);
                    $_SESSION['uni_negocio_desc'] = $unidade_desc->descricao;
                    
                                    
     
                    //Carrega os Menus do Usuario
                    $_SESSION['menus'] = $this->usuario_model->retorna_menus($user_id);
                 
                    // user login ok
                    $this->load->template('dashboard', $data);
                   
                }
            } else {

                // login failed
                $this->session->set_flashdata('erro_login', 'Login ou senha incorretos.');
                // send error to the view
                 redirect('login/login');
            }
        }
    }

    
    public function login_site() {

        // create the data object
        $data = new stdClass();
        $data->page_title = 'Home';
        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('usuario_unidade_model');
        $this->load->model('unidade_negocio_model');
        

        // set validation rules
        $this->form_validation->set_rules('usuario', 'User', 'required');
        $this->form_validation->set_rules('senha', 'Password', 'required');

    
        if ($this->form_validation->run() == false) {

            // validation not ok, send validation errors to the view
            redirect('/');
        } else {

            // set variables from the form
            $username = $this->input->post('usuario');
            $password = $this->input->post('senha');

            if ($this->usuario_model->valida_login($username, $password)) {

                $user_id = $this->usuario_model->retorna_id_por_usuario($username);
                $user = $this->usuario_model->retorna_usuario($user_id);
               
                if ($user->ativo == 0) {
                    // login failed
                    $this->session->set_flashdata('erro_login', 'User não está ativo.');
                    // send error to the view
                     redirect('/');
                } else {

                    // set session user datas
                    $_SESSION['usuario_id'] = (int) $user->id;
                    $_SESSION['pessoa_id'] = (int) $user->pessoa_id;
                    $_SESSION['usuario'] = (string) $user->usuario;
                    $_SESSION['nome'] = (string) $user->nome;
                    $_SESSION['logged_in'] = (bool) true;
                    $_SESSION['is_confirmed'] = (bool) $user->ativo;
                    $_SESSION['is_admin'] = (bool) ($user->tipo == "A" ? TRUE : FALSE);
                    $_SESSION['unidades'] = $this->usuario_unidade_model->retorna_usuario_unidades($user->id);

                    $unidade_padrao = $this->usuario_unidade_model->retorna_id_unidade_padrao($user->id);
                    $_SESSION['uni_negocio_id'] = (int)$unidade_padrao->unidade_negocio_id;
                    $unidade_desc = $this->unidade_negocio_model->retorna_Unidade_negocio($unidade_padrao->unidade_negocio_id);
                    $_SESSION['uni_negocio_desc'] = $unidade_desc->descricao;
                    
                    
                    $_SESSION["condicao_pagamento_id"]= $pessoa->condicao_pagamento_id;        
 
     
                    //Carrega os Menus do Usuario
                    $_SESSION['menus'] = $this->usuario_model->retorna_menus($user_id);
                    
                    // user login ok
                    //$this->load->template('dashboard', $data);
                   redirect('site/index');
                }
            } else {

                // login failed
                $this->session->set_flashdata('erro_login', 'Login ou senha incorretos.');
                // send error to the view
                 redirect('/');
            }
        }
    }
     public function home() {

        // create the data object
        $data = new stdClass();
        $data->page_title = 'Home';
        $this->load->template('dashboard', $data);
       
    }
    /**
     * logout function.
     * 
     * @access public
     * @return void
     */
    public function logout() {

        // create the data object
        $data = new stdClass();

        if (isset($_SESSION['usuario_id']) && $_SESSION['logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
             redirect('login/login');
        } else {

            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect('/');
        }
    }

}
