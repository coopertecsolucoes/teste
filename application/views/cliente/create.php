<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cliente <small>Novo Cliente</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('cliente/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong>Novo Cliente</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('cliente/create') ?>" ><i class="fa fa-plus-circle"></i> Nova Foto</a></li>
                        <br />
                        <form id="formulario" method="POST" enctype="multipart/form-data" action="<?php echo base_url("cliente/create_restaurante"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                               
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição Interna 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao"  class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="titulo">Título 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="titulo" name="titulo"  class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="titulo">Link 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="link" name="link"  class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ordem">Ordem <span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="ordem" name="ordem" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>    

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ativo">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>                                    
                                    <a href="<?php echo site_url('cliente/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>
</div>

</div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
       

    });

</script>
<!-- /page content -->
