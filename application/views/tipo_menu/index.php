<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Menu Type<small>Menu Type List</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li class="active">
                                <strong>List</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('tipo_menu/create') ?>" ><i class="fa fa-plus-circle"></i> New Menu Type</a></li>
                        <br />
                        <table id="example" class="display responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Description</th>                                   
                                    <th>Order Position</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($result)) {
                                    foreach ($result as $row) {
                                        if ($row->ativo == 1) {
                                            $ativo = 'Yes';
                                        } else {
                                            $ativo = 'No';
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $row->descricao; ?></td>                                           
                                            <td><?php echo $row->ordem; ?></td>
                                            <td><?php echo $ativo; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('tipo_menu/edit/' . $row->id) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                <button id="btn_delete" onclick="deleteConfirm('<?php echo site_url('tipo_menu/delete/' . $row->id) ?>')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<script src="<?php echo base_url("assets/js/datatables/DataTables-1.10.11/js/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/DataTables-1.10.11/js/dataTables.bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/Responsive-2.0.2/js/dataTables.responsive.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/Responsive-2.0.2/js/responsive.bootstrap.min.js"); ?>"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            paging: true,
            ordering: true,
            searching: true,
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
    });
    function deleteConfirm(url) {
        bootbox.confirm("Are you sure you want to delete?", function (res) {
            if (res === true) {
                window.location = url;
            } else {
                return res;
            }
        });
    }
</script>
