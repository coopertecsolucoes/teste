
<!DOCTYPE HTML>
<html>
    <head>
    <title>Tal Propaganda</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <link href="<?php echo base_url("assets/modelo_site/css/css/style.css")?>" rel="stylesheet" type="text/css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/slider.css")?>" />
        
        
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/magnific-popup1.css")?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/magnific-popup.css")?>">
    <link href="<?php echo base_url("assets/modelo_site/css/portfolio.css")?>" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/lightbox.css")?>" media="screen" />


    <link href="<?php echo base_url("assets/modelo_site/css/header_style1.css")?>" rel="stylesheet" type="text/css" media="all" />
       
    </head>
    <body>
                    <!-- start slider -->
        <div class="slider_bg s7">
        <!-- start main -->
				<!---start-da-slider----->
				<div id="da-slider" class="da-slider">
				<div class="da-slide">
					<h2 class="title"></h2>
                    <h3 class="title"></h3>
                    <p class="description"></p>  
				</div>
				<div class="da-slide">
					<h2 class="title"></h2>
                    <h3 class="title"></h3>
                    <p class="description"></p>  </div>
				<div class="da-slide">
					<h2 class="title">TAL</h2>
                    <h3 class="title">PROPAGANDA</h3>
                    <p class="description">Propagando & DESIGN</p>   
				</div>
				<div class="da-slide">
					<h2 class="title">TAL</h2>
                    <h3 class="title">PROPAGANDA</h3>
                    <p class="description">Propagando & DESIGN</p>   
				</div>	
				<div class="da-slide">
					<h2 class="title">TAL</h2>
                    <h3 class="title">PROPAGANDA</h3>
                    <p class="description">Propagando & DESIGN</p>  
				</div>
			</div>
 			<!---//End-da-slider----->
        </div>
 <a  href="#services" class="button scroll"><img src="images/arrow.png"></a>	
 	<!-- start header -->
 	<div class="header">
 		<div class="logo">
 			<a  href="#s7" class="scroll"><img src="images/logo.jpg"></a>
 		</div>
		<!-- start top-nav-->
		<div class="h_right">	
		<div class="subMenu" >
			<div class="wrap">
		 	 <div class="inner">
		 		<a href="#" id="sTop" class="subNavBtn">Serviços</a> 
				<a href="#" id="s1" class="subNavBtn">Projetos</a>
				<a href="#" id="s2" class="subNavBtn">Sobre</a>				
				<a href="#" id="s6" class="subNavBtn end">Contato</a>
			  </div>
			   <!-- /.navbar-collapse -->
	             	<a id="s7" class="right-msg subNavBtn msg-icon"href="#"><span> </span></a>
	                <div class="clearfix"> </div>		
	    </div>	
	   </div>
			<!-- start smart_nav * -->
		 	<nav class="nav">
	            <ul class="nav-list">
	                <li class="nav-item"><a href="#services">Serviços</a></li>
	                 <li class="nav-item"><a href="#work" class="scroll">Projetos</a></li>
	                  <li class="nav-item"><a href="#about" class="scroll">Sobre</a></li>
	                   <li class="nav-item"><a href="#blog" class="scroll">Contato</a></li>
	                <div class="clear"></div>
	            </ul>
	    	</nav>
	  
			<!-- end smart_nav * -->
		</div>
		<div class="clear"></div>
	  </div>
<!---//End-header--->	
<!--- services --->	
     <div class="services sTop" id="services">
		  	<div class="wrap">
		  		<h3>Serviços</h3>
		  		<p>Proin iaculis purus consequat sem cure.</p>
				<div class="col_1_of_3 span_1_of_3">
					<img src="images/service1.png" alt="">
					<h4><a href="#">JOB</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<p><a href="#">MORE</a></p>
				</div>
				<div class="col_1_of_3 span_1_of_3">
					<img src="images/service2.png" alt="">
					<h4><a href="#">FEE MENSAL</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<p><a href="#">MORE</a></p>
				</div>
				<div class="col_1_of_3 span_1_of_3">
					<img src="images/service3.png" alt="">
					<h4><a href="#">REDE SOCIAL</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<p><a href="#">MORE</a></p>
				</div>
				 <div class="clear"></div>
   		   </div>
   		 </div>
   		 <div class="portfoliO s1" id="work"><!-- start portfoli1 -->
   		 <div class="wrap">	
		 <h3>Alguns Projetos</h3>
		 <p>Proin iaculis purus consequat sem cure.</p>
		 <div class="container"><!-- start container -->		 
	       <!-- start h_menu -->
				<div class="h_menu">
				<ul class="flexy-menu thick orange">
				                <li class="active"> <span class="filter active" data-filter="app card icon web">VIEW ALL</span></li>
				                <li><span class="filter" data-filter="app  icon">ILLUSTRATION</span></li>
				                <li><span class="filter" data-filter="card ">LOGO</span></li>				            
				                <li><span class="filter" data-filter="icon app card ">THIPOGRAPHY</span></li>
				                <li><span class="filter" data-filter="web card icon">POSTER</span></li>
				</ul>
				</div>	<!-- end h_menu -->
				
			<!-- end smart_nav * -->
						<ul id="filters" class="clearfix">
							<li>
								<span class="filter active" data-filter="app card icon web">VIEW ALL</span>
							</li>
							<li>
								<span class="filter" data-filter="app  icon">ILLUSTRATION</span>
							</li>
							<li>
								<span class="filter" data-filter="card ">LOGO</span>
							</li>
							<li>
								<span class="filter" data-filter="icon app card ">THIPOGRAPHY</span>
							</li>
							<li>
								<span class="filter" data-filter="web card icon">POSTER</span>
							</li>
						</ul>
						
						<div id="portfoliolist">
							<div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port.jpg" alt="Image 2" style="top: 0px;"> </a>
									
								</div>
							</div>						
							<div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port1.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port1.jpg" alt="Image 2" style="top: 0px;"> </a>								
								</div>
							</div>
							<div class="portfolio web mix_all" data-cat="web" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port2.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port2.jpg" alt="Image 2" style="top: 0px;"> </a>							
								</div>
							</div>
							<div class="portfolio card mix_all" data-cat="card" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port3.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port3.jpg" alt="Image 2" style="top: 0px;"> </a>
									
								</div>
							</div>
							<div class="portfolio web mix_all" data-cat="web" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port4.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port4.jpg" alt="Image 2" style="top: 0px;"> </a>
									
								</div>
							</div>
							<div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
								<div class="portfolio-wrapper">
									<a href="images/port5.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port5.jpg" alt="Image 5" style="top: 0px;"> </a>
								
								</div>
							</div>
																
						</div>				
						</div>	
						<div class="img">
							<a href="#"><img src="images/zoom.png" alt=""></a>	
						</div>						
			</div><!-- end container -->
		</div>
  
<!----start-team-members---->
				<div class="experience">
					<div class="wrap"> 
						<div class="ex-head">
							<h3>SOBRE</h3>
							<p>Proin iaculis purus consequat sem cure.</p>
						</div>
						<div class="prog">
								<div class="text">
									<span>JOB</span>
								</div>
								<div class="text_p">
									<span>90%</span>
								</div>
								<div class="clear"></div>
								<div class="progress-bar-container" data-percent="90%">
									<div class="progress-bar" style="width: 90%;">
									</div>
								</div>
							</div>
							<div class="prog1">
								<div class="text">
									<span>FEE MENSAL</span>
								</div>
								<div class="text_p">
									<span>40%</span>
								</div>
								<div class="clear"></div>
								<div class="progress-bar-container" data-percent="90%">
									<div class="progress-bar1" style="width: 40%;">
									</div>
								</div>
							</div>
							<div class="prog2">
								<div class="text">
									<span>REDE SOCIAL</span>
								</div>
								<div class="text_p">
									<span>60%</span>
								</div>
								<div class="clear"></div>
								<div class="progress-bar-container" data-percent="90%">
									<div class="progress-bar2" style="width: 60%;">
									</div>
								</div>
							</div>
					</div>
				</div>
				<!----//End-team-members----> 
			
   		        
   		       <!---- strat-clent---->
		   	    <div class="clients">
					<div class="wrap"> 
						<div class="client-head">
							<h3>Alguns Clientes</h3>											
						</div>
						<!---strat-image-cursuals---->
		                  <div class="t-clients">
		                  	<div class="wrap"> 											 
									<div class="nbs-flexisel-container"><div class="nbs-flexisel-inner"><ul class="flexiselDemo3 nbs-flexisel-ul" style="left: -253.6px; display: block;">					    					    					       
									<li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client1.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client2.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client3.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client4.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client5.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client1.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client2.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 253.6px;"><img src="images/client3.png"></li></ul><div class="nbs-flexisel-nav-left" style="top: 21.5px;"></div><div class="nbs-flexisel-nav-right" style="top: 21.5px;"></div></div></div> 
									<div class="clear"> </div>      
								 
						  		<!---->
						 	 </div>				
						</div>
					</div>					
				</div>
				<!----//End-clients----> 
				  
				
		   		 <!-----cntact---->	
   		 		<div class="contact s6" id="contact">
					<div class="wrap">
						<h3>CONTATO</h3>
							<p>Proin iaculis purus consequat sem cure.</p>	
				        <div class="contact_form">
							 	 	    <form method="post" action="contact-post.html">
										    	<span><input type="text" value="NAME" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'NAME';}"><label><img src="images/con1.png" alt="" /></label></span>									    
										    	<span class="left"><input type="text" value="EMAIL" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'EMAIL';}"><label><img src="images/con2.png" alt="" /></label></span>
										    <div class="clear"></div>
										    	<span><textarea placeholder="MESSAGE"></textarea><label1><img src="images/con3.png" alt="" /></label1></span>
										   		<input type="submit" class="" value="Submit">
									    </form>
						 </div>
					</div>
				</div>
				
				<!-----logos---->	
		   		 <div class="logos"> 
		   		 	<div class="wrap"> 
			   		 	<ul>
			   		 		<li><img src="images/client6.png" alt="" /></li>
			   		 		<li><img src="images/client7.png" alt="" /></li>
			   		 		<li><img src="images/client8.png" alt="" /></li>
			   		 	</ul>
		   		 	</div>
		   		 </div>
				
				<!-----GOOGLE MAP------->
				
		   		 <div class="map">
					<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#666;text-align:left;font-size:12px"></a></small>
			     </div>
				 
				 
		   		 
   		        <!----start-bottom-footer---->
				<div class="bottom-footer">
					<div class="wrap"> 
						<div class="bottom-footer-center">
							<ul class="bottom-social-icons">
								<li><a class="bottom-twitter" href="#"> </a></li>
								<li><a class="bottom-facebook" href="#"> </a></li>
								<li><a class="bottom-pin" href="#"> </a></li>
								<li><a class="bottom-google" href="#"> </a></li>
								<div class="clear"> </div>
							</ul>
						</div>	
						<div class="bottom-footer-left">
							<p><span>&#169; Copyright 2019 By</span> <a target="_blank" href="http://www.coopertecsolucoes.com.br/">Coopertec Soluções</a></p>
						</div>		  
						<div class="clear"> </div>
					</div>
				</div>
				<!----//End-bottom-footer---->


                        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
       </body>
    </html>



        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.min.js")?>"></script>	

        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/modernizr.custom.28468.js")?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.cslider.js")?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.smint.js")?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/move-top.js")?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/easing.js")?>"></script>	
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/flexy-menu.js")?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/responsive.menu.js")?>"></script>	
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/fliplightbox.min.js")?>"></script>
        
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.easing.min.js")?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.mixitup.min.js")?>"></script>
        
        <script src="<?php echo base_url("assets/modelo_site/js/jquery.magnific-popup.js")?>" type="text/javascript"></script>
         <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.lightbox.js")?>"></script>
         
          <!---strat-image-cursuals---->
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.flexisel.js")?>"></script>
								
         
        <script type="text/javascript">

     
	
            $('document').ready(function() {
                
                $('#da-slider').cslider({
                    autoplay	: true,
                    bgincrement	: 450
                });
                
                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear' 
                };
                $().UItoTop({ easingType: 'easeOutQuart' });
                
                $('.popup-with-zoom-anim').magnificPopup({
                                type: 'inline',
                                fixedContentPos: false,
                                fixedBgPos: true,
                                overflowY: 'auto',
                                closeBtnInside: true,
                                preloader: false,
                                midClick: true,
                                removalDelay: 300,
                                mainClass: 'my-mfp-zoom-in'
                });
                                        
                $('.subMenu').smint({
                    'scrollSpeed' : 1000
                });
                
                 $(".flexy-menu").flexymenu({speed: 400,type: "horizontal",align: "right"});

            });
		
                
                    jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	   
                
                
            window.addEventListener("load",function() {
              setTimeout(function(){
                window.scrollTo(0, 1);
              }, 0);
            });
            
            $(window).load(function() {
                $(".flexiselDemo3").flexisel({
                    visibleItems: 5,
                    animationSpeed: 1000,
                    autoPlay: true,
                    autoPlaySpeed: 3000,            
                    pauseOnHover: true,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: { 
                        portrait: { 
                            changePoint:480,
                            visibleItems: 1
                        }, 
                        landscape: { 
                            changePoint:640,
                            visibleItems: 2
                        },
                        tablet: { 
                            changePoint:768,
                            visibleItems: 3
                        }
                    }
                });
            });
		
		
            $('body').flipLightBox();
		
		
            $(function() {
		var filterList = {

                init : function() {

                        // MixItUp plugin
                        // http://mixitup.io
                        $('#portfoliolist').mixitup({
                                targetSelector : '.portfolio',
                                filterSelector : '.filter',
                                effects : ['fade'],
                                easing : 'snap',
                                // call the hover effect
                                onMixEnd : filterList.hoverEffect()
                        });

                },

                hoverEffect : function() {

                        // Simple parallax effect
                        $('#portfoliolist .portfolio').hover(function() {
                                $(this).find('.label').stop().animate({
                                        bottom : 0
                                }, 200, 'easeOutQuad');
                                $(this).find('img').stop().animate({
                                        top : -40
                                }, 500, 'easeOutQuad');
                        }, function() {
                                $(this).find('.label').stop().animate({
                                        bottom : -40
                                }, 200, 'easeInQuad');
                                $(this).find('img').stop().animate({
                                        top : 0
                                }, 300, 'easeOutQuad');
                        });

                        }
                };

                // Run the show!
                filterList.init();

            });
         $(function() {
            $('#portfoliolist a').lightBox();
        });



</script>