<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!--        <?php
        if ($this->session->flashdata('alerta')) {
            ?>    
            <div id="notificacao_sucesso" class="alert bg-success text-white alert-dismissible fade show" >
                
               <?php print $this->session->flashdata('alerta')?> 
            </div>
        
        <script>
            setTimeout(function () {
			$("#notificacao_sucesso").remove();
		}, 7000);
        </script>
        <?php }?>  -->

	<div class="home_bk">
            <div class="background_image" style="background-image:url('<?php echo "../../uploads/servico/" . $apartamento->id . "/". $apartamento->nome_arquivo . $apartamento->extensao ?>')"></div>
            <div class="home_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content text-center">
                                <div class="home_title"><?php echo $apartamento->descricao ?></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>


       <div class="details">
            <div class="container">
                <div class="row">				
                    <!-- Details Image -->
                    <div class="col-xl-7 col-lg-6">
                        <div class="details_image">
                               <div class="home_slider_container">
                <div class="owl-carousel owl-theme home_slider">
                            <?php
                            if (isset($fotos)) {                        
                                foreach ($fotos as $row) { ?>      
                                <!-- Slide -->
                                <div class="slide">
                                    <div class="background_image" style="opacity:1; background-image:url(<?php echo "../../uploads/servico/" . $row->servico_id . "/". $row->nome_arquivo . $row->extensao ?>)"></div>                                    
                                </div>
                            <?php }}?>
                                 </div>
			
                <!-- Home Slider Dots -->
                <div class="home_slider_dots_container">
                    <div class="home_slider_dots">
                        <ul id="home_slider_custom_dots" class="home_slider_custom_dots d-flex flex-row align-items-start justify-content-start">
                            <?php
                                if (isset($fotos)) {
                                    $primeiro = (int)0;  
                                    $cont = (int)1;
                                    foreach ($fotos as $row) {
                                        if ($primeiro==0){
                                            $primeiro=1;                                                            
                            ?>
                                <li class="home_slider_custom_dot active"><?php echo $cont . '.' ?></li>
                            <?php } else {?>
                                <li class="home_slider_custom_dot"><?php echo $cont . '.' ?></li>
                            </li>                                              
                            <?php }
                            $cont=$cont + 1;
                            }}?>    
                        </ul>
                    </div>
                </div>
			
            </div>
                        </div>
                    </div>

                    <!-- Details Content -->
                    <div class="col-xl-5 col-lg-6">
                        <div class="details_content">
                            <div class="details_title"><?php echo $apartamento->descricao ?></div>
                            <div class="details_list">
                                <ul>
                                    <li ><?php echo nl2br($apartamento->texto) ?></li>
                                </ul>
                            </div>
                            <div class="details_long_list">
                                    <ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
                                        <li style="list-style-type:none"><?php echo nl2br($apartamento->detalhe) ?></li>
                                    </ul>
                            </div>
                                <!--<div class="book_now_button"><a href="#">Book Now</a></div>-->
                        </div>
                        
  
                    </div>

                </div>
            </div>
	</div>
	
        
	<!-- Booking -->

	<div class="booking">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="booking_title text-center"><h2><?php echo $empresa->servico_titulo?></h2></div>
                        <!-- Booking Slider -->
                        <div class="booking_slider_container">
                            <div class="owl-carousel owl-theme booking_slider">
                                <?php if (isset($servicos)) {                        
                                foreach ($servicos as $row) { ?>
                                    <!-- Slide -->
                                    <div class="booking_item">
                                        <div class="background_image" style="background-image:url(<?php echo "../../uploads/servico/" . $row->id . "/". $row->nome_arquivo . $row->extensao ?>)"></div>
                                        <div class="booking_overlay trans_200"></div>
                                        <div class="booking_item_content">
                                            <div class="booking_item_list">
                                                <ul>
                                                    <li><?php echo nl2br($row->detalhe)?></li>                                                                   
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <div class="booking_link"><a href="<?php echo base_url('site/servico/' . $row->id)?>">Detalhes</a></div>
                                        <!--<div class="booking_link"><a  onclick="mostradetalhe('<?php echo $row->id?>')">Detalhes</a></div>-->
                                    </div>

                                <?php }} ?>


                                </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>

        
       