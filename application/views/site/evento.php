<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

        <header id="fh5co-header"  >

                  <div class="banner">
                <div id="banner_carrosel" class="carousel slide">
                    <ol class="carousel-indicators">
                        <?php
                        if (isset($painel)) {
                            $pri = (int)0;
                            foreach ($painel as $row) {
                                ?>
                                    <?php 
                                        if($pri==0){
                                    ?>   
                                        <li data-target="#banner_carrosel" data-slide-to="<?php echo $pri; ?>" class="active"></li>
                                     <?php 
                                        $pri = ($pri + 1);
                                        }else {
                                    ?>                                   
                                            <li data-target="#banner_carrosel" data-slide-to="<?php echo $pri; ?>"></li>
                                           
                                        <?php 
                                         $pri = ($pri + 1);
                        }}}
                        ?>        
                    </ol>
                    <div class="carousel-inner">
                        <?php
                        if (isset($painel)) {
                            $primeiro = 0;
                            foreach ($painel as $row) {
                                ?>
                                    <?php 
                                        if($primeiro==0){
                                    ?>   
                                        <div class="item active">
                                     <?php 
                                        $primeiro =1;
                                        }else {
                                    ?>                                   
                                             <div class="item">
                                        <?php 
                                        }
                                    ?>        
                                        <img src=<?php echo base_url($row->banner);?> alt="">
                                        <div class="carousel-caption">
                                          <h1><?php echo $row->titulo; ?></h1>
                                          <h2><?php echo $row->texto; ?> </h2>                                  
                                        </div>
                                    </div>
                                <?php
                            }
                        }
                        ?>
                                
                    </div>

                         <a class="left carousel-control" href="#banner_carrosel" role="button" data-slide="prev">
                            	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                         </a>
                         <a class="right carousel-control" href="#banner_carrosel" data-slide="next">
                             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                         </a>
                    </div>
                   </div>
	</header>

	
	
	<div class="evento">
		<div class="container">
			<h3><?php echo $evento->descricao; ?></h3>
			<hr>
			<div class="col-md-8">	
                            <p><?php echo nl2br($evento->texto); ?></p>
                                <br/>
                                <br/>
                                <a target="_blank" href="<?php echo $evento->link_pagto; ?>" class="btn btn-primary">Inscrever-se</a>
			</div>
			
			<div class="col-md-4">
                            <div class="media">
                                <ul>
                                    <li>
                                        <div class="media-left">
                                            <i class="fa fa-calendar"></i>						
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Data e Horário</h4>
                                            <p><?php echo nl2br($evento->data_horario); ?></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media-left">
                                            <i class="fa fa-map-o"></i>						
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Local</h4>
                                            <p><?php echo nl2br($evento->local); ?></p>
                                        </div>
                                    </li>
                                   
                                        <li>
                                            <div class="media-left">
                                                <i class="fa fa-money"></i>						
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Investimento</h4>
                                                <p><?php echo nl2br($evento->investimento); ?></p>
                                            </div>
                                        </li>
					
                                        <li>
                                            <div class="media-left">
                                                <i class="fa fa-book"></i>						
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Conteúdo</h4>
                                                <p><?php echo nl2br($evento->conteudo); ?></p>
                                            </div>
                                        </li>
				</ul>
                            </div>
			</div>
                        <div class="col-md-12">
                           <div class="media-left">
                                <i class="fa fa-location-arrow"></i>						
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Localização</h4>
                                <div class="col-md-12 ">
                                    <?php echo $evento->mapa; ?>
                                </div>
                            </div>
                        </div>
		</div>
	</div>	
	
	
	