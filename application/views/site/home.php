
<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $empresa->nome_fantasia?></title>
        
        <link href="<?php echo base_url("assets/modelo_site/css/style.css")?>" rel="stylesheet" type="text/css" media="all" />
        <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.min.js")?>"></script>
            <!---strat-slider---->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/slider.css")?>" />
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/modernizr.custom.28468.js")?>"></script>
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.cslider.js")?>"></script>
            <script type="text/javascript">
                    $(function() {
                        $('#da-slider').cslider({
                            autoplay	: true,
                            bgincrement	: 450
                        });
                    });
            </script>
            <!---//strat-slider---->
            <!-- scroll -->
            <script type="text/javascript">
                $(document).ready(function() {
                    var defaults = {
                       containerID: 'toTop', // fading element id
                       containerHoverID: 'toTopHover', // fading element hover id
                       scrollSpeed: 1200,
                       easingType: 'linear' 
                    };
                    $().UItoTop({ easingType: 'easeOutQuart' });
                 });
            </script>
            <!-- //scroll -->	
            <!-- start portfolios -->
            <link href="<?php echo base_url("assets/modelo_site/css/portfolio.css")?>" rel="stylesheet" type="text/css" media="all" />
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/fliplightbox.min.js")?>"></script>
            
            <script type="text/javascript">
                    $('body').flipLightBox()
            </script>
            
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.easing.min.js")?>"></script>
            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.mixitup.min.js")?>"></script>
            <script type="text/javascript">
            $(function() {

                var filterList = {

                        init : function() {

                        // MixItUp plugin
                        // http://mixitup.io
                        $('#portfoliolist').mixitup({
                                targetSelector : '.portfolio',
                                filterSelector : '.filter',
                                effects : ['fade'],
                                easing : 'snap',
                                // call the hover effect
                                onMixEnd : filterList.hoverEffect()
                        });

                    },

                            hoverEffect : function() {

                                    // Simple parallax effect
                                    $('#portfoliolist .portfolio').hover(function() {
                                            $(this).find('.label').stop().animate({
                                                    bottom : 0
                                            }, 200, 'easeOutQuad');
                                            $(this).find('img').stop().animate({
                                                    top : -40
                                            }, 500, 'easeOutQuad');
                                    }, function() {
                                            $(this).find('.label').stop().animate({
                                                    bottom : -40
                                            }, 200, 'easeInQuad');
                                            $(this).find('img').stop().animate({
                                                    top : 0
                                            }, 300, 'easeOutQuad');
                                    });

                            }
                    };

                    // Run the show!
                    filterList.init();

            });
        </script>
	<!-- popup -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/magnific-popup1.css")?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/magnific-popup.css")?>">
        <script src="<?php echo base_url("assets/modelo_site/js/jquery.magnific-popup.js")?>" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
            });
        </script>
        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.lightbox.js")?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/modelo_site/css/lightbox.css")?>" media="screen" />
          <script type="text/javascript">
            $(function() {
                $('#portfoliolist a').lightBox();
            });
           </script>
         <!-- nav -->  
        <script type="text/javascript" 	src="<?php echo base_url("assets/modelo_site/js/jquery.smint.js")?>"></script>
        <script type="text/javascript">
        $(document).ready( function() {
            $('.subMenu').smint({
                'scrollSpeed' : 1000
            });
        });
        </script>
        <script>
        window.addEventListener("load",function() {
          setTimeout(function(){
            window.scrollTo(0, 1);
          }, 0);
        });</script>
    </head>
    <body>
        <!-- start slider -->
    <div class="slider_bg s7">
        <div id="da-slider" class="da-slider">
            <?php
            if (isset($painel)) {                        
                foreach ($painel as $row) { ?>               
                    <div class="da-slide" style="background-image:url(<?php echo base_url( $row->banner) ?>)">
                        <h2 class="title"><?php echo nl2br($row->titulo)?></h2>                        
                        <p class="description"><?php echo nl2br($row->texto)?></p>  
                    </div>                    
            <?php }}?>
        </div> 		
    </div>
    <a  href="#services" class="button scroll"><img src="<?php echo base_url("assets/modelo_site/images/arrow.png")?>"></a>	
 	<!-- start header -->
 	<div class="header">
 		
		<!-- start top-nav-->
            <div class="h_right">	
		<div class="subMenu" >
                    <div class="wrap">
                                <div class="inner">
                                <a href="#" id="sTop" class="subNavBtn">Serviços</a> 
                                <a href="#" id="s1" class="subNavBtn">Projetos</a>
                                <a href="#" id="s2" class="subNavBtn">Sobre</a>				
                                <a href="#" id="s6" class="subNavBtn end">Contato</a>
                             </div>
                        <div class="logo">
                                <a  href="#s7" class="scroll"><img src="<?php echo base_url($empresa->logo)?>"></a>
                              </div>	
                            <div class="clearfix"> </div>		
                      
                    </div>
			<!-- start smart_nav * -->
		<nav class="nav">
	            <ul class="nav-list">
                        <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/responsive.menu.js")?>"></script>
	                <li class="nav-item"><a href="#services">Serviços</a></li>
	                <li class="nav-item"><a href="#work" class="scroll">Projetos</a></li>
	                <li class="nav-item"><a href="#about" class="scroll">Sobre</a></li>
	                <li class="nav-item"><a href="#blog" class="scroll">Contato</a></li>
	                <div class="clear"></div>
	            </ul>
	    	</nav>
	        
            </div>
	<div class="clear"></div>
    </div>
<!---//End-header--->	
<!--- services --->	
        <div class="services sTop" id="services">
            <div class="wrap">
                   <h3><?php echo $empresa->servico_titulo?></h3>
                   <p><?php echo $empresa->servico_texto?></p>
                   
                   <?php if (isset($servicos)) {                        
                        foreach ($servicos as $row) { ?>
                        <div class="col_1_of_3 span_1_of_3">
                            <img src="<?php echo base_url("uploads/servico/" . $row->id . "/". $row->nome_arquivo . $row->extensao) ?>" alt="">
                            <h4><a href="#"><?php echo $row->descricao?></a></h4>
                            <p><?php echo $row->texto?></p>
                        </div>
                   <?php }} ?>
                    <div class="clear"></div>
            </div>
        </div>


        <div class="portfoliO s1" id="work"><!-- start portfoli1 -->
            <div class="wrap">	
		 <h3><?php echo $empresa->galeria_titulo?></h3>
		 <p><?php echo $empresa->galeria_texto?></p>
                <div class="container"> 
	       <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/flexy-menu.js")?>"></script>
                    <div class="h_menu">
                        <ul id="smartfil" class="flexy-menu thick orange">
                            <li class="active"> <span class="filter active" data-filter="app card icon web">VIEW ALL</span></li>
                            <li><span class="filter" data-filter="app  icon">ILLUSTRATION</span></li>
                            <li><span class="filter" data-filter="card ">LOGO</span></li>				            
                            <li><span class="filter" data-filter="icon app card ">THIPOGRAPHY</span></li>
                            <li><span class="filter" data-filter="web card icon">POSTER</span></li>
                        </ul>
                    </div>	
                    
                    <link href="<?php echo base_url("assets/modelo_site/css/header_style1.css")?>" rel="stylesheet" type="text/css" media="all" />
                    <script type="text/javascript">$(document).ready(function(){$(".flexy-menu").flexymenu({speed: 400,type: "horizontal",align: "right"});});</script>

			
                    <ul id="filters" class="clearfix">
                        <li>
                            <a class="filter active" data-filter="app web card" onclick="busca_galeria_categoria('-1')">Ver Todos</a>
                        </li>
                         <?php 
                                if (isset($categorias)){
                                    foreach ($categorias as $row){
                                        
                         ?>
                        <li>
                            <a class="filter" data-filter="<?php echo $row->id?>" onclick="busca_galeria_categoria('<?php echo $row->id?>')"><?php echo $row->descricao?></a>
                        </li>
                       <?php }}?>
                    </ul>
						
                                <div id="portfoliolist">
                                        <div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port.jpg" alt="Image 2" style="top: 0px;"> </a>

                                                </div>
                                        </div>						
                                        <div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port1.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port1.jpg" alt="Image 2" style="top: 0px;"> </a>								
                                                </div>
                                        </div>
                                        <div class="portfolio web mix_all" data-cat="web" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port2.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port2.jpg" alt="Image 2" style="top: 0px;"> </a>							
                                                </div>
                                        </div>
                                        <div class="portfolio card mix_all" data-cat="card" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port3.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port3.jpg" alt="Image 2" style="top: 0px;"> </a>

                                                </div>
                                        </div>
                                        <div class="portfolio web mix_all" data-cat="web" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port4.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port4.jpg" alt="Image 2" style="top: 0px;"> </a>

                                                </div>
                                        </div>
                                        <div class="portfolio app mix_all" data-cat="app" style="display: inline-block; opacity: 1;">
                                                <div class="portfolio-wrapper">
                                                        <a href="images/port5.jpg #small-dialog1" class="flipLightBox popup-with-zoom-anim"><img src="images/port5.jpg" alt="Image 5" style="top: 0px;"> </a>

                                                </div>
                                        </div>

                                </div>				
                                </div>	
                                <div class="img">
                                        <a href="#"><img src="images/zoom.png" alt=""></a>	
                                </div>						
                    </div><!-- end container -->
            </div>


            <div class="experience">
                <div class="wrap"> 
                    <div class="ex-head">
                        <h3><?php echo $empresa->titulo_sobre?></h3>
                        <p><?php echo $empresa->titulo_sobre?></p>
                    </div>
                    <div class="prog">
                        <div class="text">
                            <span><?php echo $empresa->topico1?></span>
                        </div>
                        <div class="text_p">
                            <span><?php echo $empresa->topico1_texto?></span>
                        </div>
                        <div class="clear"></div>
                        <div class="progress-bar-container" data-percent="<?php echo $empresa->topico1_texto?>">
                            <div class="progress-bar" style="width: <?php echo $empresa->topico1_texto?>;">
                            </div>
                        </div>
                    </div>
                    <div class="prog1">
                        <div class="text">
                            <span><?php echo $empresa->topico2?></span>
                        </div>
                        <div class="text_p">
                            <span><?php echo $empresa->topico2_texto?></span>
                        </div>
                        <div class="clear"></div>
                        <div class="progress-bar-container" data-percent="<?php echo $empresa->topico2_texto?>">
                            <div class="progress-bar1" style="width: <?php echo $empresa->topico2_texto?>;">
                            </div>
                        </div>
                    </div>
                    <div class="prog2">
                        <div class="text">
                            <span><?php echo $empresa->topico3?></span>
                        </div>
                        <div class="text_p">
                            <span><?php echo $empresa->topico3_texto?></span>
                        </div>
                        <div class="clear"></div>
                        <div class="progress-bar-container" data-percent="<?php echo $empresa->topico3_texto?>">
                            <div class="progress-bar2" style="width: <?php echo $empresa->topico3_texto?>;">
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
                <!----//End-team-members----> 
			
   		        
   		       <!---- strat-clent---->
            <div class="clients">
                <div class="wrap"> 
                    <div class="client-head">
                        <h3><?php echo $empresa->cliente_titulo?></h3>											
                    </div>
		    <div class="t-clients">
                  	<div class="wrap"> 											 
                            <div class="nbs-flexisel-container">
                                <div class="nbs-flexisel-inner">
                                    <ul class="flexiselDemo3 nbs-flexisel-ul" style="left: -253.6px; display: block;">					    					    					       
                                        <?php if (isset($clientes)) {                        
                                            foreach ($clientes as $row) { ?>
                                        
                                            <li href="<?php echo base_url($row->link)?>" target="_blank" class="nbs-flexisel-item" style="width: 253.6px;"><img src="<?php echo base_url($row->banner)?>"></li>
                                        <?php }} ?>
                                    </ul>
                                    <div class="nbs-flexisel-nav-left" style="top: 21.5px;"></div>
                                    <div class="nbs-flexisel-nav-right" style="top: 21.5px;"></div>
                                        
                                </div>
                            </div> 
                        <div class="clear"> </div>      
                                          <!---strat-image-cursuals---->
                            <script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery.flexisel.js")?>"></script>
                            <!---End-image-cursuals---->
                            <script type="text/javascript">
                                var path = "<?php echo base_url()?>";
                                $(window).load(function() {
                                    $(".flexiselDemo3").flexisel({
                                        visibleItems: 5,
                                        animationSpeed: 1000,
                                        autoPlay: true,
                                        autoPlaySpeed: 3000,            
                                        pauseOnHover: true,
                                        enableResponsiveBreakpoints: true,
                                        responsiveBreakpoints: { 
                                            portrait: { 
                                                changePoint:480,
                                                visibleItems: 1
                                            }, 
                                            landscape: { 
                                                changePoint:640,
                                                visibleItems: 2
                                            },
                                            tablet: { 
                                                changePoint:768,
                                                visibleItems: 3
                                            }
                                        }
                                    });
                                });
                                
        
        function busca_galeria_categoria(categoria_id){
            $('#sec_galeria').empty();
            $('#sec_galeria').load(path + "site/busca_galeria_categoria/" + categoria_id);
            
        }
        
                            </script>
            <!---->
            <!---->
                    </div>				
           </div>
   </div>					
				</div>
				<!----//End-clients----> 
				  
				
		   		 <!-----cntact---->	
   		 		<div class="contact s6" id="contact">
                                    <div class="wrap">
                                        <h3><?php echo $empresa->contato_titulo?></h3>
                                        <p><?php echo $empresa->contato_texto?></p>	
				        <div class="contact_form">
                                            <form method="post" action="contact-post.html">
                                                <span><input type="text" value="nome" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nome';}"><label><img src="<?php echo base_url("assets/modelo_site/images/con1.png")?>" alt="" /></label></span>									    
                                                <span class="left"><input type="text" value="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-mail';}"><label><img src="<?php echo base_url("assets/modelo_site/images/con2.png")?>" alt="" /></label></span>
                                                <div class="clear"></div>
                                                <span><textarea placeholder="MESSAGE"></textarea><label1><img src="<?php echo base_url("assets/modelo_site/images/con3.png")?>" alt="" /></label1></span>
                                                <input type="submit" class="" value="Enviar">
                                            </form>
                                        </div>
                                    </div>
				</div>
				
				<!-----logos---->	
		   		 <div class="logos"> 
                                    <div class="wrap"> 
                                        <ul>
                                            <li><img src="<?php echo base_url("assets/modelo_site/images/client6.png")?>" alt="" /></li>
                                            <li><img src="<?php echo base_url("assets/modelo_site/images/client7.png")?>" alt="" /></li>
                                            <li><img src="<?php echo base_url("assets/modelo_site/images/client8.png")?>" alt="" /></li>
                                        </ul>
                                    </div>
		   		 </div>
				
				<!-----GOOGLE MAP------->
				
                            <div class="map">
                                <?php echo $empresa->localizacao?>
                            </div>
				 
				 
		   		 
   		        <!----start-bottom-footer---->
				<div class="bottom-footer">
					<div class="wrap"> 
						<div class="bottom-footer-center">
							<ul class="bottom-social-icons">
                                                            <li><a class="bottom-twitter" href="#"> </a></li>
                                                            <li><a class="bottom-facebook" href="#"> </a></li>
                                                            <li><a class="bottom-pin" href="#"> </a></li>
                                                            <li><a class="bottom-google" href="#"> </a></li>
                                                            <div class="clear"> </div>
							</ul>
						</div>	
						<div class="bottom-footer-left">
							<p><span>Desenvolvido por</span> <a target="_blank" href="http://www.coopertecsolucoes.com.br/">Coopertec Soluções</a></p>
						</div>		  
						<div class="clear"> </div>
					</div>
				</div>
				<!----//End-bottom-footer---->
 <!-- scroll_top_btn -->
		<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/move-top.js")?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/easing.js")?>"></script>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>

        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
</body>
</html>