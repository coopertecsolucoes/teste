<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Painel Site <small>Nova Mensagem</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('painel_site/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong>Nova Mensagem</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('painel_site/create') ?>" ><i class="fa fa-plus-circle"></i> Nova Mensagem</a></li>
                        <br />
                        <form id="formulario" method="POST" enctype="multipart/form-data" action="<?php echo base_url("painel_site/create_painel_site"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                               
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição Interna 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao"  class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="titulo">Título 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="titulo" name="titulo"  class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="texto">Texto 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="420" id="texto" name="texto" rows="10" class="form-control col-md-7 col-xs-12"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ordem">Ordem <span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="ordem" name="ordem" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>    
<!--                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="sombra">Sombra na Imagem</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="sombra" id="sombra">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>                                        
                                    </select>
                                </div>
                            </div>                      -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ativo">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>                                    
                                    <a href="<?php echo site_url('painel_site/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>
</div>

</div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
        $('#capacidade_kw').mask("###0,00", {reverse: true});
        $('#capacidade_kcalh').mask("###0,00", {reverse: true});
        $('#carga_refrigerante').mask("###0,00", {reverse: true});
        $('#max_vazao_total_ar').mask("###0,00", {reverse: true});
        $('#vazao_total_recirc_agua').mask("###0,00", {reverse: true});
        $('#volume_base').mask("###0,00", {reverse: true});

        $('#vent_numero').mask("######", {reverse: true});
        $('#vent_potencia_nominal').mask("###0,00", {reverse: true});
        $('#vent_potencia_nominal_total').mask("###0,00", {reverse: true});
        $('#vent_velocidade_nominal').mask("#######0", {reverse: true});
        $('#vent_nivel_ruido_3').mask("###0,00", {reverse: true});
        $('#vent_nivel_ruido_50').mask("###0,00", {reverse: true});

        $('#bomba60_quantidade').mask("######", {reverse: true});
        $('#bomba60_frequencia').mask("######", {reverse: true});
        $('#bomba60_potenc_nomin_vent').mask("###0,00", {reverse: true});
        $('#bomba60_potenc_nomin_total').mask("###0,00", {reverse: true});
        $('#bomba60_velocidade_nominal').mask("######", {reverse: true});

        $('#bomba50_quantidade').mask("######", {reverse: true});
        $('#bomba50_frequencia').mask("######", {reverse: true});
        $('#bomba50_potenc_nomin_vent').mask("###0,00", {reverse: true});
        $('#bomba50_potenc_nomin_total').mask("###0,00", {reverse: true});
        $('#bomba50_velocidade_nominal').mask("######", {reverse: true});

        $('#pressao_teste').mask("###0,00", {reverse: true});
        $('#max_pressao_trabalho').mask("###0,00", {reverse: true});

        $('#peso_transporte_superior').mask("######", {reverse: true});
        $('#peso_transporte_inferior').mask("######", {reverse: true});
        $('#peso_total_transporte').mask("######", {reverse: true});
        $('#peso_total_operacao').mask("######", {reverse: true});

        $('#comprimento_transp_superior').mask("######", {reverse: true});
        $('#comprimento_transp_inferior').mask("######", {reverse: true});
        $('#largura_transp_superior').mask("######", {reverse: true});
        $('#largura_transp_inferior').mask("######", {reverse: true});
        $('#altura_transp_superior').mask("######", {reverse: true});
        $('#altura_transp_inferior').mask("######", {reverse: true});
        $('#comprimento_base').mask("######", {reverse: true});
        $('#largura_base').mask("######", {reverse: true});
        $('#altura_instalado').mask("######", {reverse: true});

    });

</script>
<!-- /page content -->
