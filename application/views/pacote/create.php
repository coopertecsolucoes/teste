<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Pacotes de Viagens </h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('pacote/index') ?>">Listagem</a>
                            </li>
                            <li class="Ativo">
                                <strong>Novo Pacote</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('pacote/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Pacote</a></li>
                        <br />
                        <form id="formulario" method="POST" action="<?php echo base_url("pacote/create_pacote"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" >Serviço <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="servico_id" name="servico_id" class="form-control" required>
                                        <option value="">Selecione...</option>
                                        <?php foreach ($servicos as $cat) { ?>
                                            <option value="<?php echo $cat->id ?>"><?php echo $cat->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="detalhe">Detalhes </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="5000" id="detalhe" name="detalhe"  rows="3" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="detalhe">Itens Inclusos </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="10000" id="item_incluso" name="item_incluso"  rows="6" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacao">Observações </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="5000" id="observacao" name="observacao"  rows="3" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valor">Valor Pacote <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="valor" name="valor" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valor_entrada">Valor Entrada <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="valor_entrada" name="valor_entrada" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="parcelamento">Parcelamento <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="parcelamento" name="parcelamento" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacao">Observação do Valor </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <textarea maxlength="10000" id="obs_valor" name="obs_valor"  rows="3" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                </div> 
                        
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="destaque">Pacote Destaque</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <select class="form-control" name="destaque" id="destaque">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ativo">Ativo</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>-->

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                                    <a href="<?php echo site_url('pacote/index') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
