<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Pacote <small>Fotos</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('pacote/index') ?>">Listagem</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('pacote/edit/' . $result->id) ?>"><?php echo $result->descricao ?></a>
                            </li>
                            <li class="active">
                                <strong>Fotos</strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <form id="formulario" method="POST" enctype="multipart/form-data" action="<?php echo base_url("pacote/update_foto"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="userfile">Imagem</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                                 <p>Tamanho mínimo da imagem: 200px X 130px </p>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('pacote/edit/' . $result->id) ?>" class="btn btn-primary">Voltar</a>                                    
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                        </form>
                       
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
            <!--Inicio Galeria de Imagens-->
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Galeria de Fotos </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>  
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                      <div class="x_content">
                            <div class="row">
                                
                                <?php
                                // FOTOS
                                foreach ($fotos as $fot) {
                                    ?>   
                                    <div class="col-md-55">
                                        <div class="thumbnail">
               
                                            <div class="image view view-first">
                                                <img style="width: 100%; display: block;" src="<?php echo base_url('uploads/pacote/' . $result->id) . '/' . $fot->nome_arquivo . $fot->extensao ?>" alt="<?php echo $fot->nome_arquivo ?>" />
                                                <div class="mask">
                                                    <p>Excluir imagem</p>
                                                    <div class="tools tools-bottom">
                                                         <?php 
                                                            if ($fot->principal==0){
                                                            ?>
                                                                <a onclick="deleteConfirm('<?php echo site_url('pacote/delete_imagem/' . $fot->id) ?>')" href="#"><i class="fa fa-times"></i></a>
                                                            <?php
                                                                }
                                                            ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption" style="text-align: center;">
                                                <?php 
                                                    if ($fot->principal==1){
                                                ?>
                                                    <input type="checkbox" class="flat" checked="checked">
                                                    <label>Imagem Principal</label>
                                                <?php
                                                    } else {
                                                        ?>
                                                    <input type="checkbox" class="flat" disabled="true">
                                                    
                                                    <button id="btn_delete" onclick="principalConfirm('<?php echo site_url('pacote/ativa_principal/' . $fot->id) ?>')"  class="btn btn-primary btn-xs"><i class="fa fa-camera"></i> Ativar Principal </button>
                                                <?php
                                                    }
                                                ?>
                                            </div>                                        
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fim Galeria de Imagens-->
        </div>
    </div>

</div>
<!-- /page content -->
<script type="text/javascript">

    function deleteConfirm(url) {
        bootbox.confirm("Tem certeza que deseja excluir?", function (res) {
            if (res === true) {
                window.location = url;
            } else {
                return res;
            }
        });
    }
    function principalConfirm(url) {
        bootbox.confirm("Tem certeza que deseja colocar essa imagem como principal?", function (res) {
            if (res === true) {
                window.location = url;
            } else {
                return res;
            }
        });
    }
    
</script>
