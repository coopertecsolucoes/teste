<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Pacotes <small>Fotos</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('linha/index') ?>">Listagem</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('linha/edit/' . $result->id) ?>"><?php echo $result->nome ?></a>
                            </li>
                            <li class="active">
                                <strong>Fotos</strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-primary" href="<?php echo site_url('linha/acabamentos/' . $result->id) ?>" ><i class="fa fa-plus-circle"></i> Acabamentos</a></li>
                        <a class="btn btn-primary" href="<?php echo site_url('linha/medidas/' . $result->id) ?>" ><i class="fa fa-plus-circle"></i> Medidas</a></li>
                        <br />
                        <h2><?php echo $result->nome; ?></h2>
                        <form id="formulario" method="POST" enctype="multipart/form-data" action="<?php echo base_url("produtos/update_foto_principal"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <input type="hidden" name="principal" id="principal" value="1">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="userfile">Foto Principal</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url('produtos/edit/' . $result->id) ?>" class="btn btn-primary">Voltar</a>
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                        </form>
                        <form id="formulario2" method="POST" enctype="multipart/form-data"  action="<?php echo base_url("produtos/update_fotos"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <input type="hidden" name="principal" id="principal" value="0">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="userfile">Fotos</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="userfile" name="userfile[]" multiple class="col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url('produtos/edit/' . $result->id) ?>" class="btn btn-primary">Voltar</a>
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                        </form>
                        <?php
                        // FOTO PRINCIPAL
                        if ($foto_principal[0]->nome_arquivo) {
                            ?>  
                            <div class="col-md-55">
                                <div class="thumbnail" style="border: 2px #337ab7 solid">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="<?php echo base_url('uploads/produtos/' . $result->id) . '/' . $foto_principal[0]->nome_arquivo . '_thumb' . $foto_principal[0]->extensao ?>" alt="<?php echo $foto_principal[0]->nome_arquivo ?>" />
                                    </div>
                                    <div class="caption">
                                        <p><strong> - PRINCIPAL - </strong>
                                            <button id="btn_delete" onclick="deleteConfirm('<?php echo site_url('produtos/delete_foto/' . $foto_principal[0]->foto_id) ?>')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Apagar </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>   
                        <?php
                        // FOTOS
                        foreach ($fotos as $fot) {
                            ?>   
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="<?php echo base_url('uploads/produtos/' . $result->id) . '/' . $fot->nome_arquivo . '_thumb' . $fot->extensao ?>" alt="<?php echo $fot->nome_arquivo ?>" />
                                    </div>
                                    <div class="caption">
                                        <button id="btn_delete" onclick="deleteConfirm('<?php echo site_url('produtos/delete_foto/' . $fot->foto_id) ?>')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Apagar </button>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>   
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<script type="text/javascript">

    function deleteConfirm(url) {
        bootbox.confirm("Tem certeza que deseja excluir?", function (res) {
            if (res === true) {
                window.location = url;
            } else {
                return res;
            }
        });
    }
</script>
