<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Pacotes de Viagens</h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('pacote/index') ?>">Listagem</a>
                            </li>
                            <li class="Ativo">
                                <strong><?php echo $result->descricao ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('pacote/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Pacote</a>
                        <a class="btn btn-primary" href="<?php echo site_url('pacote/fotos/' . $result->id) ?>" ><i class="fa fa-picture-o"></i> Fotos</a>                        <br />

                        <form id="formulario" method="POST" enctype="multipart/form-data" action="<?php echo base_url("pacote/update_pacote"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <span class="red"><?php echo validation_errors(); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <input type="text" id="descricao" name="descricao" required="required" value="<?php echo $result->descricao; ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>                      
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_id">Serviço <span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <select name="servico_id" class="form-control">
                                            <?php
                                            foreach ($servicos as $cat) {
                                                $selected = '';
                                                if ($result->servico_id == $cat->id) {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $cat->id ?>"><?php echo $cat->descricao ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">** Dica
                                    </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <input type="text" readonly="true"  value="Para deixar o texto em negrito coloque o texto ente <b> </b>" class="form-control col-md-7 col-xs-12" >
                                    </div>
                                </div>    
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="detalhe">Detalhes </label>
                                    <div class="col-md-10 col-sm-6 col-xs-12">
                                        <textarea maxlength="5000" id="detalhe" name="detalhe"  rows="6" class="form-control col-md-7 col-xs-12"><?php echo $result->detalhe ?></textarea>
                                    </div>
                                </div>  
                                 <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacao">Itens Inclusos </label>
                                    <div class="col-md-10 col-sm-6 col-xs-12">
                                        <textarea maxlength="10000" id="item_incluso" name="item_incluso"  rows="6" class="form-control col-md-7 col-xs-12"><?php echo $result->item_incluso ?></textarea>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacao">Observações </label>
                                    <div class="col-md-10 col-sm-6 col-xs-12">
                                        <textarea maxlength="5000" id="observacao" name="observacao"  rows="6" class="form-control col-md-7 col-xs-12"><?php echo $result->observacao ?></textarea>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valor">Valor Pacote
                                    </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <input type="text" id="valor" name="valor"  value="<?php echo $result->valor; ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valor_entrada">Valor Entrada 
                                    </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <input type="text" id="valor_entrada" name="valor_entrada"  value="<?php echo $result->valor_entrada; ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="parcelamento">Parcelamento 
                                    </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <input type="text" id="parcelamento" name="parcelamento"  value="<?php echo $result->parcelamento; ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacao">Observação do Valor </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <textarea maxlength="10000" id="obs_valor" name="obs_valor"  rows="3" class="form-control col-md-7 col-xs-12"><?php echo $result->obs_valor ?></textarea>
                                    </div>
                                </div> 
                               
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Pacote Detaque</label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <select class="form-control" name="destaque" id="destaque">                                            
                                            <option value="0" <?php echo ($result->destaque == 0) ? 'selected' : null ?>>Não</option>
                                            <option value="1" <?php echo ($result->destaque == 1) ? 'selected' : null ?>>Sim</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="banner">Foto Destaque</label>
                                    <div class="col-md-55">
                                        <div class="thumbnail">
                                            <img style="width: 100%; display: block;" src="<?php echo base_url($result->foto_destaque) ?>" alt="Banner" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p style="margin-left: 10px; margin-top: 10px;">Sugestão de tamanho: 300px X 364px</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <select class="form-control" name="ativo" id="ativo">
                                            <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                            <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                     <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Categorias do Pacote</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                   <div class="col-md-12 col-sm-12 col-xs-12" id="tipo">
                                                       <ul class="to_do">
                                                        <?php
                                                        foreach ($categorias as $cat) {
                                                            $checked = '';
                                                            foreach ($categorias_salvas as $ok) {
                                                                if ($ok->categoria_id == $cat->id) {
                                                                    $checked = 'checked';
                                                                }
                                                            }
                                                            ?>
                                                            <li>
                                                                <p><input type="checkbox" name="categorias[]" value="<?php echo $cat->id ?>" <?php echo $checked ?> class="flat"> <?php echo $cat->descricao ?></p>
                                                            </li>  
                                                        <?php } ?>
                                                            </ul>
                                                        <div class="help-block with-errors"></div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                    <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Informações do Painel</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-6 col-sm-6 col-xs-12">Mostra Painel</label>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <select class="form-control" name="mostra_painel" id="mostra_painel">                                            
                                                        <option value="0" <?php echo ($result->mostra_painel == 0) ? 'selected' : null ?>>Não</option>
                                                        <option value="1" <?php echo ($result->mostra_painel == 1) ? 'selected' : null ?>>Sim</option>                                            
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6 col-sm-6 col-xs-12" for="painel_titulo">Título
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="painel_titulo" name="painel_titulo" value="<?php echo $result->painel_titulo; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <label class="col-md-6 col-sm-6 col-xs-12" for="painel_preco">Preço
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="painel_preco" name="painel_preco" value="<?php echo $result->painel_preco; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-6 col-sm-6 col-xs-12" for="painel_parcelamento">Parcelamento
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="painel_parcelamento" name="painel_parcelamento" value="<?php echo $result->painel_parcelamento; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-6 col-sm-6 col-xs-12" for="painel_entrada">Entrada
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="painel_entrada" name="painel_entrada" value="<?php echo $result->painel_entrada; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-12 col-sm-12 col-xs-12" for="painel_observacao">Observação Preço
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="painel_obs" name="painel_obs" value="<?php echo $result->painel_obs; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-12 col-sm-12 col-xs-12" for="ordem">Ordem de Apresentação
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input type="text" id="ordem" name="ordem" value="<?php echo $result->ordem; ?>" class="form-control col-md-7 col-xs-12" >                                                       
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
<!--                                <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Itens Inclusos</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                   <div class="col-md-12 col-sm-12 col-xs-12" id="tipo">
                                                        <ul class="to_do">
                                                             <?php
                                                             foreach ($itens as $ite) {
                                                                 $checked = '';
                                                                 foreach ($itens_salvos as $ok) {
                                                                     if ($ok->item_id == $ite->id) {
                                                                         $checked = 'checked';
                                                                     }
                                                                 }
                                                                 ?>                                                         
                                                                <li>
                                                                    <p><input type="checkbox" name="itens[]" value="<?php echo $ite->id ?>" <?php echo $checked ?> class="flat"> <?php echo $ite->descricao ?></p>
                                                                </li>                                                       
                                                             <?php } ?>

                                                        </ul>
                                                        <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>                                -->
                            </div> 
                                
                         
                            <div class="col-md-12 form-group">
                                <div class="ln_solid"></div>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                                    <a href="<?php echo site_url('pacote/index') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario .btn').on('click', function () {
            $('#formulario').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#formulario').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });

    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
<!-- /form validation -->
