<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Líder de Si</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/modelo_site_2/images/favicon.ico")?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url("assets/modelo_site_2/images/apple-touch-icon.png")?>">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet"> 
    
    <!-- Custom & Default Styles -->
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/bootstrap.min.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/font-awesome.min.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/carousel.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/animate.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/blog3.css")?>">

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

</head>
<body>  

    <!-- LOADER -->
    <div id="preloader">
        <img class="preloader" src="images/loader.gif" alt="">
    </div><!-- end loader -->
    <!-- END LOADER -->

    <div id="wrapper">
        

        <section class="section db p120">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tagline-message page-title text-center">
                            <h3>Blog Líder de Si</h3>
                            <ul class="breadcrumb">
                                
                                <li><a href="<?php echo base_url("site/index")?>" class="page-scroll">Home</a></li>
                                <!--<li class="active">Blog</li>-->
                            </ul>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section gb nopadtop">
            <div class="container">
                <div class="boxed">
                    <div class="row">
                        <div class="col-md-8">
                           <?php
                            if (isset($posts)){
                                foreach($posts as $row){
                           ?>
                                <div class="content blog-list">
                                    <div class="blog-wrapper clearfix">
                                        <div class="blog-meta">
                                            <small><a href="#"><?php echo $row->desc_categoria;?></a></small>
                                            <h3><a href="<?php echo base_url("site/post/" . $row->id)?>" title=""><?php echo $row->descricao;?></a></h3>
                                            <ul class="list-inline">
                                                <li><i class="fa fa-calendar"></i> <?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?></li>
                                                <li><i class="fa fa-user"></i> <span>Publicado por</span><?php echo $row->nome_usuario;?></li>
                                            </ul>
                                        </div><!-- end blog-meta -->

                                        <div class="blog-media">
                                            <a href="<?php echo base_url("site/post/" . $row->id)?>" title=""><img src="<?php echo base_url("uploads/blog/" . $row->id . "/". $row->nome_arquivo . $row->extensao)?>" alt="" class="img-responsive img-rounded"></a>
                                        </div><!-- end media -->

                                        <div class="blog-desc-big">
                                            <p><?php echo $row->subtitulo;?></p>
                                         
                                            <a href="<?php echo base_url("site/post/" . $row->id)?>" class="btn btn-primary">Ler Mais</a>
                                        </div><!-- end desc -->
                                    </div><!-- end blog -->
                                </div><!-- end content -->
                            <?php
                            }}
                           ?>
                                
                            <div class="row">
                                <div class="col-md-12">
                                   <?php echo $pagination;?>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end col -->

                        <div class="sidebar col-md-4">
                            <div class="widget clearfix">
                                <h3 class="widget-title">Mais Populares</h3>
                                <div class="post-widget">
                                    <?php
                                    if (isset($populares)){
                                        foreach($populares as $row){
                                   ?>
                                    <div class="media">
                                        <img src="<?php echo base_url("uploads/blog/" . $row->id . "/". $row->nome_arquivo ."_thumb" . $row->extensao)?>" alt="" class="img-responsive alignleft img-rounded">
                                        <div class="media-body">
                                            <h5 class="mt-0"><a href="<?php echo base_url("site/post/" . $row->id)?>"><?php echo substr($row->subtitulo,0,34). '...';?></a></h5>
                                            <div class="blog-meta">
                                                <ul class="list-inline">
                                                   <li><i class="fa fa-calendar"></i> <?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?></li>
                                                    <li><i class="fa fa-user"></i> <span>Por</span><?php echo $row->nome_usuario;?></li>
                                                </ul>
                                            </div><!-- end blog-meta -->
                                        </div>
                                    </div>
                                    <?php
                                    }}
                                   ?>
                                </div><!-- end post-widget -->
                            </div><!-- end widget -->

<!--                            <div class="widget clearfix">
                                <h3 class="widget-title">Subscribe Our Newsletter</h3>
                                <div class="newsletter-widget">
                                    <p>You can opt out of our newsletters at any time. See our <a href="#">privacy policy</a>.</p>
                                    <form class="form-inline" role="search">
                                        <div class="form-1">
                                            <input type="text" class="form-control" placeholder="Enter email here..">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i></button>
                                        </div>
                                    </form>
                                </div> end newsletter 
                            </div> end widget 
                            -->
                            <div class="widget clearfix">
                                <h3 class="widget-title">Categorias</h3>
                                <div class="tags-widget">                                       
                                    <ul class="list-inline">
                                        <li><a href="<?php echo base_url("site/blog/all")?>">Todas Categorias</a></li>
                                        <?php
                                        if (isset($categorias)){
                                            foreach($categorias as $row){
                                       ?>
                                            <li><a href="<?php echo base_url("site/blog/" . $row->id)?>"><?php echo $row->descricao;?></a></li>
                                       <?php
                                        }}
                                       ?>
                                    </ul>
                                </div><!-- end list-widget -->
                            </div><!-- end widget -->
                            
<!--                            <div class="widget clearfix">
                                <h3 class="widget-title">Popular Tags</h3>
                                <div class="tags-widget">   
                                    <ul class="list-inline">
                                        <li><a href="#">course</a></li>
                                        <li><a href="#">web design</a></li>
                                        <li><a href="#">development</a></li>
                                        <li><a href="#">language</a></li>
                                        <li><a href="#">teacher</a></li>
                                        <li><a href="#">speaking</a></li>
                                        <li><a href="#">material</a></li>
                                        <li><a href="#">css3</a></li>
                                        <li><a href="#">html</a></li>
                                        <li><a href="#">learning</a></li>
                                    </ul>
                                </div> end list-widget 
                            </div> end widget -->
                        </div><!-- end sidebar -->
                    </div><!-- end row -->
                </div><!-- end boxed -->
            </div><!-- end container -->
        </section>

        <div class="copyrights">
            <div class="container">
                <div class="clearfix">
                    <div class="pull-right">
                        <div class="footer-links">
                            <ul class="list-inline">
                                <li>Desenvolvido por: <a href="https://www.coopertecsolucoes.com.br/" target="_blank">Coopertec Soluções</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copy -->
    </div><!-- end wrapper -->

    <!-- jQuery Files -->
    <script src="<?php echo base_url("assets/modelo_site_2/js/jquery.min.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/bootstrap.min.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/animate.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/custom.js")?>"></script>

</body>
</html>