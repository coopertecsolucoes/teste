
      <div class="divisao" > </div>
        <div class="slider_wrapper">
            <div id="camera_wrap" class="">
                <?php
                if (isset($pacote_fotos)) {                        
                    foreach ($pacote_fotos as $row) { ?> 
                <h1><?php echo $row->nome_arquivo?></h1>
                    <div data-src="<?php echo base_url("uploads/pacote/" . $row->pacote_id . "/". $row->nome_arquivo . $row->extensao)?>">
                        <?php
                        if ($pacote->mostra_painel==='1') {     ?>
                            <div class="camera_tarja">
<!--                                <h2>África do Sul</h2> 
                                <h3>Deixe-se levar por esse lugar encantador</h3> -->
                                <div class="tarja">                                    
                                    <div class="cabecalho">
                                        <span id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"><i class="far fa-times-circle"></i></span>
                                        <h2><?php echo nl2br($row->descricao); ?></h2>                                         
                                    </div>
                                    <div class="texto">
                                        <h2 class="titulo"><?php echo nl2br($row->painel_titulo); ?></h2> 
                                        <h2 class="preco"><?php echo nl2br($row->painel_preco); ?></h2> 
                                        <h2 class="entrada"><?php echo nl2br($row->painel_entrada); ?></h2> 
                                        <h2 class="parcelamento"><?php echo nl2br($row->painel_parcelamento); ?></h2> 
                                        <h2 class="obs"><?php echo nl2br($row->painel_obs); ?></h2>                                         
                                    </div>
                                </div>
                                
                            </div>
                        <?php }   ?>
                    </div>
                <?php } }?>
            </div>
        </div>
        <div class="divisao" > </div>

        <div class="content">
            <div class="container_12 corpo_detalhe">
                <div class="detalhe" >
                    <div class="cabecalho">
                        <h2><?php echo $pacote->descricao;?></h2>
                    </div>
                    <div class="detalhamento">
                        <h2><?php echo nl2br($pacote->detalhe);?></h2>
                        <br/>
                        <h2><?php echo nl2br($pacote->item_incluso);?></h2>
                    </div>
                    
                    
                    <div class="detalhamento">                        
                        <h3><?php echo nl2br($pacote->observacao);?></h3>
                    </div>
                    
                    
                </div>
                <div class="destaque" >
                    <div class="titulo">
                        <h2><?php echo $pacote->descricao;?> </h2>
                        
                    </div>
                    <div class="valor">
                        <h2 class="entrada"><?php echo $pacote->valor_entrada;?> </h2>                    
                        <h2 class="parcelamento"><?php echo $pacote->parcelamento;?> </h2>
                        <h2 class="total"><?php echo $pacote->valor;?> </h2>
                        <h2 class="obs">*<?php echo $pacote->obs_valor;?> </h2>
                    </div>
                    <div class="formulario">
                        <h2>Entre em contato</h2>
                        <form id="formulario" enctype="multipart/form-data" method="POST" action="<?php echo base_url("site/contato_pacote"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" id="pacote_id" name="pacote_id" value="<?php echo $pacote->id?>">
                            <div class="col-md-12">
                                <div class="col-esquerda">
                                    <label >Nome </label>
                                </div>
                                <div class="col-direita">
                                    <input type="text" id="nome" name="nome" > 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-esquerda">
                                    <label >Celular </label>
                                </div>
                                <div class="col-direita">
                                    <input type="text" id="telefone" name="telefone" > 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-esquerda">
                                    <label >Email </label>
                                </div>
                                <div class="col-direita">
                                    <input type="text" id="email" name="email" > 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-esquerda">
                                    <label >Mensagem </label>
                                </div>
                                <div class="col-direita">
                                    <textarea type="text" id="mensagem" name="mensagem" > </textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="botao">
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>

        
        

   