<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Líder de Si</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/modelo_site_2/images/favicon.ico")?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url("assets/modelo_site_2/images/apple-touch-icon.png")?>">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet"> 
    
    <!-- Custom & Default Styles -->
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/bootstrap.min.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/font-awesome.min.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/carousel.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/animate.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/modelo_site_2/css/blog3.css")?>">

</head>
<body>  

    <!-- LOADER -->
    <div id="preloader">
        <img class="preloader" src="images/loader.gif" alt="">
    </div><!-- end loader -->
    <!-- END LOADER -->

    <div id="wrapper">
       
        <header class="header header-normal">
       

        </header>

        <section class="section db p120">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tagline-message page-title text-center">
                            <h3>Blog Líder de Si</h3>
                            <ul class="breadcrumb">
                                
                                <li><a href="<?php echo base_url("site/index")?>" class="page-scroll">Home</a></li>
                                <!--<li class="active">Blog</li>-->
                            </ul>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section gb nopadtop">
            <div class="container">
                <div class="boxed">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="content blog-list">
                                <div class="blog-wrapper clearfix">
                                    <div class="blog-meta">
                                        <small><a href="#"><?php echo $result->desc_categoria;?></a></small>
                                        <h3><?php echo $result->descricao;?></h3>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-calendar"></i> <?php echo  str_replace('.', ',', isset($result->data) ? date("d/m/Y", strtotime($result->data)) : null); ?></li>
                                            <li><i class="fa fa-user"></i> <span>Publicado por</span><?php echo $result->nome_usuario;?></li>
                                        </ul>
                                    </div><!-- end blog-meta -->

                                    <div class="blog-media">
                                        <img src="<?php echo base_url("uploads/blog/" . $result->id . "/". $result->nome_arquivo . $result->extensao)?>" alt="" class="img-responsive img-rounded">
                                    </div><!-- end media -->

                                    <div class="blog-desc-big">
                                        <?php echo $result->texto;?>
                                        <hr class="invis">

                                       

                                    </div><!-- end desc -->
                                </div><!-- end blog -->
                            </div><!-- end content -->

                            <div class="authorbox">
                                <div class="site-publisher clearfix">
                                    <img src="<?php echo base_url($result->foto)?>" alt="" class="img-responsive img-circle">
                                    <h4><span><?php echo $result->nome_usuario?></span></h4>
                                    <p><?php echo $result->sobre?></p>

                                    <div class="authorbox-social">
                                        <ul class="list-inline">
                                            <li><a target="_blank" href="<?php echo $result->facebook?>"><i class="fa fa-facebook"></i></a></li>
                                            <li><a target="_blank" href="#<?php echo $result->instagram?>"><i class="fa fa-instagram"></i></a></li>
                      
                                        </ul><!-- end list -->
                                    </div><!-- end share -->
                                </div><!-- end publisher -->
                            </div><!-- end details -->

                             <div class="content boxed-comment clearfix">
                                <h3 class="small-title">Deixe seu comentário</h3>
                                <form class="big-contact-form" method="POST" action="<?php echo base_url("blog/create_comentario/" . $result->id); ?>" data-parsley-validate>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <input type="text" class="form-control" name="nome" placeholder="Nome Completo">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea placeholder="Sua Mensagem" id="mensagem" name="mensagem" class="form-control"></textarea>
                                        <button class="btn btn-primary" type="submit">Enviar</button>
                                    </div>
                                </form>
                            </div><!-- end content -->

                            <div class="content boxed-comment clearfix">
                                <h3 class="small-title"><?php echo $result->comentarios?> Comentários</h3>
                                <div class="comments-list">
                                    <?php if (isset($comentarios)){
                                         foreach($comentarios as $row){
                                    ?>
                                        <div class="media">
                                            <p class="pull-right"><small><?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?></small></p>
                                            <div class="media-body">
                                                <h4 class="media-heading user_name"><?php echo $row->nome?></h4>
                                                <p><?php echo $row->mensagem?></p>
                                                <!--<a href="#" class="btn btn-primary btn-sm">Reply</a>-->
                                            </div>
                                        </div>                        
                                    <?php }}
                                    ?>
                                </div>
                            </div><!-- end content -->

                           
                        </div><!-- end col -->

                        <div class="sidebar col-md-4">
                            <div class="widget clearfix">
                                <h3 class="widget-title">Mais Populares</h3>
                                <div class="post-widget">
                                    <?php
                                    if (isset($populares)){
                                        foreach($populares as $row){
                                   ?>
                                    <div class="media">
                                        <img src="<?php echo base_url("uploads/blog/" . $row->id . "/". $row->nome_arquivo ."_thumb" . $row->extensao)?>" alt="" class="img-responsive alignleft img-rounded">
                                        <div class="media-body">
                                            <h5 class="mt-0"><a href="<?php echo base_url("site/post/" . $row->id)?>"><?php echo substr($row->subtitulo,0,34). '...';?></a></h5>
                                            <div class="blog-meta">
                                                <ul class="list-inline">
                                                   <li><i class="fa fa-calendar"></i> <?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?></li>
                                                    <li><i class="fa fa-user"></i> <span>Por </span><?php echo $row->nome_usuario;?></li>
                                                </ul>
                                            </div><!-- end blog-meta -->
                                        </div>
                                    </div>
                                    <?php
                                    }}
                                   ?>
                                </div><!-- end post-widget -->
                            </div><!-- end widget -->

<!--                            <div class="widget clearfix">
                                <h3 class="widget-title">Subscribe Our Newsletter</h3>
                                <div class="newsletter-widget">
                                    <p>You can opt out of our newsletters at any time. See our <a href="#">privacy policy</a>.</p>
                                    <form class="form-inline" role="search">
                                        <div class="form-1">
                                            <input type="text" class="form-control" placeholder="Enter email here..">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i></button>
                                        </div>
                                    </form>
                                </div> end newsletter 
                            </div> end widget -->

<!--                            <div class="widget clearfix">
                                <h3 class="widget-title">Popular Tags</h3>
                                <div class="tags-widget">   
                                    <ul class="list-inline">
                                        <li><a href="#">course</a></li>
                                        <li><a href="#">web design</a></li>
                                        <li><a href="#">development</a></li>
                                        <li><a href="#">language</a></li>
                                        <li><a href="#">teacher</a></li>
                                        <li><a href="#">speaking</a></li>
                                        <li><a href="#">material</a></li>
                                        <li><a href="#">css3</a></li>
                                        <li><a href="#">html</a></li>
                                        <li><a href="#">learning</a></li>
                                    </ul>
                                </div> end list-widget -->
                            </div><!-- end widget -->
                        </div><!-- end sidebar -->
                    </div><!-- end row -->
                </div><!-- end boxed -->
            </div><!-- end container -->
        </section>

        <div class="copyrights">
            <div class="container">
                <div class="clearfix">
                    <div class="pull-right">
                        <div class="footer-links">
                            <ul class="list-inline">
                                <li>Desenvolvido por: <a href="https://www.coopertecsolucoes.com.br/" target="_blank">Coopertec Soluções</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copy -->
    </div><!-- end wrapper -->

  
    <!-- jQuery Files -->
    <script src="<?php echo base_url("assets/modelo_site_2/js/jquery.min.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/bootstrap.min.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/animate.js")?>"></script>
    <script src="<?php echo base_url("assets/modelo_site_2/js/custom.js")?>"></script>


</body>
</html>