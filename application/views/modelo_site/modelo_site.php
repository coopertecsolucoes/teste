

        <div class="divisao" > </div>
        
        <div class="slider_wrapper">
            <div id="camera_wrap" class="">
                <?php
                if (isset($painel)) {                        
                    foreach ($painel as $row) { ?>                     
                    <div data-src="<?php echo base_url("uploads/pacote/" . $row->pacote_id . "/". $row->nome_arquivo . $row->extensao)?>">
                        
                        <?php if ($row->sombra==1){ ?>
                            <div class="overlay-div"></div>  
                        <?php } ?>
                        <div class="caption fadeIn">
<!--                            <h2><?php echo nl2br($row->titulo); ?></h2>
                            <div class="texto">
                                <?php echo nl2br($row->texto); ?>                              
                            </div>                        -->
                        </div>
                            <div class="camera_tarja">
<!--                                <h2>África do Sul</h2> 
                                <h3>Deixe-se levar por esse lugar encantador</h3> -->
                                <div class="tarja">                                    
                                    <div class="cabecalho">
                                        <span id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"><i class="far fa-times-circle"></i></span>
                                        <h2><?php echo nl2br($row->descricao); ?></h2>                                         
                                    </div>
                                    <div class="texto">
                                        <h2 class="titulo"><?php echo nl2br($row->painel_titulo); ?></h2> 
                                        <h2 class="preco"><?php echo nl2br($row->painel_preco); ?></h2> 
                                        <h2 class="entrada"><?php echo nl2br($row->painel_entrada); ?></h2> 
                                        <h2 class="parcelamento"><?php echo nl2br($row->painel_parcelamento); ?></h2> 
                                        <h2 class="obs"><?php echo nl2br($row->painel_obs); ?></h2> 
                                        <a href="<?php echo base_url('site/detalhes/'.$row->pacote_id); ?>">Saiba Mais</a> 
                                    </div>
                                </div>
                                
                            </div>
                    </div>
                
                <?php } }?>
            </div>
        </div>
        <div class="divisao" > </div>
        
        
        <div class="content">
            <!--INICIO PACOTES DESTAQUES-->
            <div class="container_12">
                 <?php
                if (isset($pacote_destaque)) {                        
                    foreach ($pacote_destaque as $row) { ?>     

                    <div class="grid_4">
                        <div class="banner">
                            <div class="sombra"></div>
                            <img src="<?php echo base_url($row->foto_destaque)?>" alt="">
                            <div class="label">
                                <div class="title"><?php echo nl2br($row->descricao); ?></div>
                                <!--<div class="price">A partir de <span><?php echo nl2br($row->valor); ?></span></div>-->
                                <a href="<?php echo base_url('site/detalhes/'.$row->pacote_id); ?>">Saiba Mais</a>
                            </div>
                        </div>
                    </div>                
                    
                <?php } }?>
                <div class="clear"></div>
	    </div>
            <!--FINAL PACOTES DESTAQUES-->
            
            
            <!--INICIO NEWSLETTER-->
            <section class="subscribe" id="subscribe">
                <div class="container">
                    <h3 class="heading">Receba nossas novidades</h3>
                    <form action="<?php echo base_url('site/newsletter'); ?>" method="post" class="newsletter">
                        <input class="email" id="email" name="email" type="email" placeholder="Informe seu email" required="">
                        <input type="submit" class="submit" value="Enviar">
                    </form>
                </div>
            </section>
            <!--FINAL NEWSLETTER-->
            
            <!--INICIO PACOTES-->
            <section id="pacotes"  class="travel-box img-bg" style="background-image: url('../assets/modelo_site/images/slide1.jpg')"  >
        	<div class="container">
                     
                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-travel-boxes">
                                <div id="desc-tabs" class="desc-tabs">
                                    <input type="hidden" id="servico_id">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a onclick="pesquisa_pacote('-1','-1');" aria-controls="tours" role="tab" data-toggle="tab">
                                            <i class="fas fa-globe-americas"></i>Todos</a>
                                            
                                        </li>
                                        <?php
                                            if (isset($servicos)) {
                                                $primeiro = (int)0;                                                    
                                                foreach ($servicos as $row) {
                                                    if ($primeiro==0){
                                                        $primeiro=1;                                                            
                                        ?>
                                        <li role="presentation">
                                            <a onclick="pesquisa_pacote('<?php echo $row->id; ?>','-1');" aria-controls="tours" role="tab" data-toggle="tab">
                                            <i class="<?php echo $row->icone; ?>"></i><?php echo $row->descricao; ?></a>
                                        </li>
                                        <?php } else {?>
                                        <li role="presentation">
                                            <a onclick="pesquisa_pacote('<?php echo $row->id; ?>','-1');" aria-controls="tours" role="tab" data-toggle="tab">
                                            <i class="<?php echo $row->icone; ?>"></i><?php echo $row->descricao; ?></a>
                                        </li>
                                        <?php }}}?>
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <input type="hidden" name="servico_id" id="servico_id" value="-1">
                                        <div role="tabpanel" class="tab-pane active fade in" id="tours">
                                            <div class="tab-para">
                                                <div class="row">
                                                    <div class="trip-circle">
                                                        <div class="form-check">
                                                            <label>
                                                                <input id="chk_todas" type="radio" name="radio" checked onclick="pesquisa_pacote_cat('-1');"> <span class="label-text">Todas</span>
                                                            </label>
                                                        </div>
			                                <?php
                                                            if (isset($categorias)) {                                             
                                                                foreach ($categorias as $row) {
                                                                                                          
                                                            ?>
                                                        <div class="form-check">
                                                            <label>
                                                                <input type="radio" name="radio" onclick="pesquisa_pacote_cat('<?php echo $row->id; ?>');"> <span class="label-text"><?php echo $row->descricao; ?></span>
                                                            </label>
                                                        </div>
                                                   
                                                        <?php
                                                            }}                                                                  
                                                        ?>                    
                                                    </div><!--/.trip-circle-->
                                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class="single-tab-select-box">
                                                            <div id="sec_pacotes">

                                                            </div>
                                                        </div><!--/.single-tab-select-box-->
                                                    </div><!--/.col-->
                                                </div>
                                            </div><!--/.tab-para-->
                                        </div><!--/.tabpannel-->
                                        <div class="destination-pagenate">
                                            <div class="wrap">
                                                <ul>
                                                    <li><a id="btn_" class="d-next" onclick="pesquisa_pacote_todos('-1','-1');">Mostrar todos pacotes</a></li>
                                                    <div class="clear"> </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--/.tab content-->
                                        
                                </div><!--/.desc-tabs-->
                            
                            </div><!--/.single-travel-box-->
                        </div><!--/.col-->
                        
													
        	</div><!--/.container-->

        </section><!--/.travel-box-->

            <!--FIM PACOTES-->
            
            <!-- contact -->		
        <section class="contact" id="contact">
            <div class="container">
                <h3 class="heading"><?php echo $empresa->contato_titulo?></h3>
                <div class="contact-agileinfo">
                    <div class="contact-center"> 
                        <form action="<?php echo base_url('site/contato'); ?>" method="post">  
                            <input type="text" name="nome" placeholder="Nome" required="">
                            <input type="email" class="email" name="email" placeholder="Email" required="">
                            <input type="text" name="telefone" placeholder="Celular" required="">
                            <input type="text" name="assunto" placeholder="Assunto" required="">
                            <textarea name="mensagem" placeholder="Mensagem" required=""></textarea>
                            <input type="submit" value="Enviar" > 
                        </form>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <section class="mapa">
                <?php echo $empresa->localizacao?>
        </section>
            
            
    </div>            
 
                
               