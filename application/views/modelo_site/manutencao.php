<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    
        <title>Skala Viagens</title>
  
        <meta name="author" content="DTIC-IFSC">
        <meta name="description" content="Site temporário">
  
	<link rel="shortcut icon" href="img/fav.ico">
	<link rel="icon" href="img/fav.ico">
	<link rel="stylesheet" type="text/css" href=<?php echo base_url("assets/modelo_site/css/bootstrap.css"); ?> />

	<style>
	      body {
		padding-bottom: 40px;
	      }
	      .container-narrow {
		margin: 0 auto;
		max-width: 600px;
		margin-top: 20px;
	      }
	      .container-narrow > hr {
		margin: 30px 0;
	      }
	      .marketing {
		margin: 20px 0;
	      }
	      .marketing p + h3 {
		margin-top: 28px;
		text-align: center;
	      }
	     .justify{
		text-align: center;
	      }
	</style>

  	<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-responsive.min.css">

</head>

<body>


    
    <div class="container-narrow" style="text-align:center;">
      
	                    <img src=<?php echo base_url("assets/modelo_site/images/logo2.png"); ?>  alt="Logo"/>


	<hr>
        <h3 class="marketing"><p>Prezado visitante</p></h3>
	<p class="marketing">Nosso site está sendo reformulado. Em breve teremos novidades. Aguarde....<br>
	
	
        <hr>
	<div class="row-fluid">
            <h4 class="marketing"><p>Acesse nossa página no facebook</p></h4>
                <div class="thumbnail text-center">
                  <a href="https://www.facebook.com/skalaviagenserragaucha/">Facebook</a>
                </div>
         </div>    
      <hr>
  
    </div>
     
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-42336287-1']);
        _gaq.push(['_addDevId', 'i9k95']); // Google Analyticator App ID with Google 
        
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
    </script>
</body>
</html>