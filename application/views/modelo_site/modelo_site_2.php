<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $empresa->nome_fantasia;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Líder de Si Coaching, Roberta Stuani, Coaching, Mudança, Coach, Evolução, Metas, Objetivos, Realização, Sonhos, Busca de Evolução, Crescimento
            Profissional, Business, Life Coaching, Fitness Coaching" />
<!-- css files -->
<link href="<?php echo base_url("assets/modelo_site_2/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/font-awesome.min.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/jQuery.lightninBox.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/team.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/portfolio.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/services.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/pogo-slider.min.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/nav.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/aos.css")?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/modelo_site_2/css/style.css")?>" rel="stylesheet" type="text/css" media="all" />
<!-- Owl Carousel  -->
<link rel="stylesheet" href="<?php echo base_url("assets/css/owl.carousel.min.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/owl.theme.default.min.css");?>">
<!-- /css files -->
<!-- fonts -->
<!--<link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">-->
<!-- fonts -->
<!-- js files -->
<script src="<?php echo base_url("assets/modelo_site_2/js/modernizr.js")?>"></script>
<!-- /js files -->
</head>
<body>

<!-- banner section -->
<section class="banner-w3ls">
	<div class="logo-w3ls">
            <a href="index.html"><img class="responsive" src="<?php echo base_url("assets/modelo_site_2/images/lider.png")?>"></a>
	</div>	
	<div class="navigation-w3ls">
            <button id="trigger-overlay" type="button"><i class="fa fa-bars" aria-hidden="true"></i></button>
            <div class="overlay overlay-door">
                <button type="button" class="overlay-close">Close</button>
                <nav>
                    <ul>
                        <li><a href="<?php echo base_url("site/index")?>" class="page-scroll">Home</a></li>
                        <li><a href="#about" class="page-scroll">Sobre</a></li>
                        <li><a href="#service" class="page-scroll">Treinamentos</a></li>                        
                        <li><a href="<?php echo base_url("site/blog/all")?>" class="page-scroll">Blog</a></li>
                        <li><a href="#contact" class="page-scroll">Contato</a></li>
                        <li><a target="_blank" href="<?php echo site_url('login/adm')?>" class="page-scroll">Restrito</a></li>

                    </ul>
                </nav>
            </div>
	</div>
    
    
	<div class="pogoSlider" id="js-main-slider">
		
		
            <?php
            if (isset($painel)) {                        
                foreach ($painel as $row) {
                    ?>
                   <div class="pogoSlider-slide" data-transition="verticalSlide" data-duration="6000"  >
                       <div class="overlay-div"></div>
                       <img src="<?php echo base_url($row->banner)?>" alt="" class="img-responsive">
			<div class="pogoSlider-slide-element">
				<div class="container">
					<h3><?php echo nl2br($row->titulo); ?></h3>
					<p><?php echo nl2br($row->texto); ?> </p>
					<ul class="banner-agileits">
<!--						<li><a href="#readmore" class="link-w3l1" data-toggle="modal">Read More</a></li>
						<li><a href="#contact" class="link-w3l2 page-scroll">Contact Us</a></li>-->
					</ul>
					<a href="#about" class="btn btn-circle page-scroll">
						<i class="fa fa-angle-double-down animated"></i>
					</a>
				</div>	
			</div>
		</div>	
            <?php
            } }
            ?>
	</div> 	
</section>
<!-- /banner section -->
<!-- about section -->
<section class="about-w3l" id="about" data-aos="zoom-in">
	<div class="container">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<h2><?php echo $empresa->titulo_sobre;?></h2>
			<p><?php echo $empresa->texto_sobre;?></p>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
                    <div id="slideshow-banner">
                        <ul>
                             <?php
                            if (isset($fotos_empresa)) {                        
                                foreach ($fotos_empresa as $row) {
                                    ?>
                                <li class="slideshow-item"><img src="<?php echo base_url('uploads/empresa_img/' . $row->unidade_negocio_id) . '/' . $row->nome_arquivo . $row->extensao ?>" alt="" class="img-responsive"/></li>
                            <?php
                            } }
                            ?>
                        </ul>
                    </div>
		</div>
	</div>
</section>
<!-- /about section -->
<!-- services section -->
<section class="service-w3l jarallax" id="service" >
    <!--<div class="overlay-div"></div>-->
    <div class="container">
        <h3 class="text-center"><?php echo $empresa->servico_titulo;?></h3>
        <!--<span class="lidersi" aria-hidden="true"></span>-->
        <?php
        if (isset($treinamentos)) {                        
        foreach ($treinamentos as $row) {
            ?>
            <div class="col-lg-4 col-md-4 col-sm-12 serv-agileinfo1">
                <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree1" data-aos="zoom-in">
                    <ul class="ch-grid">
                        <li>
                            <div class="ch-item">				
                                <div class="ch-info">
                                    <div class="ch-info-front">
                                        <img src="<?php echo base_url('uploads/treinamento/' . $row->id) . '/' . $row->nome_arquivo . $row->extensao ?>"/>
                                    </div>                                    	
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h4 class="text-center"><?php echo $row->descricao;?></h4>
                    <p class="text-center"><?php echo $row->texto;?></p>
<!--                    <div class="text-center">
                        <a href="#" class="btn btn-sucess">Saiba Mais</a>
                    </div>-->
                </div>
                <div class="clearfix"></div>
            </div>
        <?php
        }}
            ?>

        <div class="clearfix"></div>
    </div>
</section>
<!-- /services section -->


<!-- blog section -->
<section class="blog-w3layouts" id="blog">
	<div class="container">
            <h3 class="text-center"><?php echo $empresa->blog_titulo;?></h3>
            <!--<span class="lidersi" aria-hidden="true"></span>-->
            <?php
            if (isset($populares)) {                        
            foreach ($populares as $row) {
            ?>
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="blog-agile" data-aos="zoom-in">
				<a href="<?php echo base_url("site/blog/" . $row->id)?>" class="blog-link1"><h4 class="blog-wthree"><?php echo $row->descricao;?></h4></a>
				<ul class="blog-info">
                                    <li>
                                        <i class="fa fa-calendar"></i> <?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?>
                                        <i class="fa fa-user"></i> <span>Por </span><?php echo $row->nome_usuario;?>
                                    </li>                                                  
				</ul>
				<a data-toggle="modal" href="<?php echo base_url("site/blog/" . $row->id)?>" class="blog-link4">
                                    <div class="hover01 column">
                                        <div>
                                            <figure><img src="<?php echo base_url("uploads/blog/" . $row->id . "/". $row->nome_arquivo . $row->extensao)?>" alt="" class="img-responsive"></figure>
                                        </div>
                                    </div>	
				</a>
                                <p class="blog-p1"><?php echo substr($row->subtitulo,0,150) .'...';?></p>
				<a href="<?php echo base_url("site/blog/" . $row->id)?>" class="blog-link3"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Leia Mais</a>
			</div>
		</div>
                <?php
        }}
                    ?>
		<div class="clearfix"></div>
                
	</div>
    
	<div class="col-md-12 btn_blog ">
            <a href="<?php echo base_url("site/blog/all")?>" class="btn btn-circle page-scroll">
                <i class="fa fa-angle-double-down animated"></i>
            </a>
            
        </div>
</section>
<!-- /blog section -->
<!-- map -->
<!--<div class="map" data-aos="zoom-in">
	<?php echo $empresa->localizacao;?>
</div>-->
<!-- /map -->
<!-- contact section -->
<section class="contact-w3layouts jarallax" id="contact">
    <div class="overlay-div"></div>
	<div class="container">
		<h3 class="text-center"><?php echo $empresa->contato_titulo;?></h3>
		<!--<span class="lidersi" aria-hidden="true"></span>-->
		<div class="col-lg-6 col-md-6 col-sm-12" data-aos="zoom-in">
			<ul class="contact-w3ls">
				<li>
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					<p><?php echo $empresa->endereco . ', ' . $empresa->bairro ;?></p>
				</li>
                                <li>
					<i class="fa fa-map" aria-hidden="true"></i>
					<p><?php echo $empresa->cidade;?></p>
				</li>
				<li>
					<i class="fa fa-mobile" aria-hidden="true"></i>
					<p><a href="tel://<?php echo $empresa->celular; ?>"><?php echo $empresa->celular; ?></a></p>
				</li>
				<li>
					<i class="fa fa-envelope-o" aria-hidden="true"></i>
					<p><a href="mailto:<?php echo $empresa->email_contato;?>"><?php echo $empresa->email_contato;?></a></p>
				</li>
			</ul>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12" data-aos="zoom-in"> 
			<form action="<?php echo base_url("contato/create_contato_site"); ?>" method="post">
                            <div class="col-lg-12 col-md-12 col-sm-6">
                                <div class="control-group form-group">
                                        <div class="controls">
                                        <label>Nome Completo</label>
                                        <input type="text" class="form-control" id="nome" name="nome" required data-validation-required-message="Informe seu nome.">
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">	
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <label>Email</label>
                                        <input type="email" class="form-control" id="email" name="email" required data-validation-required-message="Informe seu melhor email.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">	
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <label>Celular</label>
                                        <input type="text" class="form-control" id="telefone" name="telefone" required data-validation-required-message="Informe telefone.">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12">	
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <label>Mensagem</label>
                                        <textarea rows="10" cols="100" class="form-control" id="mensagem" name="mensagem"  required data-validation-required-message="Escreva sua mensagem" maxlength="9999" style="resize:none"></textarea>
                                    </div>
                                </div>
                            </div>	
                            <div id="success"></div>
                            <!-- For success/fail messages -->
                            <div class="col-lg-12 col-md-12 col-sm-12">    
                                <button type="submit" class="btn btn-primary">Enviar Mensagem</button>
                            </div>
                            <div class="clearfix"></div>	
                        </form>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<!-- /contact section -->

<!-- copyright section -->
<div class="copright-w3">
	<div class="container">
<!--		<div class="col-lg-6 col-md-6 col-sm-6">
			<ul class="link-w3l cl-effect-21">
				<li><a href="#service" class="page-scroll">Services</a></li>
				<li><a href="#portfolio" class="page-scroll">Portfolio</a></li>
				<li><a href="#team" class="page-scroll">Team</a></li>
				<li><a href="#blog" class="page-scroll">Blog</a></li>
			</ul>
		</div>-->
		<div class="col-lg-6 col-md-6 col-sm-6">
			<p class="copyright">Desenvolvido por <a href="https://www.coopertecsolucoes.com.br/" target="_blank">Coopertec Soluções</a></p>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- /copyright section -->
<!-- back to top -->
<a href="#0" class="cd-top">Top</a>
<!-- /back to top -->
<!-- js files -->
<script src="<?php echo base_url("assets/modelo_site_2/js/jquery.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/bootstrap.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/smoothscroll.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/aos.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/lazyload.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/jquery.easing.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/grayscale.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/jarallax.js")?>"></script>
<script type="text/javascript">
    $('.jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1680,
        imgHeight: 925
    })
</script>
<!-- js for back to top -->
<script src="<?php echo base_url("assets/modelo_site_2/js/main2.js")?>"></script>
<!-- /js for back to top -->
<!-- js for footer -->
<script src="<?php echo base_url("assets/modelo_site_2/js/jQuery.lightninBox.js")?>"></script>
<script type="text/javascript">
	$(".lightninBox").lightninBox();
</script>
<!-- /js for footer -->
<!-- js for portfolio -->
<script src="<?php echo base_url("assets/modelo_site_2/js/imagesloaded.pkgd.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/masonry.pkgd.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/classie2.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/cbpGridGallery.js")?>"></script>
<script>
	new CBPGridGallery( document.getElementById( 'grid-gallery' ) );
</script>
<!-- /js for portfolio -->
<!-- js for about section -->
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site_2/js/jquery.slide.js")?>"></script>
<script type="text/javascript">
	$('#slideshow-banner').slide({
		cdTime : 6000,      
	    controllerLeftButton : "assets/modelo_site_2/images/left.png",   
	    controllerRightButton : "assets/modelo_site_2/images/right.png"   
	});
</script>
<!-- /js for about section -->

<!-- js for banner -->
<script src="<?php echo base_url("assets/modelo_site_2/js/jquery.pogo-slider.min.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/main.js")?>"></script>
<!-- /js for banner -->
<!-- js for navigation -->
<script src="<?php echo base_url("assets/modelo_site_2/js/classie.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site_2/js/demo1.js")?>"></script>
<!-- /js for navigation -->

<!-- /js files -->
</body>
</html>