<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cliente <small>Edição da Cliente</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('pessoa/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->nome_fantasia ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('pessoa/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Cliente</a></li>
                        <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("pessoa/update_Pessoa"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="razao_social">Razão Social <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="razao_social" name="razao_social" required="required" value="<?php echo $result->razao_social; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome_fantasia">Nome Fantasia <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="nome_fantasia" name="nome_fantasia" required="required" value="<?php echo $result->nome_fantasia; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnpj">CNPJ <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="cnpj" name="cnpj" required="required" value="<?php echo $result->cnpj; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnpj">Inscrição Estadual <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="inscricao_estadual" name="inscricao_estadual"  value="<?php echo $result->inscricao_estadual; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cidade_id">Cidade <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-2 col-xs-12">
                                    <select name="cidade_id" class="form-control">
                                        <?php
                                        foreach ($cidades as $cid) {
                                            $selected = '';
                                            if($result->cidade_id == $cid->id){
                                                $selected = 'selected';
                                            }
                                            ?>
                                        <option <?php echo $selected ?> value="<?php echo $cid->id ?>"><?php echo $cid->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="endereco">Endereço <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="endereco" required="required" value="<?php echo $result->endereco; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bairro">Bairro <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="bairro" name="bairro" required="required" value="<?php echo $result->bairro; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cep">CEP <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="cep" name="cep" required="required" value="<?php echo $result->cep; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone">Telefone <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="telefone" name="telefone" required="required" value="<?php echo $result->telefone; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="email" required="required" value="<?php echo $result->email; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="condicao_pagamento_id">Cond. Pagamento Padrão <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-2 col-xs-12">
                                    <select name="condicao_pagamento_id" class="form-control">
                                        <?php
                                        foreach ($cond_pgto as $pgto) {
                                            $selected = '';
                                            if($result->condicao_pagamento_id == $pgto->id){
                                                $selected = 'selected';
                                            }
                                            ?>
                                        <option <?php echo $selected ?> value="<?php echo $pgto->id ?>"><?php echo $pgto->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('pessoa/index') ?>" class="btn btn-primary">Voltar</a>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#cnpj').mask("99.999.999/9999-99");  
        $('#telefone').mask("(99)99999-9999");  
        $('#cep').mask("99999-999");  

    });
</script>
<!-- /form validation -->
