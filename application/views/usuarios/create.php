<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Users</h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('usuarios/index') ?>">List</a>
                            </li>
                            <li class="active">
                                <strong>New User</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <form id="formulario" method="POST" action="<?php echo base_url("usuarios/create_usuario"); ?>" data-toggle="validator" class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <?php
                                if ($_SESSION['is_admin']) {
                            ?>                            
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" >User Group <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select id="usuario_grupo_id" name="usuario_grupo_id" class="form-control" required>
                                            <option value="">Select...</option>
                                            <?php foreach ($usuario_grupos as $grp) { ?>
                                                <option value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario"><span class="required">*</span> User
                                </label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="usuario" name="usuario" required="required" value="<?php echo set_value('usuario'); ?>" class="form-control col-md-7 col-xs-12 validate[required]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="senha" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="required">*</span> Password</label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input id="senha" class="form-control col-md-7 col-xs-12  validate[required],minSize[5]]" type="password" name="senha"  minlength="5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="confirma_senha" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="required">*</span> Confirm Password</label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input id="confirma_senha" class="form-control col-md-7 col-xs-12  validate[required,equals[senha],minSize[5]]" type="password" name="confirma_senha"  minlength="5">
                                </div>
                            </div>
                            <?php
                                if ($_SESSION['is_admin']) {
                            ?>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Access Type</label>
                                    <div class="col-md-3 col-sm-2 col-xs-12">
                                        <select class="form-control" name="tipo" id="tipo">
                                            <option value="A">Master</option>
                                            <option value="T">Basic</option>
                                        </select>
                                    </div>
                                </div>
                            
                            <?php
                                }
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome"><span class="required">*</span> Name 
                                </label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="nome" name="nome" required="required" value="<?php echo set_value('nome'); ?>" class="form-control col-md-7 col-xs-12  validate[required]" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email"><span class="required">*</span> Email
                                </label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control col-md-7 col-xs-12 validate[custom[email]]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Active</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('usuarios/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->

<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
    });

</script>
<!-- /form validation -->
