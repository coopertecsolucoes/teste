<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Business Subsidiary <small>New Business Subsidiary</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('usuarios/index') ?>">List</a>
                            </li>
                            <li class="active">
                                <strong>New User Business Subsidiary</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="formulario" method="POST" action="<?php echo base_url("usuarios/create_unidade_negocio"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                          <input type="hidden" name="usuario_id" id="cotacao_id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" for="unidade_negocio_id">Business Subsidiary <span class="required">*</span></label>
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="unidade_negocio_id" class="form-control">
                                        <option value="">Select...</option>
                                        <?php foreach ($Unidade_negocio  as $uni) { ?>
                                            <option value="<?php echo $uni->id ?>"><?php echo $uni->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                            </div>    
                            <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-2 col-xs-12">Standard</label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <select class="form-control" name="padrao" id="padrao">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Add</button>
                                    <a href="<?php echo site_url('usuarios/edit/'. $result->id) ?>" class="btn btn-primary">Return</a>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><small>Business Subsidiary do User</small></h2>                       
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="example" class="display responsive no-wrap" cellspacing="0" width="70%">
                            <thead>
                                <tr>
                                    <th class="text-left">Description</th>
                                    <th class="text-left">Padrão</th>
                                    <th class="text-left">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($Usuario_unidade)) {
                                    foreach ($Usuario_unidade as $row) {
                                        if ($row->padrao == 1) {
                                            $padrao = 'Yes';
                                        } else {
                                            $padrao = 'No';
                                        }
                                        ?>
                                        <tr class="text-left">
                                            <td><?php echo str_replace('.', ',', $row->descricao); ?></td>
                                            <td><?php echo str_replace('.', ',', $padrao); ?></td>
                                            <td>
                                                <button id="btn_delete" onclick="deleteConfirm('<?php echo site_url('usuarios/delete_unidade_negocio/' . $row->id) ?>')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /page content -->

<!-- form validation -->

<script type="text/javascript">
    $(document).ready(function () {

        $("#formulario").validationEngine();    
    });
    function deleteConfirm(url) {
    bootbox.confirm("Are you sure you want to delete?", function (res) {
        if (res === true) {
            window.location = url;
        } else {
            return res;
        }
    });
    };
</script>
<!-- /form validation -->
