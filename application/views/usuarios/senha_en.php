<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Change Password</h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="">User</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->usuario ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <form id="formulario" method="POST" action="<?php echo base_url("usuarios/update_troca_senha"); ?>" data-toggle="validator" class="form-horizontal form-label-left">

                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="senha" class="control-label col-md-2 col-sm-2 col-xs-12">New Password </label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input id="senha" class="form-control col-md-7 col-xs-12 validate[minSize[5]]"  type="password" name="senha"  minlength="5">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Change</button>
                                    <a href="<?php echo site_url('login/home') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
    });

</script>
<!-- /form validation -->
