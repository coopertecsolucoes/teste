<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Users</h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('usuarios/index') ?>">List</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->usuario ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-primary" href="<?php echo site_url('usuarios/unidade_negocio/' . $result->id) ?>" ><i class="fa fa-building"></i> Business Subsidiary</a></li>
                        <br />
                        <form id="formulario" enctype="multipart/form-data" method="POST" action="<?php echo base_url("usuarios/update_usuario"); ?>" data-toggle="validator" class="form-horizontal form-label-left">

                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <?php
                                if ($_SESSION['is_admin']) {
                            ?>                             
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario_grupo_id">User Group <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="usuario_grupo_id" class="form-control">
                                            <?php
                                            foreach ($usuario_grupos as $grp) {
                                                $selected = '';
                                                if ($result->usuario_grupo_id == $grp->id) {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario"><span class="required">*</span> User</label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="usuario" name="usuario" required="required" value="<?php echo $result->usuario ?>" class="form-control col-md-7 col-xs-12 validate[required]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="senha" class="control-label col-md-2 col-sm-2 col-xs-12">New Password </label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input id="senha" class="form-control col-md-7 col-xs-12 validate[minSize[5]]"  type="password" name="senha"  minlength="5">
                                </div>
                            </div>

                            <?php
                                if ($_SESSION['is_admin']) {
                            ?>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12"><span class="required">*</span> Access Type</label>
                                    <div class="col-md-3 col-sm-2 col-xs-12">
                                        <select class="form-control" name="tipo" id="tipo">
                                            <option value="A" <?php echo ($result->tipo == "A") ? 'selected' : null ?>>Master</option>
                                            <option value="T" <?php echo ($result->tipo == "T") ? 'selected' : null ?>>Basic</option>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome"><span class="required">*</span> Name</label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="nome" name="nome" required="required" value="<?php echo $result->nome; ?>" class="form-control col-md-7 col-xs-12 validate[required]" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email"><span class="required">*</span> Email</label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="email" required name="email" value="<?php echo $result->email; ?>" class="form-control col-md-7 col-xs-12 validate[required,custom[email]]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"><span class="required">*</span> Active</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Active</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="logo">Foto</label>
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <img style="width: 100%; display: block;" src="<?php echo base_url($result->foto) ?>"  />
                                      
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p style="margin-left: 10px; margin-top: 10px;">Sugestão de tamanho: 200px X 80px</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="sobre">Sobre </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea  id="sobre" name="sobre"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->sobre ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="facebook"> Facebook</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="facebook"  name="facebook" value="<?php echo $result->facebook; ?>" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="instagram"> Instagram</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="instagram"  name="instagram" value="<?php echo $result->instagram; ?>" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('usuarios/index') ?>" class="btn btn-primary">Voltar</a>
                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
    });

</script>
<!-- /form validation -->
