<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url("assets/images") ?>/favicon.png">

        <title>Painel de Controle </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">

        <link href="<?php echo base_url("assets/fonts/css/font-awesome.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/icheck/flat/green.css"); ?>" rel="stylesheet" />

        <script src="<?php echo base_url("assets/js/jquery.min.js"); ?>"></script>

        <!--[if lt IE 9]>
              <script src="<?php echo base_url("assets/js/ie8-responsive-file-warning.js"); ?>"></script>
              <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

    </head>
    <style>
        body{
            background:url('assets/images/img_bg_3.jpg') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }        

    </style>

    <body>


        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper">
                <div id="login">
<!--                <div id="login" class="panel">-->
                    <div class="panel-heading">

                    </div> 
                    <div style="padding-top:50px; text-align: center;" class="panel-body" >
                        <img src="<?php echo base_url("assets/images/logo2.png"); ?>" class="img-logo" alt="Logo"/>
                        <form id="formulario" method="POST" action="<?php echo base_url("login/login"); ?>" class="form-horizontal" data-toggle="validator" role="login">
                          
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" class="form-control" placeholder="User" title="Please, complete this data." required name="usuario" />
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" class="form-control" placeholder="Password" required name="senha" />
                            </div>
                            <span class="help-block" style="color: #cb1f1f"><?php echo $this->session->flashdata('erro_login') ?></span>
                            
                            <div style="margin-top:10px" class="input-group">
                                 <span class="input-group-btn">
                                     <button type="submit" class="btn btn-primary" style="width: 100%">Login</button>
                                 </span>                      
                            </div>
                            <div style="margin-top:10px" class="input-group" >
                                 <p >
<!--                                    <a class="color:#FFFFFF " href="mailto:obaldin@guentner.com.br?subject=Recovery Password - GPS Brasil.&body=Olá,%0D%0APlease, I need recovery the password of GPS Brasil Software. %0D%0APlease, I need recovery the password of GPS Brasil Software." >
                                        Forgot password?
                                    </a> -->
                                 </p>
                            </div>
                        </form>
                    </div>
                    <!-- form -->

                    <!-- content -->
                </div>

            </div>
        </div>
    </body>

</html>
