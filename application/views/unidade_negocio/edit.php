<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Unidade de Negócio <small>Editar Unidade de Negócio</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('unidade_negocio/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->nome_fantasia ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-primary" href="<?php echo site_url('unidade_negocio/imagens/' . $result->id) ?>" ><i class="fa fa-plus-circle"></i> Fotos Sobre</a>
                    
                        <form id="formulario" enctype="multipart/form-data" method="POST" action="<?php echo base_url("unidade_negocio/update_Unidade_negocio"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                    <span class="red"><?php echo $this->session->flashdata('alerta_erro') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome_fantasia">Nome Fantasia <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="nome_fantasia" name="nome_fantasia" required="required" value="<?php echo $result->nome_fantasia; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnpj">CNPJ 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="cnpj" name="cnpj"  value="<?php echo $result->cnpj; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                                                       
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cidade">Cidade <span class="required">*</span></label>
                                  <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="cidade" name="cidade" required="required" value="<?php echo $result->cidade; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="endereco">Endereço <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="endereco" name="endereco" required="required" value="<?php echo $result->endereco; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bairro">Bairro <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="bairro" name="bairro" required="required" value="<?php echo $result->bairro; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cep">CEP <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="cep" name="cep" required="required" value="<?php echo $result->cep; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone">Telefone <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="telefone" name="telefone" required="required" value="<?php echo $result->telefone; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="celular">Celular <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="celular" name="celular" required="required" value="<?php echo $result->celular; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="fanpage">FanPage 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="fanpage" name="fanpage"  value="<?php echo $result->fanpage; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="instagram">Instagram 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="instagram" name="instagram"  value="<?php echo $result->instagram; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="skype">Skype 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="skype" name="skype"  value="<?php echo $result->skype; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email_envio">Email Contato <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="email_contato" name="email_contato" required="required" value="<?php echo $result->email_contato; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                      
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="logo">Logo</label>
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <img style="width: 100%; display: block;" src="<?php echo base_url($result->logo) ?>" alt="<?php echo $fot->nome_arquivo ?>" />
                                      
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p style="margin-left: 10px; margin-top: 10px;">Sugestão de tamanho: 200px X 80px</p>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="titulo_sobre">Título Sobre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="titulo_sobre" name="titulo_sobre" required="required" value="<?php echo $result->titulo_sobre; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="texto_sobre">Texto Sobre </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="texto_sobre" name="texto_sobre"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->texto_sobre ?></textarea>
                                </div>
                            </div>  
                            
     
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_titulo">Título Serviço <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="servico_titulo" name="servico_titulo" required="required" value="<?php echo $result->servico_titulo; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_texto">Texto Serviço </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="servico_texto" name="servico_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->servico_texto ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="galeria_titulo">Título Galeria <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="galeria_titulo" name="galeria_titulo" required="required" value="<?php echo $result->galeria_titulo; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_texto">Texto Galeria </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="5000" id="galeria_texto" name="galeria_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->galeria_texto ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="contato_titulo">Título Contato <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="contato_titulo" name="contato_titulo" required="required" value="<?php echo $result->contato_titulo; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="localizacao_titulo">Título Localização <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="localizacao_titulo" name="localizacao_titulo" required="required" value="<?php echo $result->localizacao_titulo; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                         
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cliente_titulo">Título Cliente 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="cliente_titulo" name="cliente_titulo"  value="<?php echo $result->cliente_titulo; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                               
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cliente_texto">Título Texto <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="cliente_texto" name="cliente_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->cliente_texto ?></textarea>
                                    
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="localizacao">Localização </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="localizacao" name="localizacao"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->localizacao ?></textarea>
                                </div>
                            </div> 
                            
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="topico1">Tópico 1
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="topico1" name="topico1"  value="<?php echo $result->topico1; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_texto">Texto Tópico 1 </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="topico1_texto" name="topico1_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->topico1_texto ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="topico1">Tópico 2
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="topico2" name="topico2"  value="<?php echo $result->topico2; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_texto">Texto Tópico 2 </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="topico2_texto" name="topico2_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->topico2_texto ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="topico1">Tópico 3
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="topico3" name="topico3"  value="<?php echo $result->topico3; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="servico_texto">Texto Tópico 3 </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="topico3_texto" name="topico3_texto"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->topico3_texto ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="localizacao_titulo">Mensagem WhatsApp <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="msg_whats" name="msg_whats" required="required" value="<?php echo $result->msg_whats; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                     <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('unidade_negocio/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">
    $(document).ready(function () {
       
        $('#formulario .btn').on('click', function () {
            $('#formulario').parsley().validate();
            validateFront();
        });
        $('#cnpj').mask("99.999.999/9999-99");  
        $('#telefone').mask("(99) 9999-9999");  
         $('#celular').mask("(99) 9 9999-9999");  
        $('#cep').mask("99999-999");  
        var validateFront = function () {
            if (true === $('#formulario').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });

    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
<!-- /form validation -->
