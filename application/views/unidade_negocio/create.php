<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Business Subsidiary <small>New Business Subsidiary</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('unidade_negocio/index') ?>">List</a>
                            </li>
                            <li class="active">
                                <strong>New Business Subsidiary</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('unidade_negocio/create') ?>" ><i class="fa fa-plus-circle"></i> New Business Subsidiary</a></li>
                        <br />
                        <form id="formulario" method="POST" action="<?php echo base_url("unidade_negocio/create_Unidade_negocio"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Description <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="idioma">Standard Language</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="idioma" id="idioma">
                                        <option value="1">Portuguese</option>
                                        <option value="2">English</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="moeda">Standard Coin</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="moeda" id="moeda">
                                        <option value="">Select...</option>
                                        <option value="R$">Real - R$</option>
                                        <option value="$">Dolar - $</option>
                                        <option value="€">Euro - €</option>  
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" >User Group <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <select id="usuario_grupo_id" name="usuario_grupo_id" class="form-control" required>
                                        <option value="">Select...</option>
                                        <?php foreach ($usuario_grupos as $grp) { ?>
                                            <option value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="fator_mercado">Market Factor <span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="fator_mercado" name="fator_mercado" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ativo">Active</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo site_url('unidade_negocio/index') ?>" class="btn btn-primary">Cancel</a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#fator_mercado').mask("#0,00", {reverse: true});
    });
    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
