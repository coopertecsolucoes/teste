<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url("assets/js/progressbar/bootstrap-progressbar.min.js"); ?>"></script>

<!-- icheck -->
<script src="<?php echo base_url("assets/js/icheck/icheck.min.js"); ?>"></script>
<!-- jqquery mask -->
<script src="<?php echo base_url("assets/js/mask/jquery.mask.js"); ?>"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url("assets/js/moment/moment.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/datepicker/daterangepicker.js"); ?>"></script>

<!-- pace -->
<script src="<?php echo base_url("assets/js/pace/pace.min.js"); ?>"></script>

<!-- PNotify -->
<script type="text/javascript" src="<?php echo base_url("assets/js/notify/pnotify.core.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/notify/pnotify.buttons.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/notify/pnotify.nonblock.js"); ?>"></script>

<!-- CUSTOM -->
<script src="<?php echo base_url("assets/js/custom.js"); ?>"></script>

<!-- skycons -->
<script src="<?php echo base_url("assets/js/skycons/skycons.min.js"); ?>"></script>

<!--validation-->
<script src="<?php echo base_url("assets/js/validation/js/jquery.validationEngine.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/validation/js/languages/jquery.validationEngine-pt_BR.js"); ?>"></script> 
<script src="<?php echo base_url("assets/js/editor.js"); ?>"></script>
<script>
    var icons = new Skycons({
        "color": "#73879C"
    }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

    for (i = list.length; i--; )
        icons.set(list[i], list[i]);

    icons.play();
</script>

<?php
if ($this->session->flashdata('alerta_erro')) {
    ?>
    <script>
        notice = new PNotify({
            title: 'Erro',
            text: '<?php echo $this->session->flashdata('alerta_erro'); ?>',
            type: 'error',
            mouse_reset: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });

    </script>
    <?php
}
if ($this->session->flashdata('alerta_sucesso')) {
    ?>
    <script>
        notice = new PNotify({
            title: 'Sucesso',
            text: '<?php print $this->session->flashdata('alerta_sucesso') ?>',
            type: 'success',
            mouse_reset: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });

    </script>
    <?php
}
?>
<script>
    NProgress.done();
</script>
<!-- /footer content -->
</body>

</html>