<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($_SESSION['usuario_id'] == null) {
    redirect('/login/login');
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url("assets/images") ?>/favicon.png">

        <title>Área Restrita</title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">

        <link href="<?php echo base_url("assets/fonts/css/font-awesome.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/maps/jquery-jvectormap-2.0.3.css"); ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url("assets/css/icheck/flat/green.css"); ?>" rel="stylesheet" />
        <link href="<?php echo base_url("assets/css/floatexamples.css"); ?>" rel="stylesheet" type="text/css" />


        <link href="<?php echo base_url("assets/js/datatables/DataTables-1.10.11/css/dataTables.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/js/datatables/Responsive-2.0.2/css/responsive.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />


        <script src="<?php echo base_url("assets/js/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/nprogress.js"); ?>"></script>
        <!-- Bootbox -->
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootbox/bootbox.min.js"); ?>"></script>
        <!-- Form Validator -->
        <script type="text/javascript" src="<?php echo base_url("assets/js/validator/validator.js"); ?>"></script>
        <!--<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/js/validation/css/validationEngine.jquery.css"); ?>">

        <link href="<?php echo base_url("assets/css/editor.css"); ?>" rel="stylesheet">
       
            
        <!--[if lt IE 9]>
              <script src="<?php echo base_url("assets/js/ie8-responsive-file-warning.js"); ?>"></script>
              <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->
  <link rel="shortcut icon" href="<?php echo base_url("assets/images") ?>/logo.ico">
    </head>


    <body class="nav-md">

        <div class="container body">

            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo site_url('login/home') ?>" class="site_title" style="text-align: center;" ><img style=" width: 160px; height: 60px;" src="<?php echo base_url( $_SESSION['logo'])?>" /></a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3 col-sm-2 col-xs-12">

                        </div>
                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_info">
                                <span>Bem vindo,</span>
                                <h2><?php
                                    echo $_SESSION['nome'];
                                    ?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->

                        <br />
                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <ul class="nav side-menu">
                                    <?php
                                    $tipo_menu = '0';
                                    $fechar_tipo_menu = 'NAO';
                                    $menus = $_SESSION['menus'];
                                    foreach ($menus as $mnu) {
                                        if ($mnu->tipo_menu_id != $tipo_menu) {
                                            if ($fechar_tipo_menu == 'SIM') {
                                                ?>                                            
                                            </ul >
                                            </li>
                                            <?php
                                        }
                                        $tipo_menu = $mnu->tipo_menu_id;
                                        ?>                                             
                                        <li><a><i class="fa fa-edit"></i><?php echo $mnu->desc_tipo_menu ?> <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <?php
                                                $fechar_tipo_menu = 'SIM';
                                            }
                                            ?>                                             
                                            <li><a href="<?php echo site_url($mnu->menu) ?>"><i class="fa fa-home"></i> <?php echo $mnu->desc_menu ?></a></li>
                                            <?php
                                        }
                                        ?>

                                    </ul>
                            </div>
                            <!--                             <div class="menu_section">
                                                            <ul class="nav side-menu">
                            
<?php
if ($_SESSION['is_admin'] == true) {
    ?>
                                                                        <li><a><i class="fa fa-edit"></i> Cadastros <span class="fa fa-chevron-down"></span></a>
                                                                        <ul class="nav child_menu">
                                                                            <li><a href="<?php echo site_url('tipo_menu/index') ?>"><i class="fa fa-home"></i> Tipo Menu</a></li>
                                                                            <li><a href="<?php echo site_url('menu/index') ?>"><i class="fa fa-home"></i> Menu</a></li>
                                                                            <li><a href="<?php echo site_url('usuario_grupo/index') ?>"><i class="fa fa-home"></i> Grupo User</a></li>
                                                                            <li><a href="<?php echo site_url('unidade_negocio/index') ?>"><i class="fa fa-home"></i> Unidade Negócio</a></li>
                                                                            <li><a href="<?php echo site_url('usuarios/index') ?>"><i class="fa fa-home"></i> Users</a></li>
                                                                            <li><a href="<?php echo site_url('acessorios/index') ?>"><i class="fa fa-home"></i> Acessórios</a></li>
                                                                            <li><a href="<?php echo site_url('nota/index') ?>"><i class="fa fa-home"></i> Notas</a></li>
                                                                            <li><a href="<?php echo site_url('alimentacoes/index') ?>"><i class="fa fa-home"></i> Alimentação Elétrica</a></li>
                                                                            <li><a href="<?php echo site_url('fluidos_refrigerantes/index') ?>"><i class="fa fa-home"></i> Refrigerantes</a></li>
                                                                            <li><a href="<?php echo site_url('tipo_aplicacao/index') ?>"><i class="fa fa-home"></i> Tipo Aplicação</a></li>
                                                                            <li><a href="<?php echo site_url('aplicacao/index') ?>"><i class="fa fa-home"></i> Aplicação</a></li>
                                                                            <li><a href="<?php echo site_url('tipo_series/index') ?>"><i class="fa fa-home"></i> Tipo Séries</a></li>
                                                                            <li><a href="<?php echo site_url('series/index') ?>"><i class="fa fa-home"></i> Séries</a></li>                                            
                                                                            
                                                                            <li><a href="<?php echo site_url('importacao/index') ?>"><i class="fa fa-home"></i> Importar Tabela</a></li>
                                                                            <li><a href="<?php echo site_url('tabela_refrigerante/index') ?>"><i class="fa fa-home"></i> Importar Refrigerantes</a></li>
                                                                            <li><a href="<?php echo site_url('tabela_refrigerante_cp/index') ?>"><i class="fa fa-home"></i> Importar Refrigerantes CP</a></li>
                                                                            <li><a href="<?php echo site_url('produtos/index') ?>"><i class="fa fa-home"></i> Produtos</a></li>
                                                                            <li><a href="<?php echo site_url('produtos/copiar_produtos') ?>"><i class="fa fa-home"></i> Copiar Produtos</a></li>
                                
                                                                        </ul>
                                                                        </li>
                                                                      <li><a href="<?php echo site_url('GPS/aplicacao') ?>"><i class="fa fa-calculator"></i> Güntner Product Selector</a></li>
    <?php
    if ($_SESSION['idioma'] == 1) {
        ?>
                                                                                <li><a href="<?php echo site_url('usuarios/senha/' . $_SESSION['usuario_id']) ?>"><i class="fa fa-key"></i> Trocar Senha</a></li>
                                <?php } else { ?>
                                                                                <li><a href="<?php echo site_url('usuarios/senha_en/' . $_SESSION['usuario_id']) ?>"><i class="fa fa-key"></i> Change Password</a></li>
                                <?php } ?>                                        
                                                                        
                                                                        <li><a href="<?php echo site_url('report/rel_acessos') ?>"><i class="fa fa-dashboard"></i> Relatório de Acessos</a></li>
                            <?php } else { ?>
                                                                        <li><a href="<?php echo site_url('GPS/aplicacao') ?>"><i class="fa fa-calculator"></i> Güntner Product Selector</a></li>
    <?php
    if ($_SESSION['idioma'] == 1) {
        ?>
                                                                                <li><a href="<?php echo site_url('usuarios/senha/' . $_SESSION['usuario_id']) ?>"><i class="fa fa-key"></i> Trocar Senha</a></li>
                                <?php } else { ?>
                                                                                <li><a href="<?php echo site_url('usuarios/senha_en/' . $_SESSION['usuario_id']) ?>"><i class="fa fa-key"></i> Change Password</a></li>
                                <?php } ?>                                        
                                                                        
                            <?php } ?>
                            
                                                            </ul>
                                                        </div>-->

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <!--
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        -->
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>                              

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
<?php echo $_SESSION['usuario'] ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li>
                                            <a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-sign-out pull-right"></i> Logout</a>

                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
<?php echo $_SESSION['uni_negocio_desc'] ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <?php
                                        $unidades_neg = $_SESSION['unidades'];
                                        foreach ($unidades_neg as $uni) {
                                            $selected = '';
                                            if ($_SESSION['uni_negocio_id'] != $uni->unidade_negocio_id) {
                                                ?>
                                                <li>
                                                    <a href="<?php echo base_url('Troca_empresa/Trocar') . "/" . $uni->unidade_negocio_id ?>"><i class="fa fa-sign-out pull-right"></i><?php echo $uni->nome_fantasia ?></a>
                                                </li>   

        <?php
    }
}
?>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->
                <script type="text/javascript">
                    // Selecionamos o menu dropdown, que possui os valores possíveis:
                    var uni_negocio = document.getElementById("unidade_negocio_id");

                    // Requisitamos que a função handler (que copia o valor selecionado para a caixa de texto) [...]
                    // [...] seja executada cada vez que o valor do menu dropdown mude:
                    uni_negocio.addEventListener("change", function () {
                        // Como este código é executado após cada alteração, sempre obtemos o valor atualizado:
                        var valor_selecionado = uni_negocio.options[uni_negocio.selectedIndex].value;
                        '<%Session["uni_negocio_id"] = "' + valor_selecionado + '"; %>';
                    });

                    $(document).ready(function () {
                        $('#formulario .btn').on('click', function () {
                            $('#formulario').parsley().validate();
                            validateFront();
                        });
                    });


                </script>