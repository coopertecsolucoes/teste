<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Serviços <small>Edição de Serviço</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('servico/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->descricao ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-primary" href="<?php echo site_url('servico/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Serviço</a></li>
                        <a class="btn btn-primary" href="<?php echo site_url('servico/imagens/' . $result->id) ?>" ><i class="fa fa-plus-circle"></i> Fotos</a></li>
                        <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("servico/update_servico"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>

                                                 
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição *</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" value="<?php echo $result->descricao ?>" required="required" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="texto">Texto </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="255" id="texto" name="texto"  rows="3" class="form-control col-md-7 col-xs-12"><?php echo $result->texto ?></textarea>
                                </div>
                            </div>      
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="detalhe">Detalhes </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="detalhe" name="detalhe"  rows="5" class="form-control col-md-7 col-xs-12"><?php echo $result->detalhe ?></textarea>
                                </div>
                            </div>   
               
                             <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ordem">Ordem *</label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="ordem" name="ordem" value="<?php echo $result->ordem ?>" required="required" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>
                        
                            
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('servico/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            
        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script src="<?php echo base_url("assets/js/ckeditor/ckeditor.js"); ?>"></script>
<script type="text/javascript">

    $(document).ready(function () {
        
    });
</script>
<!-- /form validation -->

