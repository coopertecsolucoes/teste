<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Serviços <small>Novo Serviço</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('servico/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong>Novo Serviço</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('servico/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Serviço</a></li>
                        <br />
                        <form id="formulario" method="POST" action="<?php echo base_url("servico/create_servico"); ?>" class="form-horizontal form-label-left" data-toggle="validator">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                                                 
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="texto">Texto 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="255" id="texto" name="texto" rows="3" class="form-control col-md-7 col-xs-12"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="detalhe">Detalhes 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="detalhe" name="detalhe" rows="5" class="form-control col-md-7 col-xs-12"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                              
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ordem">Ordem <span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="ordem" name="ordem" required="required" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ativo">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>                                    
                                    <a href="<?php echo site_url('servico/index') ?>" class="btn btn-primary">Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>
</div>

</div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#formulario").validationEngine();
        
    });

</script>
<!-- /page content -->
