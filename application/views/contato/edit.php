<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contato<small>Edição de Contato</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('contato/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->nome ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('contato/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Contato</a></li>
                        
                    <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("contato/update_contato"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="data">Data Abertura
                                </label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="data" name="data" value="<?php echo isset($result->data) ? date("d/m/Y", strtotime($result->data)) : null ?>"  maxlength="10"class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome">Nome do Contato 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="nome" name="nome" value="<?php echo $result->nome;?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone">Telefone
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="telefone" name="telefone" value="<?php echo $result->telefone;?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="email" name="email" value="<?php echo $result->email;?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="assunto">Assunto
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="assunto" name="assunto" value="<?php echo $result->assunto;?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mensagem">Mensagem 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="mensagem" name="mensagem" rows="3" class="form-control col-md-7 col-xs-12"><?php echo $result->mensagem;?></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="observacoes">Observações 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea maxlength="2000" id="observacoes" name="observacoes" rows="3" class="form-control col-md-7 col-xs-12"><?php echo $result->observacoes;?></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select disabled="true" class="form-control" name="status" id="status">
                                        <option value="1" <?php echo ($result->status == 1) ? 'selected' : null ?>>Aberto</option>
                                        <option value="2" <?php echo ($result->status == 2) ? 'selected' : null ?>>Encerrado</option>
                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('contato/index') ?>" class="btn btn-primary">Voltar</a>  
                                    <?php
                                        if($result->status == 1){
                                    ?>
                                        <a href="<?php echo site_url('contato/encerrar/' . $result->id) ?>" class="btn btn-primary">Encerrar</a>                                    
                                    <?php } else { ?>
                                        <a href="<?php echo site_url('contato/reabrir/' . $result->id) ?>" class="btn btn-primary">Reabrir</a>                                    
                                    <?php } ?>
                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->

<link rel="stylesheet" href="assets/css/bootstrap-select.css">

<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario .btn').on('click', function () {
            $('#formulario').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#formulario').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });


</script>
<!-- /form validation -->
