<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contato <small>Lista de Contato</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li class="active">
                                <strong>Listagem</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('contato/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Contato</a></li>
                        <br />
                        <table id="example" class="display responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Aberto em</th> 
                                    <th>Nome</th> 
                                    <th>Telefone</th>
                                    <th>Mensagem</th>      
                                    <th>Status</th>    
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($result)) {
                                    foreach ($result as $row) {
                                        if ($row->status == 1) {
                                            $status = 'Aberto';
                                        } else {
                                            $status = 'Encerrado';
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo  str_replace('.', ',', isset($row->data) ? date("d/m/Y", strtotime($row->data)) : null); ?></td>
                                            <td><?php echo $row->nome; ?></td> 
                                            <td><?php echo $row->telefone; ?></td> 
                                            <td><?php echo $row->mensagem; ?></td> 
                                            <td><?php echo $status; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('contato/edit/' . $row->id) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                                <button id="btn_delete" onclick="deleteConfirm('<?php echo site_url('contato/delete/' . $row->id) ?>')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Apagar </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<script src="<?php echo base_url("assets/js/datatables/DataTables-1.10.11/js/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/DataTables-1.10.11/js/dataTables.bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/Responsive-2.0.2/js/dataTables.responsive.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/datatables/Responsive-2.0.2/js/responsive.bootstrap.min.js"); ?>"></script>

<script type="text/javascript">
            $(document).ready(function () {
                $('#example').DataTable({
                    responsive: true,
                    language: {
                        url: '<?php echo base_url("assets/js/datatables/portugues.json"); ?>'
                    }
                });
            });
            function deleteConfirm(url) {
                bootbox.confirm("Tem certeza que deseja excluir?", function (res) {
                    if (res === true) {
                        window.location = url;
                    } else {
                        return res;
                    }
                });
            }
</script>
