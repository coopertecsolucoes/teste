<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Itens Inclusos nos Pacotes</h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('area/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->descricao ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('item/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Item</a></li>
                       
                        <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("item/update_item"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>

                                                 
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição *</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" value="<?php echo $result->descricao ?>" required="required" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Ícone *</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" id="icone" name="icone" value="<?php echo $result->icone ?>" required="required" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>
                        
                            
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                                    <a href="<?php echo site_url('item/index') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>                                    

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            
        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script src="<?php echo base_url("assets/js/ckeditor/ckeditor.js"); ?>"></script>
<script type="text/javascript">

    $(document).ready(function () {
        
    });
</script>
<!-- /form validation -->

