<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Parâmetro<small>Edição de Parâmetro</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url('parametro/index') ?>">Listagem</a>
                            </li>
                            <li class="active">
                                <strong><?php echo $result->descricao ?></strong>
                            </li>
                        </ol>
                    </div>

                    <div class="x_content">
                        <a class="btn btn-success" href="<?php echo site_url('parametro/create') ?>" ><i class="fa fa-plus-circle"></i> Novo Parâmetro</a></li>
                        <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("parametro/update_parametro"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                            <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <span class="red"><?php echo validation_errors(); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="descricao" name="descricao" required="required" value="<?php echo $result->descricao; ?>" class="form-control col-md-7 col-xs-12" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Ativo</label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select class="form-control" name="ativo" id="ativo">
                                        <option value="1" <?php echo ($result->ativo == 1) ? 'selected' : null ?>>Ativo</option>
                                        <option value="0" <?php echo ($result->ativo == 0) ? 'selected' : null ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="<?php echo site_url('parametro/index') ?>" class="btn btn-primary">Voltar</a>                                    
                                    <a href="<?php echo site_url('parametro/EnviarEmail') ?>" class="btn btn-primary">Enviar Email</a>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->

<link rel="stylesheet" href="assets/css/bootstrap-select.css">

<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario .btn').on('click', function () {
            $('#formulario').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#formulario').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });


</script>
<!-- /form validation -->
