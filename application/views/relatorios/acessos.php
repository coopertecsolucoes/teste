<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">

        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Report <small>User Access</small></h2>                       
                        <div class="clearfix"></div>
                        <ol class="breadcrumb">
                           
                        </ol>
                    </div>

                    <div class="x_content">
                        
                        <br />

                        <form id="formulario" method="POST" action="<?php echo base_url("report/gera_rel_acessos"); ?>" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                          

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="data_inicial">Date from
                                    </label>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <input type="text" id="data_inicial" name="data_inicial" required="required" value="<?php echo isset($result->data_inicial) ? date("d/m/Y", strtotime($result->data_inicial)) : null ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-3" for="data_final"> up to 
                                    </label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input type="text" id="data_final" name="data_final" required="required" value="<?php echo isset($result->data_final) ? date("d/m/Y", strtotime($result->data_final)) : null ?>" class="form-control col-md-7 col-xs-12" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario_id">User</label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="usuario_id" class="form-control">
                                        <option value="">Select...</option>
                                        <?php foreach ($usuarios  as $user) { ?>
                                            <option value="<?php echo $user->id ?>"><?php echo $user->nome ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tipo_rel">Group</label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <select class="form-control" name="tipo_rel" id="tipo_rel">
                                            <option value="0">By User</option>
                                            <option value="1">By Date</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">View</button>                                    
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</div>
<!-- /page content -->
<!-- form validation -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario .btn').on('click', function () {
            $('#formulario').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#formulario').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
        $('#data_inicial').mask("##/##/####");
        $('#data_final').mask("##/##/####");
     });
  
    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
<!-- /form validation -->
