<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Tipo_menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_Tipomenus() {

        $this->db->from('tipo_menu');
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_Tipomenu($data) {
        return $this->db->insert('tipo_menu', $data);
    }

    public function update_Tipomenu($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('tipo_menu', $data);
        }
    }

    public function delete_Tipomenu($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('tipo_menu');
        }
    }

    public function retorna_Tipomenu($id) {

        $this->db->from('tipo_menu');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_Tipomenu_ativos() {

        $this->db->from('tipo_menu');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
}
