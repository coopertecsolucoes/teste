<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Estado_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_estados($unidade_negocio_id) {

        $this->db->from('estado');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_estado($data) {
        return $this->db->insert('estado', $data);
    }

    public function update_estado($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('estado', $data);
        }
    }

    public function delete_estado($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('estado');
        }
    }

    public function retorna_estado($id) {

        $this->db->from('estado');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_estado_ativos($unidade_negocio_id) {

        $this->db->from('estado');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
