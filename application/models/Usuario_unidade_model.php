<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Frequencia_model class.
 * 
 * @extends CI_Model
 */
class Usuario_unidade_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_usuario_unidade($data) {
        return $this->db->insert('usuario_unidade', $data);
    }

    public function update_usuario_unidade($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('usuario_unidade', $data);
        }
    }

    public function delete_usuario_unidade($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('usuario_unidade');
        }
    }

    public function retorna_usuario_unidade($id) {
    
        $this->db->from('usuario_unidade');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }
    public function retorna_usuario_unidades($usuario_id) {

        $this->db->select('usu_uni.id id, usu_uni.usuario_id, usu_uni.unidade_negocio_id, uni.nome_fantasia, usu_uni.padrao, uni.logo');
        $this->db->from('usuario_unidade AS usu_uni');
        $this->db->join('unidade_negocio uni', 'uni.id = usu_uni.unidade_negocio_id');
        $this->db->where('usu_uni.usuario_id', $usuario_id);    
        $this->db->order_by('usu_uni.padrao desc, uni.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_id_unidade_padrao($usuario_id) {
        
        $this->db->from('usuario_unidade');
        $this->db->where('usuario_id', $usuario_id);
        $this->db->where('padrao', 1);
        return $this->db->get()->row();
    }
}
