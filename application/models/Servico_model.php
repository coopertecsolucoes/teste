<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Servico_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_servicos($unidade_negocio_id) {

        $this->db->from('servico');
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function create_servico($data) {
        return $this->db->insert('servico', $data);
    }

    public function update_servico($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('servico', $data);
        }
    }

    public function delete_servico($id) {

        if ($id) {
            $this->db->query('delete from servico_foto where servico_id='. $id);
            $this->db->where('id', $id);
            return $this->db->delete('servico');
        }
    }

    public function retorna_servico($id) {

        $this->db->select("a.*,  f.nome_arquivo , f.extensao");
        $this->db->from('servico a');
        $this->db->join('servico_foto f', 'f.servico_id=a.id and f.principal=1','left');
        
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
    public function retorna_servico_fotos($id) {

        $this->db->from('servico_foto ');        
        $this->db->where('servico_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_servicos_ativas_foto_principal($unidade_negocio_id) {

        $this->db->select("a.*,  f.nome_arquivo , f.extensao");
        $this->db->from('servico a');
        $this->db->join('servico_foto f', 'f.servico_id=a.id','left');
        $this->db->where('a.ativo', 1);
        $this->db->where('f.principal', 1);
         $this->db->where('a.unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('a.ordem');
        $query = $this->db->get();
        return $query->result();
        
    }
    
    public function retorna_servicos_ativos($unidade_negocio_id) {

        $this->db->select("m.*,  f.nome_arquivo , f.extensao");
        $this->db->from('servico m');
        $this->db->join('servico_foto f', 'f.servico_id=m.id','left');
        $this->db->where('m.ativo', 1);
        $this->db->where('f.principal', 1);
        $this->db->where('m.unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('m.ordem');
        $query = $this->db->get();
        return $query->result();
    }
}
