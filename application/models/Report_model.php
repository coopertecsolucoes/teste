<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categoria_model class.
 * 
 * @extends CI_Model
 */
class Report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('PHPJasperXML');
    }

    public function Gera_Relatorio($Parametros, $NomeRelatorio, $CaminhoCompleto) {   
        $PHPJasperXML = new PHPJasperXML();
        //$PHPJasperXML->debugsql=true;
        $PHPJasperXML->arrayParameter=$Parametros; 
        $xml =  simplexml_load_file('./reports/'. $NomeRelatorio . '.jrxml');
        $PHPJasperXML->xml_dismantle($xml);
        $PHPJasperXML->transferDBtoArray($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);
        //$PHPJasperXML->outpage("D",$NomeSave . '.pdf');    //page output method I:standard output  D:Download file
            
        $PHPJasperXML->outpage("I",$CaminhoCompleto);       
   
    }
    
    public function Gera_Relatorio_Arquivo($Parametros, $NomeRelatorio, $CaminhoCompleto) {   
        $PHPJasperXML = new PHPJasperXML();

        $PHPJasperXML->arrayParameter=$Parametros; 
        $xml =  simplexml_load_file('./reports/'. $NomeRelatorio . '.jrxml');
        $PHPJasperXML->xml_dismantle($xml);
        $PHPJasperXML->transferDBtoArray($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);
        //$PHPJasperXML->outpage("D",$NomeSave . '.pdf');    //page output method I:standard output  D:Download file
            
        $PHPJasperXML->outpage("F",$CaminhoCompleto);       
   
    }
}