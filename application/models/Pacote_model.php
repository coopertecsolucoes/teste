<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Pacote_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_pacotes($unidade_negocio_id) {
        $this->db->select('exe.*, ser.descricao desc_servico');
        $this->db->from('pacote as exe');
        $this->db->join('servico as ser','ser.id=exe.servico_id');
        
        
        $this->db->where('exe.unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('exe.descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_pacote($data) {
        return $this->db->insert('pacote', $data);
    }

    public function update_pacote($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('pacote', $data);
        }
    }

    public function delete_pacote($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('pacote');
        }
    }

    public function retorna_pacote($id) {

        $this->db->select("exe.*, ser.descricao desc_servico");
        $this->db->from('pacote exe ');
        $this->db->join('servico ser','ser.id=exe.servico_id');      
        $this->db->where('exe.id', $id);
        return $this->db->get()->row();
    }

    public function retorna_pacote_ativos($unidade_negocio_id) {

        $this->db->from('pacote');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_pacotes_fotos($pacote_id) {        
        $this->db->select("exe.id pacote_id, exe.*, ft.*");
        $this->db->from('pacote exe ');
        $this->db->join('pacote_foto ft',' ft.pacote_id=exe.id');        
        $this->db->where('exe.id',$pacote_id);                        
        $query = $this->db->get();
        return $query->result();
    }
    public function retorna_pacotes_destaques($unidade_negocio_id) {

        
        $this->db->select("exe.id pacote_id, exe.*, ft.*, ser.descricao desc_servico");
        $this->db->from('pacote exe ');
        $this->db->join('pacote_foto ft',' ft.pacote_id=exe.id');
        $this->db->join('servico ser','ser.id=exe.servico_id');
        $this->db->where('exe.unidade_negocio_id',$unidade_negocio_id);
        $this->db->where('exe.ativo',1);
        $this->db->where('ft.principal',1);
        $this->db->where('exe.destaque',1);
        $this->db->order_by('exe.descricao');
        $this->db->limit('3');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_pacotes_ativos_servico($unidade_negocio_id, $servico_id,$categoria_id,$mostra_mais) {
        $this->db->select("exe.*, ft.*, ser.descricao desc_servico");
        $this->db->from('pacote exe ');
        $this->db->join('pacote_foto ft',' ft.pacote_id=exe.id');
        $this->db->join('servico ser','ser.id=exe.servico_id');
        $this->db->where('exe.unidade_negocio_id',$unidade_negocio_id);
        $this->db->where('exe.ativo',1);
        $this->db->where('ft.principal',1);
        if ($servico_id<>-1){
            $this->db->where('exe.servico_id',$servico_id);
        }
        if ($categoria_id<>-1){
            $this->db->join('pacote_categoria pc','pc.pacote_id=exe.id');
            $this->db->where('pc.categoria_id',$categoria_id);
        }
        if ($mostra_mais==='false'){
            $this->db->limit('6');
        }
        $this->db->order_by('exe.descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
 
    public function retorna_pacotes_ativos_painel($unidade_negocio_id) {
        $this->db->select("exe.*, ft.*, ser.descricao desc_servico");
        $this->db->from('pacote exe ');
        $this->db->join('pacote_foto ft',' ft.pacote_id=exe.id');
        $this->db->join('servico ser','ser.id=exe.servico_id');
        $this->db->where('exe.unidade_negocio_id',$unidade_negocio_id);
        $this->db->where('exe.ativo',1);
        $this->db->where('ft.principal',1);
        $this->db->where('exe.mostra_painel',1);
        
        $this->db->order_by('exe.ordem');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
    public function retorna_pacote_categorias($pacote_id) {
        $this->db->from('pacote_categoria');
        $this->db->where('pacote_id', $pacote_id);        
        $query = $this->db->get();
        return $query->result();
    }
    public function retorna_pacote_itens($pacote_id) {
        $this->db->from('pacote_item');
        $this->db->where('pacote_id', $pacote_id);        
        $query = $this->db->get();
        return $query->result();
    }
}
