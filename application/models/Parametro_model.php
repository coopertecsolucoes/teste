<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Parametro_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_parametros() {

        $this->db->from('parametro');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_parametro($data) {
        return $this->db->insert('parametro', $data);
    }

    public function update_parametro($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('parametro', $data);
        }
    }

    public function delete_parametro($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('parametro');
        }
    }

    public function retorna_parametro($id) {

        $this->db->from('parametro');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

//    public function retorna_parametro_ativas() {
//
//        $this->db->from('parametro');
//        $this->db->where('ativo', 1);
//        $this->db->order_by('descricao');
//        $query = $this->db->get();
//        return $query->result();
//    }
    
    public function retorna_parametro_ativas($unidade_negocio_id) {

        $this->db->select("m.*,  f.nome_arquivo , f.extensao");
        $this->db->from('parametro m');
        $this->db->join('parametro_foto f', 'f.parametro_id=m.id','left');
        $this->db->where('m.ativo', 1);
         $this->db->where('m.unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('m.descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
