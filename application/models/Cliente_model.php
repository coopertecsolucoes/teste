<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Cliente_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_clientes($unidade_negocio_id) {

        $this->db->from('cliente');
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function create_cliente($data) {
        return $this->db->insert('cliente', $data);
    }

    public function update_cliente($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('cliente', $data);
        }
    }

    public function delete_cliente($id) {

        if ($id) {
            $this->db->query('delete from cliente_foto where cliente_id='. $id);
            $this->db->where('id', $id);
            return $this->db->delete('cliente');
        }
    }

    public function retorna_cliente($id) {

        $this->db->from('cliente');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_cliente_ativos($unidade_negocio_id) {
        $this->db->from('cliente');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('ordem, descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
