<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Painel_site_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_painel_sites($unidade_negocio_id) {

        $this->db->from('painel_site');
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function create_painel_site($data) {
        return $this->db->insert('painel_site', $data);
    }

    public function update_painel_site($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('painel_site', $data);
        }
    }

    public function delete_painel_site($id) {

        if ($id) {
            $this->db->query('delete from painel_site where id='. $id);
            $this->db->where('id', $id);
            return $this->db->delete('painel_site');
        }
    }

    public function retorna_painel_site($id) {

        $this->db->from('painel_site');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_painel_site_ativas($unidade_negocio_id) {
        $this->db->from('painel_site');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('ordem, descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
