<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Item_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_itens($unidade_negocio_id) {

        $this->db->from('item');

        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
     
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
        public function retorna_item_ativos($unidade_negocio_id) {

        $this->db->from('item');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
     
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_item($data) {
        return $this->db->insert('item', $data);
    }

    public function update_item($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('item', $data);
        }
    }

    public function delete_item($id) {

        if ($id) {
            
            $this->db->where('id', $id);
            return $this->db->delete('item');
        }
    }

    public function retorna_item($id) {

        $this->db->from('item');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

   
}
