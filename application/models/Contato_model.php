<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Contato_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_contatos($unidade_negocio_id) {
    
        $this->db->select('con.*, pa.descricao desc_pacote');  
        $this->db->from('contato con');  
        $this->db->join('pacote pa','pa.id=con.pacote_id','left');  
        
        $this->db->where('con.unidade_negocio_id',$unidade_negocio_id );
        $query = $this->db->get();
        return $query->result();
    }

    public function create_contato($data) {
        return $this->db->insert('contato', $data);
    }

    public function update_contato($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('contato', $data);
        }
    }

    public function delete_contato($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('contato');
        }
    }

    public function retorna_contato($id) {

        $this->db->select('con.*, pa.descricao desc_pacote');  
        $this->db->from('contato con');  
        $this->db->join('pacote pa','pa.id=con.pacote_id','left');  
        $this->db->where('con.id', $id);
        return $this->db->get()->row();
    }
    


    public function retorna_contato_ativas($unidade_negocio_id) {
   
        $this->db->from('contato');        
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('data desc');
        $query = $this->db->get();
        return $query->result();
    }
}
