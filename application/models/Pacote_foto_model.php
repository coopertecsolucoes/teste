<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Pacote_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_pacote_foto($data) {
        return $this->db->insert('pacote_foto', $data);
    }

    public function update_pacote_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('pacote_foto', $data);
        }
    }
    
    public function ativa_foto_principal($area_id, $pacote_foto_id) {
        $this->db->query("update pacote_foto set principal=0 where "
                . "principal=1 and pacote_id=" . $area_id );
        
        $this->db->query("update pacote_foto set principal=1 where "
                . "id=" . $pacote_foto_id );
        return true;
    }

    public function delete_pacote_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('pacote_foto');
        }
    }

    public function retorna_pacote_foto($id) {

        $this->db->from('pacote_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_pacote_fotos_ativas() {

        $this->db->from('pacote_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
     public function retorna_pacote_foto_principal($area_id) {

        $this->db->select('lf.id, lf.area_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('pacote_foto AS lf');
        $this->db->where('lf.area_id', $area_id);
        $this->db->where('lf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_pacote_fotos($pacote_id) {
        $this->db->from('pacote_foto AS lf');
        $this->db->where('lf.pacote_id', $pacote_id);
        $query = $this->db->get();
        return $query->result();
    }
}
