<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Usuario_acesso_model class.
 * 
 * @extends CI_Model
 */
class Usuario_acesso_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_usuario_acesso($data) {
        return $this->db->insert('usuario_acesso', $data);
    }
    
    public function acrescenta_acesso_usuario($usuario_id) {
        $this->db->set('acesso', 'acesso+1', FALSE);    
        $this->db->where('usuario_id',$usuario_id);
        return $this->db->update('usuario_acesso');
    }

    public function retorna_usuario_acesso($id) {
        $this->db->from('usuario_acesso');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_usuario_acessos_completo($usuario_id) {

        $this->db->select('ua.id, ua.usuario_id, ua.acesso, ua.arquivo_id, u.usuario, u.nome');
        $this->db->from('usuario_acesso AS ua');
        $this->db->join('usuario AS u', 'u.id = ua.usuario_id');
        $this->db->where('ua.usuario_id', $usuario_id);
        $query = $this->db->get();
        return $query->result();
    }

}
