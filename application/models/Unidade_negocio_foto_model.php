<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Unidade_negocio_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_unidade_negocio_foto($data) {
        return $this->db->insert('unidade_negocio_foto', $data);
    }

    public function update_unidade_negocio_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('unidade_negocio_foto', $data);
        }
    }
    
    public function ativa_foto_principal($unidade_negocio_id, $unidade_negocio_foto_id) {
        $this->db->query("update unidade_negocio_foto set principal=0 where "
                . "principal=1 and unidade_negocio_id=" . $unidade_negocio_id );
        
        $this->db->query("update unidade_negocio_foto set principal=1 where "
                . "id=" . $unidade_negocio_foto_id );
        return true;
    }

    public function delete_unidade_negocio_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('unidade_negocio_foto');
        }
    }

    public function retorna_unidade_negocio_foto($id) {

        $this->db->from('unidade_negocio_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_unidade_negocio_fotos_ativas() {

        $this->db->from('unidade_negocio_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
     public function retorna_unidade_negocio_foto_principal($unidade_negocio_id) {

        $this->db->select('lf.id, lf.unidade_negocio_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('unidade_negocio_foto AS lf');
        $this->db->where('lf.unidade_negocio_id', $unidade_negocio_id);
        $this->db->where('lf.principal', 1);
        return $this->db->get()->row();
    }

    public function retorna_unidade_negocio_fotos($unidade_negocio_id) {

        $this->db->select('lf.id, lf.unidade_negocio_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('unidade_negocio_foto AS lf');
        $this->db->where('lf.unidade_negocio_id', $unidade_negocio_id);
        
        $query = $this->db->get();
        return $query->result();
    }
}
