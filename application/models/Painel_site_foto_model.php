<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Painel_site_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_painel_site_foto($data) {
        return $this->db->insert('painel_site_foto', $data);
    }

    public function update_painel_site_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('painel_site_foto', $data);
        }
    }

    public function delete_painel_site_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('painel_site_foto');
        }
    }

    public function retorna_painel_site_foto($id) {

        $this->db->from('painel_site_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_painel_site_fotos_ativas() {

        $this->db->from('painel_site_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
     public function retorna_painel_site_foto_principal($painel_site_id) {

        $this->db->select('lf.id, lf.painel_site_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('painel_site_foto AS lf');
        $this->db->where('lf.painel_site_id', $painel_site_id);
        $this->db->where('lf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_painel_site_fotos($painel_site_id) {

        $this->db->select('lf.id, lf.painel_site_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('painel_site_foto AS lf');
        $this->db->where('lf.painel_site_id', $painel_site_id);
        $this->db->where('lf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }
}
